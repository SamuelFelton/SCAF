#
# SCAF Project Dockerfile
#
FROM gradle:4.5.1-jdk8
MAINTAINER Guillaume Courtet "guillaume.courtet@hotmail.fr"
USER root

RUN mkdir -p /scaf && apt-get update && apt-get install -y openjfx && rm -rf /var/lib/apt/lists/*

WORKDIR /scaf
