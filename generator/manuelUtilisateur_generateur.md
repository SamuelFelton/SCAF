﻿# Générateur de données : how to

Ce manuel passe en revue plusieurs façons d'utiliser le **générateur de données**. Il explique en détails les points clefs de l'édition du fichier de configuration XML ainsi que la sortie au format texte.

## Fichier d'entrée

Le fichier d'entrée doit avoir le format suivant :

***/!\ Attention*** : la casse doit être respectée pour le nom des balises et des attributs.

```xml
<MACHINE id="1" step="0,2" nbRepetitions="1" name="machine1" >
	<MOTIF name="motif1" nbRepetitions="1">
		<SEQUENCE durationMin="1" durationMax="2">
			<FUNCTION value="2*x+1"/>
		</SEQUENCE>
		<SEQUENCE durationMin="5" durationMax="5">
			<FUNCTION value="max(-x+2,0)" />
		</SEQUENCE>
	</MOTIF>
</MACHINE>
```

* L'attribut **id** dans la balise **\<MACHINE\>** correspond à l'identifiant de la simulation, si deux machines font partie de la même simulation, elles doivent avoir le même identifiant afin que le module **fouille de données** puissent les lier ensemble;
* Si l'identifiant n'est pas renseigné, un identifiant unique et aléatoire est généré;
* Si le nom de la machine n'est pas renseigné, le nom est "*default*";
* Une machine peut avoir plusieurs motifs et peut se répéter (s'il n'y a pas de répétition, mettre *nbRepetitions="1"*);
* Chaque motif possède des séquences, qui correspondent chacune à une fonction mathématique. Le motif, comme la machine, peut se répéter.


### Transformations

Si une fonction doit avoir un comportement aléatoire entre deux répétitions, il est possible, en plus de faire varier la durée avec les attributs *durationMin* et *durationMax*, d'ajouter des transformations aux fonctions, comme dans l'exemple qui suit : 

```xml
<SEQUENCE durationMin="25" durationMax="35">
	<FUNCTION value="sqrt(x)"/>
	<TRANSFORMATION type="verticalTransition" min="-1" max="1" />
</SEQUENCE>
```

Il existe 4 types de transformations :

* transition verticale (mot clef ***verticalTranslation***);
* dilatation verticale (mot clef ***verticalDilatation***);
* dilatation horizontale (mot clef ***verticalDilatation***);
* renversement (mot clef ***overthrow***);

Une transformation est accompagnée d'un indice minimum et maximum, choisi aléatoirement à chaque répétition.

## Utilisation de la librairie mxParser

Le générateur utilise l'API de mxParser pour gérer les fonctions mathématiques, cette partie détaille la syntaxe de quelques fonctions de la librairie ainsi que l'utilisation des variables propres au générateur.

### La variable b

La variable **b** peut être utilisée lorsque la suite de fonctions formées par un motif doit être **continue**, voici un exemple d'utilisation de cette variable :

**FONCTION CONTINUE**

```xml
	<MOTIF name="motif1" nbRepetitions="1">
		<SEQUENCE durationMin="35" durationMax="35">
			<FUNCTION value="sqrt(x)"/>
		</SEQUENCE>
		<SEQUENCE durationMin="50" durationMax="50">
			<FUNCTION value="b+sin(x)"/>
		</SEQUENCE>
	</MOTIF>
```

Résultat de la génération : 

![fonction continue](https://zupimages.net/up/18/15/09u0.png)

**FONCTION NON CONTINUE**

```xml
	<MOTIF name="motif1" nbRepetitions="1">
		<SEQUENCE durationMin="35" durationMax="35">
			<FUNCTION value="sqrt(x)"/>
		</SEQUENCE>
		<SEQUENCE durationMin="50" durationMax="50">
			<FUNCTION value="sin(x)"/>
		</SEQUENCE>
	</MOTIF>
```

Résultat de la génération : 

![fonction continue](https://zupimages.net/up/18/15/m8y6.png)

**b** correspond donc à la **valeur de fin** de la séquence précédente, si la variable n’apparaît pas dans la fonction, elle démarre à 0.

### Fonctions de l'API mxParser 

Toutes les fonctions utilisables sont détaillées dans la documentation de l'API mxParser disponible à l'adresse suivante : [mxParser-API](http://mathparser.org/mxparser-math-collection/)

## Lancer le générateur

Pour lancer le générateur, il est nécessaire d'utiliser la ligne de commande suivante :

`$ java -jar generator.jar -input fichier.xml -output dossierCible`

où "***fichier.xml***" est le fichier de configuration décrit ci-dessus et "***dossierCible***" est le répertoire de destination du résultat de la génération.

**A noter** : il est possible d'inverser *-input* et *-output* dans la commande.

## Résultat de la génération

Le fichier résultat est au format texte (.txt) et liste toute les valeurs générées, séparées par des "**;**". Le fichier est nommé automatiquement en suivant le format suivant : `nomMachine.identidiantSimulation.txt`
**Exemple de résultat de génération :**

`
1/1.0/0.0;1.0;1.4142;1.7320;2.0;2.2360;2.4494;2.6457;2.8284;3.0;0.0;0.8414;0.9092;0.1411;
2/1.0/0.0;1.0;1.4142;1.7320;2.0;2.2360;2.4494;0.0;0.8414;0.9092;0.1411;-0.7568;-0.9589;
3/1.0/0.0;1.0;1.4142;1.7320;2.0;2.2360;0.0;0.8414;0.9092;0.1411;-0.7568;-0.9589;-0.2794;
`

* Chaque ligne correspond à une répétition de la simulation (ici **3** répétitions), les éléments sont séparés par des **/**;
* La première valeur correspond au compteur de répétition;
* La deuxième valeur correspond au pas de temps de la génération (ici une valeur tous les **1 pas de temps**);
* La troisième valeur correspond à la liste des données générées.