package generator;

import generator.algorithm.GeneratorAlgo;
import generator.model.Machine;
import generator.parser.GeneratorParser;

import java.io.*;
import java.text.Normalizer;
import java.util.List;
import java.util.Objects;

/**
 * Main class of generator
 * Parse the xml file (-input), execute the Machine and generate a second file (with path = -output) containing the values.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        if (args.length != 2 && args.length != 4) throw new IllegalArgumentException("Parameters format : -input \"myFile.xml\" [-output \"folderPath\"]");
        File input;
        File output;

        if(args.length == 2) {
            if(args[0].equals("-input") || args[0].equals("-in")){
                input = new File(args[1]);
            } else {
                throw new IllegalArgumentException("Need an -input parameter");
            }
            output = input.getParentFile();
        } else {
            if(args[0].equals("-input") || args[0].equals("-in")){
                input = new File(args[1]);
            } else if(args[2].equals("-input") || args[2].equals("-in")) {
                input = new File(args[3]);
            } else {
                throw new IllegalArgumentException("Need an -input parameter");
            }

            if(args[0].equals("-output") || args[0].equals("-out")){
                output = new File(args[1]);
            } else if(args[2].equals("-output") || args[2].equals("-out")) {
                output = new File(args[3]);
            } else {
                throw new IllegalArgumentException("Parameters format : -input \"myFile.xml\" [-output \"folderPath\"]");
            }
        }

        if (!output.exists()){
            output.mkdirs();
        }

        if (input == null || !input.exists()) {
            throw new FileNotFoundException("Input file doesn't exist.");
        }

        if (output == null || !output.exists() || !output.isDirectory()) {
            throw new FileNotFoundException("Can't reach the output directory.");
        }

        final Machine sim = GeneratorParser.generateSimulation(input);
        final List<List<Double>> values = GeneratorAlgo.executeSimulation(sim);

        Objects.requireNonNull(values,"Generation can't result in empty values.");

        String normalized = Normalizer.normalize(sim.getName().trim().replaceAll("\\s+","_"), Normalizer.Form.NFD);
        String accentRemoved = normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

        final String resultFile = output.getAbsolutePath()+'/'+accentRemoved+"."+sim.getId()+".txt";

        generateResultFile(resultFile, sim.getStep(), values);
    }

    private static void generateResultFile(String resultFile, double step, List<List<Double>> values) {
        try(FileWriter fw = new FileWriter(resultFile);
            BufferedWriter bw = new BufferedWriter(fw)) {

            final StringBuilder content = new StringBuilder();
            long nbLine = 1;

            for (List<Double> l : values) {
                content.append(nbLine+"/"+step+"/");
                nbLine++;

                for (Double d : l) {
                    content.append(d);
                    content.append(';');
                }

                bw.write(content.toString());
                bw.write(System.lineSeparator());

                content.setLength(0);
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}