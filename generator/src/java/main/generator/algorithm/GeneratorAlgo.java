package generator.algorithm;

import generator.model.Machine;
import generator.model.Motif;
import generator.model.Sequence;
import org.mariuszgromada.math.mxparser.Expression;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class GeneratorAlgo {

    public static List<List<Double>> executeSimulation(Machine machine) {

        System.out.print("Generating");

        return IntStream.range(0, machine.getNbRepetitions()).boxed().parallel()
                .collect(ArrayList<List<Double>>::new, (ArrayList<List<Double>> mvs, Integer i) -> {
                    //Show progress
                    if (i % machine.getNbRepetitions()/20 == 0) {
                        System.out.print(".");
                        System.out.flush();
                    }

                    double beginningOrd = 0;
                    final List<Double> values = new ArrayList<>();

                    for(Motif m : machine.getMotifs()){
                        final int nbRepMotif = m.getNbRepetitions();

                        for (int r = 0; r < nbRepMotif; r++) {
                            for(Sequence seq : m.getSequences()) {
                                final Sequence s = seq.withTransformations();
                                //each repetition of a motif has a different duration and transformations indexes
                                final double sequenceDuration = s.computeDuration();

                                double res = 0;
                                double l;
                                int cpt = 0;
                                for (l = 0; l < sequenceDuration; l = cpt * machine.getStep()) {
                                    final Expression e = s.getFunction().getExp();
                                    e.setArgumentValue("x", l);
                                    e.setArgumentValue("b", beginningOrd);
                                    res = e.calculate();
                                    values.add(res);
                                    cpt++;


                                }
                                beginningOrd = res;
                            }
                        }
                    }
                    mvs.add(values);
                }, ArrayList::addAll);
    }
}