package generator.model;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Wrapper class between the generator and the mxParser api.
 * A function has and expression (corresponding to its value) and
 * two arguments 'x' and 'b'.
 */
public class Function {
    private String value;
    private Expression exp;
    private Argument x;
    private Argument b;
    private float duration;

    public Function() { }

    public Function(String value, float duration) {
        this.value = value;
        this.x = new Argument("x");
        this.b = new Argument("b");
        this.exp = new Expression(value, x, b);
        this.duration = duration;
    }

    @XmlAttribute(name = "value")
    public void setValue(String value) {
        this.value = value;
        exp = new Expression(value);
        x = new Argument("x");
        b = new Argument("b");
        exp.addArguments(x);
        exp.addArguments(b);

        if(!exp.checkSyntax()) throw new IllegalArgumentException("Incorrect function value : " + this.value);
    }

    public String getValue() {
        return value;
    }

    public void setExp(Expression exp) {
        this.exp = exp;
    }

    public Expression getExp() {
        return exp;
    }

    public void setX(Argument x) {
        this.x = x;
    }

    public Argument getX() {
        return x;
    }

    /**
     * b corresponds to the end-value of the previous sequence.
     * 0 is default, the sequence will start at 0.
     * @param b
     */
    public void setB(Argument b) {
        this.b = b;
    }

    public Argument getB() {
        return b;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public float getDuration() {
        return duration;
    }
}