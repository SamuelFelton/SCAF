package generator.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Model for the Generator. Mainly a data-class.
 * A machine contains a list of motifs.
 * It has a step and a number of repetitions.
 */
@XmlRootElement(name="MACHINE")
public class Machine {
    private String name;
    private double step;
    private int nbRepetitions;
    private List<Motif> motifs;
    private String id;

    public Machine(){
        motifs = new ArrayList<>();
        id = UUID.randomUUID().toString();
        name = "default name";
    }

    @XmlAttribute(name="id")
    public void setId(String id){ this.id = id; }

    public String getId(){
        return id;
    }

    @XmlAttribute(name="name")
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @XmlAttribute(name="step")
    public void setStep(double step) {
        this.step = step;
    }

    public double getStep() {
        return step;
    }

    @XmlAttribute(name="nbRepetitions")
    public void setNbRepetitions(int nbRepetitions) {
        this.nbRepetitions = nbRepetitions;
    }

    public int getNbRepetitions() {
        return nbRepetitions;
    }

    @XmlElement(name="MOTIF")
    public void setMotifs(List<Motif> motifs) {
        this.motifs = motifs;
    }

    public List<Motif> getMotifs() {
        return motifs;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(Locale.US, "Machine\nId : %s\nName : %s\nStep : %f\nNbRepetitions : %d\n",
                getId(),getName(), getStep(), getNbRepetitions()));

        for(Motif m : getMotifs()) {
            sb.append("\tMOTIF\n");
            sb.append(String.format("\tName : %s\n",m.getName()));

            for(Sequence s : m.getSequences()){
                sb.append("\t\tFUNCTION\n");
                sb.append(String.format("\t\tValue : %s\n",s.getFunction().getValue()));
                sb.append(String.format("\t\tDuration : %f\n", s.getDuration()));
            }
        }
        return sb.toString();
    }
}
