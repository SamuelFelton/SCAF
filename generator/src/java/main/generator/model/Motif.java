package generator.model;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * A motif contains a list of sequences and a name.
 */
public class Motif {
    private String name;
    private int nbRepetitions;
    private List<Sequence> sequences;

    public Motif() {
        sequences = new ArrayList<>();
        nbRepetitions = 1;
    }

    @XmlAttribute(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @XmlAttribute(name = "nbRepetitions")
    public void setNbRepetitions(int nbRepetitions) {
        this.nbRepetitions = nbRepetitions;
    }

    public int getNbRepetitions() {
        return nbRepetitions;
    }

    @XmlElement(name = "SEQUENCE")
    public void setSequences(List<Sequence> sequences) {
        this.sequences = sequences;
    }

    public List<Sequence> getSequences() {
        return sequences;
    }
}