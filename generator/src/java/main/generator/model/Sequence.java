package generator.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * A sequence is a function combined with a list of transformation.
 * It has a duration computed randomly between two values in an interval.
 */
public class Sequence {
    private float duration;
    private float durationMin;
    private float durationMax;
    private Function function;
    private List<Transformation> transformations;

    public Sequence() {
        transformations = new ArrayList<>();
    }

    /**
     * Choose randomly the duration of the sequence between lower and upper bounds.
     * @return The sequence duration.
     */
    public float computeDuration() {
        if (durationMax - durationMin < 0) throw new ArithmeticException("durationMin must be < than durationMax");
        //durationMax is exclusive
        duration = durationMin + (float) (Math.random() * (durationMax - durationMin));
        function.setDuration(duration);
        return duration;
    }

    public Sequence withTransformations() {
        final Sequence s = new Sequence();
        s.durationMin = durationMin;
        s.durationMax = durationMax;
        Function f = new Function(function.getValue(), function.getDuration());
        for(final Transformation tr : transformations) {
            f = tr.withTransformation(f);
        }
        s.function = f;
        return s;
    }

    public float getDuration() {
        return duration;
    }

    @XmlAttribute(name = "durationMin")
    public void setDurationMin(float durationMin) {
        if (durationMin < 0) throw new IllegalArgumentException("duration cannot be negative");
        this.durationMin = durationMin;
    }

    public float getDurationMin() {
        return durationMin;
    }

    @XmlAttribute(name = "durationMax")
    public void setDurationMax(float durationMax) {
        if (durationMax < 0) throw new IllegalArgumentException("duration cannot be negative");
        this.durationMax = durationMax;
    }

    public float getDurationMax() {
        return durationMax;
    }

    @XmlElement(name = "FUNCTION")
    public void setFunction(Function function) {
        this.function = function;
    }

    public Function getFunction() {
        return function;
    }

    @XmlElement(name = "TRANSFORMATION")
    public void setTransformations(List<Transformation> transformations) {
        this.transformations.clear();
        transformations.forEach(tr -> this.transformations.add(tr));
    }

    public List<Transformation> getTransformations() {
        return transformations;
    }
}