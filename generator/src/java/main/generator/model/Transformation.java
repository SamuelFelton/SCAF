package generator.model;

import javax.xml.bind.annotation.XmlAttribute;
import java.util.HashMap;
import java.util.Map;

/**
 * Apply a transformation to a function changes its value and maybe its duration.
 * The transformation index is randomly compute between to values min and max.
 */
public class Transformation {
    private String type;
    private float min;
    private float max;

    @FunctionalInterface
    private interface Command {
        Function execute(Function f, float value);
    }

    private static Map<String, Command> commandsForTransformations = new HashMap<>();

    static {
        final Command horizontalDilatation = (f, value) -> {
            final String argName = f.getX().getArgumentName();
            final String newArgName = "(" + value + "*" + argName + ")";

            final String expression = f.getExp().getExpressionString();
            final String newExpression = expression.replaceAll(argName, newArgName);

            //if value > 1 then the duration increases, else it decreases
            return new Function(newExpression, f.getDuration() / value);
        };

        final Command verticalDilatation =
                (f, value) -> new Function(value + "*(" + f.getExp().getExpressionString() + ")", f.getDuration());

        final Command verticalTranslation = (f, value) -> {
            final String expression = f.getExp().getExpressionString();
            String op = value > 0 ? "+" : "-";
            String newExpression = "("+expression+")"+ op + Math.abs(value);

            return new Function(newExpression, f.getDuration());
        };

        final Command overthrow = (f, value) -> verticalDilatation.execute(f, -1);

        commandsForTransformations.put("verticalDilatation", verticalDilatation);
        commandsForTransformations.put("horizontalDilatation", horizontalDilatation);
        commandsForTransformations.put("verticalTranslation", verticalTranslation);
        commandsForTransformations.put("overthrow", overthrow);
    }

    public Transformation() { }

    /**
     * Apply transformations to a function and return it.
     * @param function
     * @return the transformed function.
     */
    public Function withTransformation(Function function) {
        float value = computeValue();
        return commandsForTransformations.getOrDefault(type,
                (f, v) -> {
                    throw new IllegalArgumentException("Invalid transformation, please check the syntax in the user manual (is there any typo ?)");
                })
                .execute(function, value);
    }

    private float computeValue() {
        //max is exclusive
        return min + (float) (Math.random() * (max - min));
    }

    public String getType() {
        return type;
    }

    @XmlAttribute(name = "type")
    public void setType(String type) {
        this.type = type;
    }

    public float getMin() {
        return min;
    }

    @XmlAttribute(name = "min")
    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    @XmlAttribute(name = "max")
    public void setMax(float max) {
        this.max = max;
    }
}