package generator.parser;

import generator.model.Motif;
import generator.model.Sequence;
import generator.model.Machine;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Parser for the generator
 * Create a Machine object from a xml file
 */
public class GeneratorParser {
    /**
     * Create a Machine object from the xml file.
     * We assume the file exists and is well formed.
     * @param file XML file.
     * @return A Machine configured accordingly with the xml file.
     */
    public static Machine generateSimulation(File file) {
        Machine sim = null;

        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(Machine.class);
            final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            sim = (Machine) jaxbUnmarshaller.unmarshal(file);
            computeTransformation(sim);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return sim;
    }

    /*Compute duration to all sequences*/
    private static void computeTransformation(Machine sim) {
        for (Motif m : sim.getMotifs()) {
            for (Sequence s : m.getSequences()) {
                s.computeDuration();
            }
        }
    }
}
