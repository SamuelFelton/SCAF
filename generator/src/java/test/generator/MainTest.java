package generator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import static junit.framework.TestCase.fail;

public class MainTest {
    private String xmlFile;
    private String outputPath;


    @Before
    public void initArgs(){
        String content = "<MACHINE step=\"0.2\" nbRepetitions=\"2\" name=\"machine1\" >\n" +
                "    <MOTIF name=\"motif 1\" nbRepetitions=\"3\">\n" +
                "        <SEQUENCE durationMin=\"2\" durationMax=\"6\">\n" +
                "            <FUNCTION value=\"2*x+1\"/>\n" +
                "        </SEQUENCE>\n" +
                "        <SEQUENCE durationMin=\"1\" durationMax=\"5\">\n" +
                "            <FUNCTION value=\"b\" />\n" +
                "        </SEQUENCE>\n" +
                "        <SEQUENCE durationMin=\"2\" durationMax=\"2\">\n" +
                "            <FUNCTION value=\"-2*x+b\" />\n" +
                "        </SEQUENCE>\n" +
                "    </MOTIF>\n" +
                "    <MOTIF name=\"motif 2\">\n" +
                "        <SEQUENCE durationMin=\"3\" durationMax=\"10\">\n" +
                "            <FUNCTION value=\"x\" />\n" +
                "            <TRANSFORMATION type=\"horizontalDilatation\" min=\"0.8\" max=\"0.9\" />\n" +
                "            <TRANSFORMATION type=\"verticalDilatation\" min=\"1.3\" max=\"1.5\" />\n" +
                "        </SEQUENCE>\n" +
                "    </MOTIF>\n" +
                "</MACHINE>";

        xmlFile = "testMain.xml";

        try (PrintStream out = new PrintStream(new FileOutputStream(xmlFile))) {
            out.print(content);
        } catch(Exception e){
            e.printStackTrace();
        }

        outputPath = "./testOutput";
        if(!(new File(outputPath).mkdir())){
            fail();
        }
    }

    @Test
    public void checkMainArgumentsInOrder() {
        try {
            Main.main(new String[] {"-input", xmlFile, "-output", outputPath});
        } catch (FileNotFoundException e) {
            fail();
        }
    }

    @Test
    public void checkMainArgumentsInDisorder() {
        try {
            Main.main(new String[] {"-output", outputPath, "-input", xmlFile});
        } catch (FileNotFoundException e) {
            fail();
        }
    }

    @Test(expected=IllegalArgumentException.class)
    public void checkIfMainFailsWithoutKeyWordsInArguments() {
        try {
            Main.main(new String[] {outputPath, xmlFile});
        } catch (FileNotFoundException e) {
            fail();
        }
    }

    @After
    public void deleteFiles(){
        Path output = new File(outputPath).toPath();
        Path xml = new File(xmlFile).toPath();
        try {
            Files.walk(output)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
            Files.delete(xml);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(Files.exists(output)) fail();
        if(Files.exists(xml)) fail();
    }
}