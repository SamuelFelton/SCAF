package generator.algorithm;
import static org.junit.Assert.assertTrue;

import generator.model.Machine;
import generator.parser.GeneratorParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class GeneratorAlgoTest {
    private String filepath;
    private File file;
    @Before
    public void initXMLFile(){
        String content = "<MACHINE step=\"0.2\" nbRepetitions=\"2\" machine=\"machine1\" >\n" +
                "    <MOTIF name=\"motif 1\" nbRepetitions=\"3\">\n" +
                "        <SEQUENCE durationMin=\"2\" durationMax=\"6\">\n" +
                "            <FUNCTION value=\"2*x+1\"/>\n" +
                "        </SEQUENCE>\n" +
                "        <SEQUENCE durationMin=\"1\" durationMax=\"5\">\n" +
                "            <FUNCTION value=\"b\" />\n" +
                "        </SEQUENCE>\n" +
                "        <SEQUENCE durationMin=\"2\" durationMax=\"2\">\n" +
                "            <FUNCTION value=\"-2*x+b\" />\n" +
                "        </SEQUENCE>\n" +
                "    </MOTIF>\n" +
                "    <MOTIF name=\"motif 2\">\n" +
                "        <SEQUENCE durationMin=\"3\" durationMax=\"10\">\n" +
                "            <FUNCTION value=\"x\" />\n" +
                "            <TRANSFORMATION type=\"horizontalDilatation\" min=\"0.8\" max=\"0.9\" />\n" +
                "            <TRANSFORMATION type=\"verticalDilatation\" min=\"1.3\" max=\"1.5\" />\n" +
                "        </SEQUENCE>\n" +
                "    </MOTIF>\n" +
                "</MACHINE>";

        filepath = "testGeneratorAlgo.xml";

        try (PrintStream out = new PrintStream(new FileOutputStream(filepath))) {
            out.print(content);
        } catch(Exception e){
            e.printStackTrace();
        }
        file = new File(filepath);
    }

    @After
    public void deleteXMLFile() {
        try {
            Files.delete(Paths.get(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkIfResultFileIsWellCreated() {
        Machine sim = GeneratorParser.generateSimulation(file);
        List<List<Double>> machine = GeneratorAlgo.executeSimulation(sim);
        assertTrue(!machine.isEmpty());

        for(List<Double> values : machine){
            assertTrue(!values.isEmpty());
        }
    }
}
