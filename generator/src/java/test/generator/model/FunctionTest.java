package generator.model;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Argument;

public class FunctionTest {
    @Test
    public void changeTheValueOfAFunctionShouldChangeTheExpressionAndArguments(){
        //ARRANGE
        Argument x = new Argument("x");

        Function f = new Function("2*x+1",1);
        //ACT
        f.setValue("3*x+b");
        //ASSERT
        assertThat(f.getX().getArgumentName()).isEqualTo("x");
        assertThat(f.getValue()).isEqualTo("3*x+b");
        assertThat(f.getExp().getExpressionString()).isEqualTo("3*x+b");
    }
}
