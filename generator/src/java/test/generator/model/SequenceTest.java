package generator.model;

import org.junit.Test;
import org.mariuszgromada.math.mxparser.Argument;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.*;

public class SequenceTest {
    @Test
    public void checkThatComputeDurationResultIsBtwMinAndMaxDuration() {
        Sequence seq = new Sequence();
        Function f = mock(Function.class);
        seq.setFunction(f);
        float max = 5;
        float min = 1;
        seq.setDurationMin(min);
        seq.setDurationMax(max);
        seq.computeDuration();
        assertThat(seq.getDuration()).isBetween(min, max);
    }

    @Test
    public void checkApplyTransformationIsCalledOncePerTransformation() {
        //ARRANGE
        Sequence seq = new Sequence();
        Argument x = new Argument("x");
        Argument b = new Argument("b");
        float duration = 1;
        Function f = new Function("2x+1", duration);
        seq.setFunction(f);
        seq.setDurationMin(duration);
        seq.setDurationMax(duration);
        seq.computeDuration();
        List<Transformation> tr = new ArrayList<>();
        Transformation tr1 = new Transformation();
        tr1.setType("overthrow");
        Transformation tr2 = new Transformation();
        tr2.setType("verticalTranslation");
        tr2.setMin(duration);
        tr2.setMax(duration);
        tr.add(tr1);
        tr.add(tr2);

        //ACT
        seq.setTransformations(tr);
        Sequence s_transformed = seq.withTransformations();
        Function changedFunction = s_transformed.getFunction();

        //ASSERT
        assertThat(changedFunction.getValue()).isEqualTo("(-1.0*(2x+1))+1.0");
    }

    @Test
    public void checkIfSequenceValueAfterTransformationIsCorrect(){
        //ARRANGE
        Argument x = new Argument("x");
        Argument b = new Argument("b");
        float duration = 1;
        Function f = new Function("2x+1", duration);

        Sequence seq = new Sequence();
        seq.setFunction(f);
        seq.setDurationMin(duration);
        seq.setDurationMax(duration);
        seq.computeDuration();
        List<Transformation> tr = new ArrayList<>();

        Transformation t = new Transformation();
        t.setType("overthrow");
        tr.add(t);
        seq.setTransformations(tr);

        //ACT
        Sequence s_transformed = seq.withTransformations();

        //ASSERT
        assertThat(s_transformed.getFunction().getValue()).isEqualTo("-1.0*(2x+1)");
    }

    @Test
    public void checkThatDurationMinAndDurationMaxArentLessThanZero() {
        float min = -1f;
        float max = -5f;
        Sequence s = new Sequence();

        try {
            s.setDurationMin(min);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {

        }
        try {
            s.setDurationMax(max);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {

        }
    }

    @Test
    public void checkThatComputeDurationValueFailsIfMinGreaterThanMax(){
        float min = 5f;
        float max = 1f;
        Sequence s = new Sequence();
        s.setDurationMin(min);
        s.setDurationMax(max);
        try{
            s.computeDuration();
            failBecauseExceptionWasNotThrown(ArithmeticException.class);
        } catch(ArithmeticException e){

        }


    }
}