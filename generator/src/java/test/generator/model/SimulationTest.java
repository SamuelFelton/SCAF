package generator.model;

import org.junit.Test;
import java.util.ArrayList;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class SimulationTest {
    @Test
    public void checkIfMachineHasCorrectNumberOfMotifs(){
        //ARRANGE
        Machine sim = new Machine();
        ArrayList<Motif> motifs = new ArrayList<>();
        Motif m1 = new Motif();
        m1.setName("m1");
        Motif m2 = new Motif();
        m2.setName("m2");
        motifs.add(m1);
        motifs.add(m2);

        //ACT
        sim.setMotifs(motifs);

        //ASSERT
        assertThat(sim.getMotifs().size()).isEqualTo(2);
        assertThat(sim.getMotifs().get(0).getName()).isEqualTo("m1");
        assertThat(sim.getMotifs().get(1).getName()).isEqualTo("m2");
    }

    @Test
    public void checkIfMachineDisplaysCorrectly() {
        //ARRANGE
        Machine sim = new Machine();
        sim.setName("machine1");
        sim.setId("123");
        ArrayList<Motif> motifs = new ArrayList<>();
        Motif m1 = new Motif();
        m1.setName("m1");
        Motif m2 = new Motif();
        m2.setName("m2");
        motifs.add(m1);
        motifs.add(m2);
        sim.setMotifs(motifs);

        //ACT
        String display = sim.toString();

        //ASSERT
        assertThat(display).isEqualTo(
                "Machine\n" +
                        "Id : 123\n" +
                        "Name : machine1\n" +
                        "Step : 0.000000\n" +
                        "NbRepetitions : 0\n" +
                        "\tMOTIF\n" +
                        "\tName : m1\n" +
                        "\tMOTIF\n" +
                        "\tName : m2\n");
    }

    @Test
    public void checkIdFormat(){
        //ARRANGE
        Machine sim = new Machine();
        String id = sim.getId();
        String idTmp = UUID.randomUUID().toString();

        assertThat(id.length()).isEqualTo(idTmp.length());

    }
}
