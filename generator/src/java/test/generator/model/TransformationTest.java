package generator.model;

import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.mock;

public class TransformationTest {
    @Test(expected = RuntimeException.class)
    public void applyTransformationShouldThrowIfTransformationIsntRecognized() {
        Transformation t = new Transformation();
        t.setType("error");
        Function f = mock(Function.class);
        f = t.withTransformation(f);
    }

    @Test
    public void applyHorizontalDilatationShouldChangeFunctionExpressionWithValueLT1() {
        float min = 0.5f;
        float max = 0.5f;
        Transformation t = new Transformation();
        t.setType("horizontalDilatation");
        t.setMin(min);
        t.setMax(max);

        Function f = new Function();
        String initValue = "2*x+1";
        f.setValue(initValue);
        f.setDuration(1);
        float duration = f.getDuration();

        Function fWithTransformation = t.withTransformation(f);
        String changedValue = fWithTransformation.getValue();
        float changedDuration = fWithTransformation.getDuration();

        String newExpr = fWithTransformation.getExp().getExpressionString();

        assertThat(changedDuration).isEqualTo(duration/0.5f);
        assertThat(changedValue).isEqualTo(newExpr);
        assertThat(changedValue).isEqualTo("2*(0.5*x)+1");
    }

    @Test
    public void applyHorizontalDilatationShouldChangeFunctionExpressionWithValueGE1() {
        float min = 5f;
        float max = 5f;
        Transformation t = new Transformation();
        t.setType("horizontalDilatation");
        t.setMin(min);
        t.setMax(max);

        Function f = new Function();
        String initValue = "2*x+1";
        f.setValue(initValue);
        f.setDuration(1);
        float duration = f.getDuration();

        Function fWithTransformation = t.withTransformation(f);
        String changedValue = fWithTransformation.getValue();
        float changedDuration = fWithTransformation.getDuration();

        String newExpr = fWithTransformation.getExp().getExpressionString();

        assertThat(changedDuration).isEqualTo(duration/5f);
        assertThat(changedValue).isEqualTo(newExpr);
        assertThat(changedValue).isEqualTo("2*(5.0*x)+1");
    }

    @Test
    public void applyVerticalDilatationShouldChangeFunctionExpressionWithValueLT1() {
        float min = 0.5f;
        float max = 0.5f;
        Transformation t = new Transformation();
        t.setType("verticalDilatation");
        t.setMin(min);
        t.setMax(max);

        Function f = new Function();
        String initValue = "2*x+1";
        f.setValue(initValue);
        f.setDuration(1);
        float duration = f.getDuration();

        Function fWithTransformation = t.withTransformation(f);
        String changedValue = fWithTransformation.getValue();
        float changedDuration = fWithTransformation.getDuration();

        String newExpr = fWithTransformation.getExp().getExpressionString();

        assertThat(changedDuration).isEqualTo(duration);
        assertThat(changedValue).isEqualTo(newExpr);
        assertThat(changedValue).isEqualTo("0.5*(2*x+1)");
    }

    @Test
    public void applyVerticalDilatationShouldChangeFunctionExpressionWithValueGT1() {
        float min = 5f;
        float max = 5f;
        Transformation t = new Transformation();
        t.setType("verticalDilatation");
        t.setMin(min);
        t.setMax(max);

        Function f = new Function();
        String initValue = "2*x+1";
        f.setValue(initValue);
        f.setDuration(1);
        float duration = f.getDuration();

        Function fWithTransformation = t.withTransformation(f);
        String changedValue = fWithTransformation.getValue();
        float changedDuration = fWithTransformation.getDuration();

        String newExpr = fWithTransformation.getExp().getExpressionString();

        assertThat(changedDuration).isEqualTo(duration);
        assertThat(changedValue).isEqualTo(newExpr);
        assertThat(changedValue).isEqualTo("5.0*(2*x+1)");
    }

    @Test
    public void applyverticalTranslationShouldChangeFunctionExpressionWithNegativeValue() {
        float min = -5f;
        float max = -5f;
        Transformation t = new Transformation();
        t.setType("verticalTranslation");
        t.setMin(min);
        t.setMax(max);

        Function f = new Function();
        String initValue = "2*x+1";
        f.setValue(initValue);
        f.setDuration(1);
        float duration = f.getDuration();

        Function fWithTransformation = t.withTransformation(f);
        String changedValue = fWithTransformation.getValue();
        float changedDuration = fWithTransformation.getDuration();

        String newExpr = fWithTransformation.getExp().getExpressionString();

        assertThat(changedDuration).isEqualTo(duration);
        assertThat(changedValue).isEqualTo(newExpr);
        assertThat(changedValue).isEqualTo("(2*x+1)-5.0");
    }

    @Test
    public void applyverticalTranslationShouldChangeFunctionExpressionWithPositiveValue() {
        float min = 5f;
        float max = 5f;
        Transformation t = new Transformation();
        t.setType("verticalTranslation");
        t.setMin(min);
        t.setMax(max);

        Function f = new Function();
        String initValue = "2*x+1";
        f.setValue(initValue);
        f.setDuration(1);
        float duration = f.getDuration();

        Function fWithTransformation = t.withTransformation(f);
        String changedValue = fWithTransformation.getValue();
        float changedDuration = fWithTransformation.getDuration();

        String newExpr = fWithTransformation.getExp().getExpressionString();

        assertThat(changedDuration).isEqualTo(duration);
        assertThat(changedValue).isEqualTo(newExpr);
        assertThat(changedValue).isEqualTo("(2*x+1)+5.0");
    }

    @Test
    public void applyOverthrowShouldChangeFunctionExpression() {
        Transformation t = new Transformation();
        t.setType("overthrow");

        Function f = new Function();
        String initValue = "2*x+1";
        f.setValue(initValue);
        f.setDuration(1);
        float duration = f.getDuration();

        Function fWithTransformation = t.withTransformation(f);
        String changedValue = fWithTransformation.getValue();
        float changedDuration = fWithTransformation.getDuration();

        String newExpr = fWithTransformation.getExp().getExpressionString();

        assertThat(changedDuration).isEqualTo(duration);
        assertThat(changedValue).isEqualTo(newExpr);
        assertThat(changedValue).isEqualTo("-1.0*(2*x+1)");
    }

    @Test
    public void checkIfATransformationHasCorrectValues(){
        float min = 1f;
        float max = 5f;
        Transformation t = new Transformation();
        t.setType("verticalTranslation");
        t.setMin(min);
        t.setMax(max);

        assertThat(t.getMin()).isEqualTo(min);
        assertThat(t.getMax()).isEqualTo(max);
        assertThat(t.getType()).isEqualTo("verticalTranslation");
    }
}