package generator.parser;

import generator.model.Machine;
import generator.model.Motif;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class GeneratorParserTest {
    private String filepath;
    private File file;
    @Before
    public void initXMLFile(){
        String content = "<MACHINE step=\"0.2\" nbRepetitions=\"2\" name=\"machine1\" >\n" +
                "    <MOTIF name=\"motif 1\" nbRepetitions=\"3\">\n" +
                "        <SEQUENCE durationMin=\"2\" durationMax=\"6\">\n" +
                "            <FUNCTION value=\"2*x+1\"/>\n" +
                "        </SEQUENCE>\n" +
                "        <SEQUENCE durationMin=\"1\" durationMax=\"5\">\n" +
                "            <FUNCTION value=\"b\" />\n" +
                "        </SEQUENCE>\n" +
                "        <SEQUENCE durationMin=\"2\" durationMax=\"2\">\n" +
                "            <FUNCTION value=\"-2*x+b\" />\n" +
                "        </SEQUENCE>\n" +
                "    </MOTIF>\n" +
                "    <MOTIF name=\"motif 2\">\n" +
                "        <SEQUENCE durationMin=\"3\" durationMax=\"10\">\n" +
                "            <FUNCTION value=\"x\" />\n" +
                "            <TRANSFORMATION type=\"horizontalDilatation\" min=\"0.8\" max=\"0.9\" />\n" +
                "            <TRANSFORMATION type=\"verticalDilatation\" min=\"1.3\" max=\"1.5\" />\n" +
                "        </SEQUENCE>\n" +
                "    </MOTIF>\n" +
                "</MACHINE>";

        filepath = "testGeneratorAlgo.xml";

        try (PrintStream out = new PrintStream(new FileOutputStream(filepath))) {
            out.print(content);
        } catch(Exception e){
            e.printStackTrace();
        }
        file = new File(filepath);
    }

    @After
    public void deleteXMLFile() {
        try {
            Files.delete(Paths.get(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkIfParsingGivesTheCorrectSimulation(){
        //ARRANGE
        double simStep = 0.2;
        int simNbRepetitions = 2;
        Motif motif = new Motif();
        motif.setName("motif 1");
        motif.setNbRepetitions(3);
        //ACT
        Machine sim = GeneratorParser.generateSimulation(file);
        //ASSERT
        assertThat(sim.getStep()).isEqualTo(simStep);
        assertThat(sim.getNbRepetitions()).isEqualTo(simNbRepetitions);
        assertThat(sim.getMotifs().get(0).getName()).isEqualTo(motif.getName());
        assertThat(sim.getMotifs().get(0).getNbRepetitions()).isEqualTo(motif.getNbRepetitions());
    }
}
