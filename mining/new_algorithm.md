# Ajouter un nouvel algorithme
## Bases
Il est possible d'intégrer des algorithmes de manière aisée dans l'interface de fouille SCAF.
Il existe des algorithmes pour :
 * La discrimination (package `mining.algorithms.discrimination`). 
 Ceux-ci permettent de séparer les données d'entrée en catégories ;
 * La discrétisation (package `mining.algorithms.discretisation`). Ils transforment des séries temporelles représentées
  par des valeurs réelles en listes symboliques discrètes ;
 * La fouille (package `mining.algorithms.mining`). À partir de séries discrètes, 
 ils extraient les séquences intéressantes et fréquentes.
 
Tous les types d'algorithmes fonctionnent de manière similaire :
 * Ils implémentent `mining.parameters.WithParameters`. Cela permet d'expliciter le fait qu'un algorithme peut avoir des
 paramètres et de les communiquer à l'interface ;
 * Ils sont gérés par un `mining.AlgorithmManager`. Chaque type d'algorithme à son propre Manager. Par exemple, 
 les algorithmes de discrimination ont pour manager un singleton de la classe
  `mining.algorithms.discrimination.DiscriminationManager`.
   C'est auprès de ces Managers qu'il faut enregistrer un nouvel algorithme.
   
## Paramètres

### Interface Parameter
Afin de pouvoir communiquer avec l'interface, les paramètres d'un algorithme sont encapsulés dans des objets implémentant 
l'interface générique `Parameter`. Celle-ci permet de :
 * récuperer la valeur du paramètre ;
 * affecter une nouvelle valeur au paramètre ;
 * récuperer le nom du paramètre. 
L'implémentation basique de cette interface est `ParameterImpl`. D'autres implémentations sont fournies, définissant par
 exemple des paramètres avec bornes `BoundedIntParameter` et `BoundedDoubleParameter`.

#### Nouveau type de paramètre
Il est possible de créer de nouveaux types de paramètres (par exemple, pour des énumérations). 
Pour que ceux-ci soient affichés dans l'interface, 
il est nécessaire de rajouter sa traduction en composant JavaFX. Cette opération se fait dans la classe 
`mining.gui.ParameterComponentFactory`. 
Il faut rajouter dans la `Map` statique `creators` une paire (clé, valeur) qui associe le type de paramètre a l'interface
 générique `ControlCreator`. Celle-ci n'a qu'une seule méthode qui prend en argument un type générique, qui doit être 
 le type du nouveau paramètre et retourne un composant JavaFX de type `Control`.
 
Suit un exemple d'ajout d'un paramètre de type `XParam`
 ```java
public final class ParameterComponentFactory {
    //...
    static {
        //...
        // Retourne un TextField avec pour valeur de départ la valeur du XParam
        ControlCreator<XParam> xCreator = (p) -> 
                    new javafx.scene.control.TextField(p.getValue().toString());
        //...
        creators.put(XParam.class, xCreator);
                
    }
}
```

### Interface WithParameter
Cette interface est implémentée par tous les types d'algorithmes. Elle définit plusieurs méthodes :
 * `parameters()` qui retourne l'ensemble des paramètres ;
 * `getParameterValue(String paramName)` qui permet d'accéder directement à la valeur d'un paramètre ;
 * `setParameterValue(String paramName, T value)` qui permet d'affecter une nouvelle valeur au paramètre.

Dans `ParameterUtils` sont définies plusieurs méthodes pouvant faciliter l'implémentation des deux dernières méthodes.

Exemple d'utilisation :

```java
public class MySuperConfigurable implements WithParameters {
    private final Set<Parameter<?>> params;
    public MySuperConfigurable() {
        params = new HashSet<>(Arrays.asList(
                    new BoundedIntParameter("symbolCount", 5, 1, 20),
                    new BoundedIntParameter("segmentCount", 5, 1, 1000)
        ));
    }
    
    @Override
    public Set<Parameter<?>> parameters() {
        return params;
    }

    @Override
    public <T> T getParameterValue(String name) {
        return ParameterUtils.getParameterValueFromSet(params, name);
    }

    @Override
    public <T> void setParameterValue(String name, T val) {
        ParameterUtils.setParameterValueFromSet(params, name, val);
    }
}
``` 
## Enregistrer un algorithme
Une fois l'algorithme écrit, il est nécessaire de l'enregistrer afin qu'il soit disponible dans l'interface.
Cela se passe dans la méthode `registerAlgorithms` de la classe `mining.gui.Main`.

Pour enregistrer un algorithme, il faut fournir un nom **unique** à la famille de cet algorithme,
 ainsi qu'une instance dudit algorithme. 
 Il faut ensuite appeler la méthode `registerAlgorithm` du singleton manager gérant la famille de l'algorithme.
 Cela donne pour un algorithme de **discrétisation** :
 ```java
DiscretisationManager.INSTANCE.registerAlgorithm("SAX", new SAXAlgorithm());
```
Une fois cette étape complétée, l'algorithme devrait être visible dans l'interface.

## Créer un algorithme de discrimination

Un algorithme de discrimination implémente l'interface `mining.algorithms.discrimination.DiscriminationAlgorithm`.
Celle-ci n'a qu'une méthode `discriminate` qui prend en entrée une collection de `TimeSeries` et 
retourne une `DiscriminatedSeriesIndexesMap`. Cette classe hérite de `HashMap`, et a pour clé des `String`s, 
qui représentent les catégories, et pour valeurs des `List<Integer>` qui représentent 
les indices dans la collection des séries temporelles associées à la catégorie.

Par mesure de simplicité, la classe `DiscriminationManager` propose la fonction `fromMappingFunction` qui à partir d'une
fonction prenant une `TimeSeries` en entrée et retournant une `String` représentant sa catégorie,
 définit un nouvel algorithme de discrimination.

## Créer un algorithme de discrétisation

Un algorithme de discrétisation implémente l'interface `mining.algorithms.discretisation.DiscretisationAlgorithm`.
Celle-ci n'a qu'une méthode `discretise` qui prend en entrée une collection de `TimeSeries` et 
retourne un objet de type `DiscretisationResults`. Celui-ci contient les bornes des symboles de discrétisation,
 ainsi que les séries temporelles discrétisées. **Note importante** : L'ordre des séries temporelles doit être conservé.

Une `DiscretisedSeries` contient une `List<Integer` qui correspond à l'ensemble des symboles de la série, ainsi qu'une
référence vers la `TimeSeries` correspondante.

## Créer un algorithme de fouille
Un algorithme de fouille de données implémente l'interface `mining.algorithms.mining.MiningAlgorithm`.
Celle-ci n'a qu'une méthode, `mine` qui prend un `DiscretisationResult` en entrée et retourne une `List<Sequence>`.

Une `Sequence` est composée de :
* Une `List<Integer>` qui correspond au motif trouvé ;
* Un `int` qui correspond au support de la séquence ;
* Une `List<DiscretisedSeries>` qui correspondent aux séries discrétisées où il est possible de trouver la séquence.