package mining;

import com.google.gson.*;
import framework.api.TimeSeries;
import framework.impl.TimeSeriesImpl;
import javafx.util.Pair;
import mining.algorithms.discretisation.DiscretisationAlgorithm;
import mining.algorithms.discretisation.DiscretisedSeries;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.mining.MiningAlgorithm;
import mining.algorithms.mining.Sequence;
import mining.gui.logging.LoggerController;
import mining.parameters.Parameter;
import mining.parameters.WithParameters;
import mining.pipeline.PipelineResults;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MiningUtils {

    /**
     * Returns the main logger, associated to the user interface
     *
     * @return
     */
    public static Logger getMainLogger() {
        return LoggerController.GUI_LOGGER;
    }

    /**
     * Loads multiple input files, with the same format as the generator output:
     * Each file's name has this structure:
     * machineName.uniqueSimulationName[possible_extension]
     * Each file contains multiple lines, each corresponding to a simulation run
     * Each line has the following structure:
     * runNumber/xStep/data
     * data is a list of number, each separated by a ;
     *
     * @param files the files to load
     * @return All the TimeSeries that were in the files
     */
    public static List<TimeSeries> loadTimeSeries(List<String> files) {
        final Logger logger = getMainLogger();
        logger.info("Loading files = " + files);
        long startTime = System.currentTimeMillis();
        final String filenameIdSeparatorRegex = "\\.";
        List<TimeSeries> series = files.parallelStream().collect(ArrayList<TimeSeries>::new, (vals, fileLocation) -> {
            getMainLogger().info("Loading " + fileLocation);
            final Path filePath = Paths.get(fileLocation);
            final String[] parts = filePath.getFileName().toString().split(filenameIdSeparatorRegex);
            final String machineName = parts[0];
            final String simuRun = parts.length == 2 ? parts[1].split("\\.")[0] : parts[1]; // if the separator is not a dot, we still have the file extension
            try (Stream<String> stream = Files.lines(filePath)) {
                logger.info("Reading file " + filePath);
                vals.addAll(readAllGeneratorInputFileLines(stream, machineName, simuRun));
            } catch (IOException e) {
                getMainLogger().severe("Could not read file " + fileLocation);
                e.printStackTrace();
            }
        }, ArrayList::addAll);
        logger.info("Finished loading files in " + (System.currentTimeMillis() - startTime) + "ms");
        return series;
    }

    public static TimeSeries readGeneratorInputLine(String line, String machineName, String simuRun) {
        final String args[] = line.split("/", 3);
        final List<Double> values = Arrays.stream(args[2].split(";"))
                .map(Double::parseDouble)
                .collect(Collectors.toList());
        final double step = Double.parseDouble(args[1]);
        final String simuId = simuRun + "_" + args[0];
        return new TimeSeriesImpl(values, step, machineName, simuId);
    }

    public static List<TimeSeries> readAllGeneratorInputFileLines(Stream<String> lines, String machineName, String simuRun) {
        return lines.map(line -> readGeneratorInputLine(line, machineName, simuRun)).collect(Collectors.toList());
    }

    private static JsonObject parametrableAsJson(String name, WithParameters parametrable) {
        Objects.requireNonNull(parametrable);
        final JsonObject algJson = new JsonObject();
        algJson.add("name", new JsonPrimitive(name));
        JsonObject parametersJson = new JsonObject();
        for (Parameter<?> param : parametrable.parameters()) {
            parametersJson.add(param.name(), param.valueAsJson());
        }
        algJson.add("parameters", parametersJson);
        return algJson;
    }

    private static JsonObject sequenceAsJson(Sequence s) {
        Objects.requireNonNull(s);
        final JsonObject sequenceJson = new JsonObject();
        sequenceJson.add("support", new JsonPrimitive(s.getSupport()));
        JsonArray sequenceItemsJson = new JsonArray(s.getSeq().size());
        for (Integer i : s.getSeq()) {
            sequenceItemsJson.add(i);
        }
        sequenceJson.add("sequence", sequenceItemsJson);
        JsonArray matchingSeriesNamesJson = new JsonArray(s.getMatchingSeries().size());
        for (DiscretisedSeries discr : s.getMatchingSeries()) {
            TimeSeries original = discr.getOriginalSeries();
            matchingSeriesNamesJson.add(original.name() + "/" + original.id());
        }
        sequenceJson.add("matching", matchingSeriesNamesJson);
        return sequenceJson;
    }

    /**
     * Transforms a mining run into a JSON structure
     *
     * @param discretisationAlgo
     * @param discriminationAlgo
     * @param miningAlgo
     * @param results
     * @return a JSON Object representing a mining run
     */
    public static JsonObject miningRunAsJson(Pair<String, DiscretisationAlgorithm> discretisationAlgo,
                                             Pair<String, DiscriminationAlgorithm> discriminationAlgo,
                                             Pair<String, MiningAlgorithm> miningAlgo, PipelineResults results) {
        final JsonObject resultJson = new JsonObject();
        getMainLogger().warning("discretisation = " + discretisationAlgo);
        getMainLogger().warning("mining = " + miningAlgo);

        resultJson.add("discretisation", parametrableAsJson(discretisationAlgo.getKey(),
                discretisationAlgo.getValue()));
        resultJson.add("discrimination", parametrableAsJson(discriminationAlgo.getKey(),
                discriminationAlgo.getValue()));
        resultJson.add("mining", parametrableAsJson(miningAlgo.getKey(),
                miningAlgo.getValue()));
        final JsonObject categoriesJson = new JsonObject();
        for (Map.Entry<String, List<Sequence>> cat : results.getSequencesByCategory().entrySet()) {
            final JsonObject categoryJson = new JsonObject();
            final JsonArray sequenceArray = new JsonArray(cat.getValue().size());
            for (final Sequence s : cat.getValue()) {
                sequenceArray.add(sequenceAsJson(s));
            }
            categoryJson.add("sequences", sequenceArray);
            categoriesJson.add(cat.getKey(), categoryJson);
        }
        resultJson.add("categories", categoriesJson);
        return resultJson;
    }

    /**
     * Saves a json object into a file
     *
     * @param filename the file to save to
     * @param json     the json
     * @throws IOException if the file could not be opened
     */
    public static void saveJson(String filename, JsonObject json) throws IOException {
        try (Writer writer = new OutputStreamWriter(new FileOutputStream(filename), StandardCharsets.UTF_8)) {
            getMainLogger().info("Writing to file " + filename);
            final Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(json, writer);
        }
    }

    /**
     * Returns a copy of the object via serialization->deserialization
     *
     * @param object the object to copy. must be {@link Serializable}.
     * @param <T>    the type of the object
     * @return a copy of the object if everything went well, null otherwise
     */
    public static <T> T makeCopy(T object) {
        final ByteArrayOutputStream bos =
                new ByteArrayOutputStream();
        ByteArrayInputStream bin;

        try (final ObjectOutputStream oos = new ObjectOutputStream(bos)) {
            oos.writeObject(object);
            oos.flush();
            bin = new ByteArrayInputStream(bos.toByteArray());
        } catch (IOException e) {
            MiningUtils.getMainLogger().severe("Error while cloning and writing object : " + e);
            return null;
        }
        try (final ObjectInputStream ois = new ObjectInputStream(bin)) {
            return (T) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            MiningUtils.getMainLogger().severe("Error while cloning and reading object : " + e);
            return null;
        }

    }
}
