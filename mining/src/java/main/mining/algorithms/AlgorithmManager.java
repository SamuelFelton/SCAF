package mining.algorithms;

import java.util.*;

/**
 * A generic algorithm manager.
 * It holds multiple algorithms of the same type and associates them to a name.
 * @param <AlgorithmType> the type of the contained algorithms
 */
public class AlgorithmManager<AlgorithmType> {
    private final Map<String, AlgorithmType> algorithms;

    public AlgorithmManager() {
        algorithms = new HashMap<>();
    }

    /**
     * Registers a new algorithm with name {@code name}.
     * If there already is an algorithm with the same name, an {@link IllegalArgumentException} is thrown.
     * @param name the name of the algorithm
     * @param algorithm the algorithm to register
     */
    public void registerAlgorithm(String name, AlgorithmType algorithm) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(algorithm);
        if (algorithms.putIfAbsent(name, algorithm) != null) {
            throw new IllegalArgumentException("An algorithm for the name " + name + " is already registered");
        }
    }

    /**
     *
     * @return the Set of registered algorithms names
     */
    public Set<String> algorithmNames() {
        return algorithms.keySet();
    }

    /**
     * Queries this manager to get an Algorithm if it present, Optional.empty() otherwise
     * @param name the name of the queried algorithm
     * @return an {@link Optional} containing or not the algorithm
     */
    public Optional<AlgorithmType> get(String name) {
        return Optional.ofNullable(algorithms.get(name));
    }

    /**
     * Tries to get the name of an algorithm if it was registered
     * @param algo the algorithm of which we want to know the name
     * @return an {@link Optional} containing or not the name
     */
    public Optional<String> nameOf(AlgorithmType algo) {
        return algorithms.entrySet().stream().filter(e -> e.getValue().equals(algo)).map(Map.Entry::getKey).findFirst();
    }

    /**
     * Removes every algorithm that was held in this manager
     */
    public void clearAlgorithms() {
        algorithms.clear();
    }

}
