package mining.algorithms.discretisation;

import framework.api.TimeSeries;
import mining.parameters.WithParameters;

import java.util.Collection;

public interface DiscretisationAlgorithm extends WithParameters {
    DiscretisationResults discretise(Collection<TimeSeries> series);
}
