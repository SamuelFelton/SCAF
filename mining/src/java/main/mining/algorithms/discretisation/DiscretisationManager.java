package mining.algorithms.discretisation;

import mining.algorithms.AlgorithmManager;

public final class DiscretisationManager extends AlgorithmManager<DiscretisationAlgorithm> {
    public static final DiscretisationManager INSTANCE = new DiscretisationManager();

    private DiscretisationManager() {
    }
}
