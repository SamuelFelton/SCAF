package mining.algorithms.discretisation;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class DiscretisationResults {
    private final Map<Integer, Bounds> symbolBounds;
    private final List<DiscretisedSeries> series;

    public DiscretisationResults(Map<Integer, Bounds> symbolBounds, List<DiscretisedSeries> series) {
        this.symbolBounds = symbolBounds;
        this.series = series;
    }

    public Map<Integer, Bounds> getSymbolBounds() {
        return Collections.unmodifiableMap(symbolBounds);
    }

    public List<DiscretisedSeries> getSeries() {
        return Collections.unmodifiableList(series);
    }

    public static class Bounds implements Serializable {
        public final double lower, upper;

        public Bounds(double l, double u) {
            lower = l;
            upper = u;
        }

        @Override
        public String toString() {
            return String.format("[%f; %f]", lower, upper);
        }
    }
}
