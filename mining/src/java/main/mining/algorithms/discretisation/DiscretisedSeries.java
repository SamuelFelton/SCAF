package mining.algorithms.discretisation;

import framework.api.TimeSeries;
import javafx.util.Pair;
import mining.algorithms.mining.Sequence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiscretisedSeries {
    private final List<Integer> symbols;
    private final TimeSeries originalSeries;

    public DiscretisedSeries(List<Integer> symbols, TimeSeries originalSeries) {
        this.symbols = symbols;
        this.originalSeries = originalSeries;
    }

    public List<Integer> getSymbols() {
        return symbols;
    }

    public TimeSeries getOriginalSeries() {
        return originalSeries;
    }

    /**
     * Find the occurrences of a sequence in this discretised series. the returned bounds are inclusive
     * @param s the sequence to search for
     * @return the list containing all of the occurences of the parameter sequence.
     * Each pair corresponds to the lower and upper bound at which the occurrence is seen. the list is empty if no occurrence is seen
     */
    public List<Pair<Integer, Integer>> getOccurrences(Sequence s) {
        final List<Pair<Integer, Integer>> occurences = new ArrayList<>();
        final List<Integer> seqItems = s.getSeq();
        /*Iterate to match head of sequence*/
        for(int i = 0; i <= symbols.size() - seqItems.size(); ++i) {
            if(symbols.get(i).equals(seqItems.get(0))) {
                if(seqItems.size() == 1) {
                    occurences.add(new Pair<>(i, i));
                } else {
                    final List<Integer> upperBounds = searchOneSeqOccurrence(i + 1, 1, seqItems);
                    for(final Integer upper : upperBounds) {
                        occurences.add(new Pair<>(i, upper));
                    }
                }
            }
        }
        return occurences;
    }

    /**
     * Search for the sequence from a starting position. The bounds are inclusive
     * @param startIdx the index of the start in series
     * @param seqIdx the index where we're at in the series
     * @param s the sequence symbols
     * @return the list of upper bounds where the sequence occurrences were found
     */
    private List<Integer> searchOneSeqOccurrence(int startIdx, int seqIdx, List<Integer> s) {
        /*End recursion*/
        if(seqIdx >= s.size() || startIdx >= symbols.size() || symbols.size() - startIdx < s.size() - seqIdx) {
            return new ArrayList<>(0);
        }
        /*We found one symbol*/
        if(symbols.get(startIdx).equals(s.get(seqIdx))) {
            //as it is equal, we first take it as recognised, then we explore further to see if we cannot another occurrence
            final List<Integer> result = searchOneSeqOccurrence(startIdx + 1, seqIdx + 1, s);
            result.addAll(searchOneSeqOccurrence(startIdx + 1, seqIdx, s));
            if(seqIdx >= s.size() - 1) {
                result.add(startIdx);
            }
            return result;
        } else {
            return searchOneSeqOccurrence(startIdx + 1, seqIdx, s);
        }

    }
}
