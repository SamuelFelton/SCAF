package mining.algorithms.discretisation;

import ca.pfv.spmf.algorithms.timeseries.sax.AlgoSAX;
import ca.pfv.spmf.algorithms.timeseries.sax.SAXSymbol;
import framework.api.TimeSeries;
import mining.MiningUtils;
import mining.parameters.BoundedIntParameter;
import mining.parameters.Parameter;
import mining.parameters.ParameterUtils;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SAXAlgorithm implements DiscretisationAlgorithm {
    private static final long serialVersionUID = 4716446484062972533L;
    Set<Parameter<?>> params;

    public SAXAlgorithm() {
        params = new HashSet<>(Arrays.asList(
                new BoundedIntParameter("symbolCount", 5, 1, 20),
                new BoundedIntParameter("segmentCount", 5, 1, 1000)
        ));
    }

    @Override
    public DiscretisationResults discretise(Collection<TimeSeries> seriesColl) {
        Logger logger = MiningUtils.getMainLogger();
        logger.info("Running SAX algorithm");
        List<TimeSeries> series = seriesColl instanceof List ? (List) seriesColl : new ArrayList<>(seriesColl);
        List<ca.pfv.spmf.algorithms.timeseries.TimeSeries> spmfSeries =
                series.parallelStream().map((TimeSeries t) -> new ca.pfv.spmf.algorithms.timeseries.TimeSeries(t.toValuesArray(), t.name() + t.id())).collect(Collectors.toList());
        Map<Integer, DiscretisationResults.Bounds> symbols = new HashMap<>();
        List<DiscretisedSeries> discretisedSeries = new ArrayList<>();
        AlgoSAX sax = new ca.pfv.spmf.algorithms.timeseries.sax.AlgoSAX();
        try {
            SAXSymbol[][] saxResult = sax.runAlgorithm(spmfSeries,
                    getParameterValue("segmentCount"),
                    getParameterValue("symbolCount"),
                    false);
            logger.info("SAX symbols :");
            for (final SAXSymbol s : sax.getSymbols()) {
                logger.info("\t" + s.symbol + ": [" + s.lowerBound + ", " + s.upperBound + "]");
                DiscretisationResults.Bounds b = new DiscretisationResults.Bounds(s.lowerBound, s.upperBound);
                symbols.putIfAbsent(s.symbol, b);
            }
            int i = 0;
            for (final SAXSymbol[] seriesSax : saxResult) {
                List<Integer> symbolsSax = new ArrayList<>();
                for (final SAXSymbol symbolSax : seriesSax) {
                    symbolsSax.add(symbolSax.symbol);
                }
                TimeSeries originalSeries = series.get(i);
                discretisedSeries.add(new DiscretisedSeries(symbolsSax, originalSeries));
                ++i;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new DiscretisationResults(symbols, discretisedSeries);
    }

    @Override
    public Set<Parameter<?>> parameters() {
        return params;
    }

    @Override
    public <T> T getParameterValue(String name) {
        return ParameterUtils.getParameterValueFromSet(params, name);
    }

    @Override
    public <T> void setParameterValue(String name, T val) {
        ParameterUtils.setParameterValueFromSet(params, name, val);
    }
}
