package mining.algorithms.discrimination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class DiscriminatedSeriesIndexesMap extends HashMap<String, List<Integer>> {
    public DiscriminatedSeriesIndexesMap(Set<String> categoryNames) {
        if (categoryNames != null) {
            categoryNames.forEach(c -> put(c, new ArrayList<>()));
        }
    }
}
