package mining.algorithms.discrimination;

import framework.api.TimeSeries;
import mining.parameters.WithParameters;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface of a discrimination algorithm.
 * A discrimination algorithm separates different {@link TimeSeries} into categories.
 * Categories can vary between algorithms.
 */
public interface DiscriminationAlgorithm extends WithParameters, Serializable {
    /**
     * Discriminates multiple {@link TimeSeries} which are split and returned in a {@link DiscriminatedSeriesIndexesMap}.
     *
     * @param series the series to discriminate. No element should be null
     * @return the series discriminated by category.
     */
    DiscriminatedSeriesIndexesMap discriminate(Collection<TimeSeries> series);
}
