package mining.algorithms.discrimination;

import framework.api.TimeSeries;
import mining.algorithms.AlgorithmManager;
import mining.parameters.Parameter;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class DiscriminationManager extends AlgorithmManager<DiscriminationAlgorithm> implements Serializable {

    public static final DiscriminationManager INSTANCE = new DiscriminationManager();
    private static final long serialVersionUID = 5257875487400896124L;

    private DiscriminationManager() {
    }

    /**
     * Creates a new discrimination algorithm, which categorises {@link TimeSeries} independently.
     * This algorithm is considered parameterless, and thus parameters returns an empty set.
     *
     * @param categories the full set of categories
     * @param mapper     the function which, given a TimeSeries, returns its category
     * @return a DiscriminationAlgorithm which evaluates {@link TimeSeries} with {@code mapper}
     */
    public static <F extends Function<TimeSeries, String> & Serializable> DiscriminationAlgorithm fromMappingFunction(Set<String> categories, F mapper) {
        return new DiscriminationAlgorithm() {

            private static final long serialVersionUID = -1448303158973987822L;

            @Override
            public DiscriminatedSeriesIndexesMap discriminate(Collection<TimeSeries> series) {
                final DiscriminatedSeriesIndexesMap res = new DiscriminatedSeriesIndexesMap(categories);
                List<String> mappedCategories = series.parallelStream().map(mapper).collect(Collectors.toList());
                int i = 0;
                for (final TimeSeries t : series) {
                    String cat = mappedCategories.get(i);
                    res.putIfAbsent(cat, new ArrayList<>());
                    res.get(mappedCategories.get(i)).add(i);
                    ++i;
                }
                return res;
            }

            @Override
            public Set<Parameter<?>> parameters() {
                return Collections.emptySet();
            }

            @Override
            public <T> T getParameterValue(String name) {
                return null;
            }

            @Override
            public <T> void setParameterValue(String name, T value) {
                throw new IllegalArgumentException("Cannot set parameter, there is no parameter for this implementation");
            }
        };
    }
}
