package mining.algorithms.mining;


import ca.pfv.spmf.algorithms.sequentialpatterns.prefixspan.AlgoBIDEPlus;
import ca.pfv.spmf.algorithms.sequentialpatterns.prefixspan.SequenceDatabase;
import ca.pfv.spmf.algorithms.sequentialpatterns.prefixspan.SequentialPattern;
import ca.pfv.spmf.algorithms.sequentialpatterns.prefixspan.SequentialPatterns;
import mining.MiningUtils;
import mining.algorithms.discretisation.DiscretisationResults;
import mining.algorithms.discretisation.DiscretisedSeries;
import mining.parameters.BoundedDoubleParameter;
import mining.parameters.Parameter;
import mining.parameters.ParameterUtils;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class BIDEAlgorithm implements MiningAlgorithm {
    private static final long serialVersionUID = 3682701951927055756L;
    Set<Parameter<?>> params;

    public BIDEAlgorithm() {
        params = new HashSet<>(Arrays.asList(
                new BoundedDoubleParameter("minSup", 0.5, 0.0, 1.0, 0.01)
        ));
    }

    @Override
    public List<Sequence> mine(DiscretisationResults results) {
        Logger logger = MiningUtils.getMainLogger();
        //logger.info("Running BIDE+ algorithm");
        List<DiscretisedSeries> discretisedSeries = results.getSeries();
        SequenceDatabase spmfDb = convertToDatabase(discretisedSeries);
        AlgoBIDEPlus algo = new AlgoBIDEPlus();
        SequentialPatterns patterns = algo.runAlgorithm(spmfDb, getParameterValue("minSup"));
        List<Sequence> res = new ArrayList<>();
        //logger.info("BIDE+ sequences");
        for (final List<SequentialPattern> l : patterns.getLevels()) {
            for (final SequentialPattern s : l) {
                List<DiscretisedSeries> matching = new ArrayList<>();
                for (final Integer i : s.getSequenceIDs()) {
                    matching.add(discretisedSeries.get(i));
                }
                List<Integer> frequentSequence = new ArrayList<>();
                s.getItemsets().forEach(item -> frequentSequence.addAll(item.getItems()));
                //logger.info("Sequence: " + frequentSequence + ", support: " + s.getAbsoluteSupport());
                res.add(new Sequence(frequentSequence, s.getAbsoluteSupport(), matching));
            }
        }
        return res;
    }

    private int[] convertToSequence(List<Integer> integers) {
        int[] s = new int[integers.size() * 2 + 1];
        for (int i = 0; i < integers.size(); ++i) {
            s[i * 2] = integers.get(i);
            s[i * 2 + 1] = -1; // itemset separator
        }
        s[s.length - 1] = -2; // end of seq
        return s;
    }

    private SequenceDatabase convertToDatabase(List<DiscretisedSeries> series) {
        List<int[]> sequences = series.parallelStream().map(ds -> convertToSequence(ds.getSymbols())).collect(Collectors.toList());
        SequenceDatabase db = new SequenceDatabase();
        db.setSequences(sequences);
        return db;
    }

    @Override
    public Set<Parameter<?>> parameters() {
        return params;
    }

    @Override
    public <T> T getParameterValue(String name) {
        return ParameterUtils.getParameterValueFromSet(params, name);
    }

    @Override
    public <T> void setParameterValue(String name, T val) {
        ParameterUtils.setParameterValueFromSet(params, name, val);
    }
}
