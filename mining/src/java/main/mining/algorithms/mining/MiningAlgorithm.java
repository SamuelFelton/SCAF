package mining.algorithms.mining;

import mining.algorithms.discretisation.DiscretisationResults;
import mining.parameters.WithParameters;

import java.util.List;

public interface MiningAlgorithm extends WithParameters {
    List<Sequence> mine(DiscretisationResults series);
}
