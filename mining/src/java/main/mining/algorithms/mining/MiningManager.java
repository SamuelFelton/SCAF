package mining.algorithms.mining;

import mining.algorithms.AlgorithmManager;

public final class MiningManager extends AlgorithmManager<MiningAlgorithm> {
    public static final MiningManager INSTANCE = new MiningManager();

    private MiningManager() {
    }
}
