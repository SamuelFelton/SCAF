package mining.algorithms.mining;

import mining.algorithms.discretisation.DiscretisedSeries;

import java.util.Collections;
import java.util.List;

public class Sequence {
    private final List<Integer> seq;
    private final int support;
    private final List<DiscretisedSeries> matchingSeries;

    public Sequence(List<Integer> s, int supp, List<DiscretisedSeries> seriesNames) {
        seq = Collections.unmodifiableList(s);
        support = supp;
        matchingSeries = Collections.unmodifiableList(seriesNames);
    }

    public List<Integer> getSeq() {
        return Collections.unmodifiableList(seq);
    }

    public int getSupport() {
        return support;
    }

    public List<DiscretisedSeries> getMatchingSeries() {
        return Collections.unmodifiableList(matchingSeries);
    }

    public String stringRepresentation() {
        StringBuilder builder = new StringBuilder();
        builder.append('(');
        for (final Integer i : seq) {
            builder.append('{');
            builder.append(i);
            builder.append("}, ");
        }
        builder.replace(builder.length() - 2, builder.length() - 1, ")");
        return builder.toString();
    }
}
