package mining.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import mining.algorithms.AlgorithmManager;
import mining.algorithms.discretisation.DiscretisationAlgorithm;
import mining.algorithms.discretisation.DiscretisationManager;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.discrimination.DiscriminationManager;
import mining.algorithms.mining.MiningAlgorithm;
import mining.algorithms.mining.MiningManager;
import mining.parameters.Parameter;
import mining.parameters.WithParameters;
import mining.pipeline.DefaultMiningPipeline;
import mining.pipeline.DiscretiseThenSplitPipeline;
import mining.pipeline.MiningPipeline;

import java.util.Optional;

public class AlgorithmOptionsController {


    public ComboBox<PipelineChoice> pipelineComboBox;

    interface PipelineChoice {
        MiningPipeline get();
    }

    interface OnComputeListener {
        void compute(MiningPipeline pipeline,
                     DiscriminationAlgorithm discriminationAlgorithm,
                     DiscretisationAlgorithm discretisationAlgorithm,
                     MiningAlgorithm miningAlgorithm);
    }

    private OnComputeListener onComputeListener;
    @FXML
    public Button buttonCompute;
    @FXML
    public GridPane discriminationPane;
    @FXML
    public ComboBox<String> discriminationComboBox;
    @FXML
    public GridPane miningPane;
    @FXML
    public GridPane discretisationPane;
    @FXML
    public ComboBox<String> discretisationComboBox;
    @FXML
    public ComboBox<String> miningComboBox;
    @FXML
    public TabPane optionTabs;

    @FXML
    public void initialize() {
        prepareTab(DiscriminationManager.INSTANCE, discriminationComboBox, discriminationPane);
        prepareTab(DiscretisationManager.INSTANCE, discretisationComboBox, discretisationPane);
        prepareTab(MiningManager.INSTANCE, miningComboBox, miningPane);
        buttonCompute.setOnAction(event -> {
            if (onComputeListener != null) {
                MiningPipeline pipeline = pipelineComboBox.getSelectionModel().getSelectedItem().get();
                Optional<DiscriminationAlgorithm> discriOpt = getFromComboBox(discriminationComboBox, DiscriminationManager.INSTANCE);
                Optional<DiscretisationAlgorithm> discreOpt = getFromComboBox(discretisationComboBox, DiscretisationManager.INSTANCE);
                Optional<MiningAlgorithm> miningOpt = getFromComboBox(miningComboBox, MiningManager.INSTANCE);
                if (checkAlgorithmSelectedOrDisplayMessage(discriOpt, "discrimination") &&
                        checkAlgorithmSelectedOrDisplayMessage(discreOpt, "discretisation") &&
                        checkAlgorithmSelectedOrDisplayMessage(miningOpt, "mining")) {
                    onComputeListener.compute(pipeline, discriOpt.get(), discreOpt.get(), miningOpt.get());
                }
            } else {
                throw new IllegalStateException("There was no listener to call for the compute");
            }
        });
        pipelineComboBox.getItems().addAll(
                new PipelineChoice() {
                    @Override
                    public MiningPipeline get() {
                        return new DefaultMiningPipeline();
                    }

                    @Override
                    public String toString() {
                        return "Discretise -> Mine -> Split";
                    }
                },
                new PipelineChoice() {
                    @Override
                    public MiningPipeline get() {
                        return new DiscretiseThenSplitPipeline();
                    }

                    @Override
                    public String toString() {
                        return "Discretise -> Split -> Mine";
                    }
                }
        );
        pipelineComboBox.getSelectionModel().selectFirst();
    }

    private <T> Optional<T> getFromComboBox(ComboBox<String> comboBox, AlgorithmManager<T> manager) {
        Optional<String> selected = Optional.ofNullable(comboBox.getSelectionModel().getSelectedItem());
        return selected.isPresent() ? manager.get(selected.get()) : Optional.empty();
    }

    private boolean checkAlgorithmSelectedOrDisplayMessage(Optional<?> o, String algorithmType) {
        if (o.isPresent()) return true;
        GUIUtils.makeError("No " + algorithmType + " algorithm selected",
                "There was no algorithm selected for the " + algorithmType + " category, please selected one to mine the data",
                "").showAndWait();
        return false;
    }

    private <T extends WithParameters> void prepareTab(AlgorithmManager<T> manager, ComboBox<String> comboBox, GridPane pane) {
        populateComboBox(comboBox, manager);
        comboBox.setOnAction(getEventForComboBoxSelection(comboBox, manager, pane));


        if(!manager.algorithmNames().isEmpty()) {
            comboBox.getSelectionModel().selectFirst();
            getEventForComboBoxSelection(comboBox, manager, pane).handle(null);
        }
    }

    private void populateComboBox(ComboBox<String> cb, AlgorithmManager<?> manager) {
        cb.getItems().addAll(manager.algorithmNames());
    }

    private void populateTabWithAlgorithmOptions(GridPane gp, WithParameters parametrable) {
        int algoSelectionRow = 0;
        gp.getChildren().removeIf(n -> GridPane.getRowIndex(n) != algoSelectionRow);
        int i = 1;
        for (Parameter p : parametrable.parameters()) {
            gp.add(new Label(p.name() + ": "), 0, i);
            try {
                gp.add(ParameterComponentFactory.controlFromParameter(p), 1, i);
            } catch (IllegalArgumentException e) {
                gp.add(new Label(p.value().toString()), 1, i);
                GUIUtils.makeError("No available control for type: " + p.getClass(),
                        e.getMessage(), "").showAndWait();
            }
            ++i;
        }
        optionTabs.requestLayout(); // ask for a redraw to show the parameters
    }

    private <T extends WithParameters> EventHandler<ActionEvent> getEventForComboBoxSelection(ComboBox<String> comboBox, AlgorithmManager<T> manager, GridPane pane) {
        return event -> {
            String selectedValue = comboBox.getSelectionModel().getSelectedItem();
            Optional<T> optAlgo = manager.get(selectedValue);
            if (!optAlgo.isPresent()) {
                GUIUtils.makeError("Wrong algorithm selection",
                        "Something happened while selecting an algorithm!",
                        "The selected value couldn't be found in the corresponding algorithm manager").showAndWait();
                System.exit(200);
            }
            populateTabWithAlgorithmOptions(pane, optAlgo.get());

        };
    }


    public void setOnComputeListener(OnComputeListener onComputeListener) {
        this.onComputeListener = onComputeListener;
    }
}
