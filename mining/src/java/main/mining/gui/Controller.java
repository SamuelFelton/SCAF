package mining.gui;

import com.google.gson.JsonObject;
import framework.api.TimeSeries;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Pair;
import mining.MiningUtils;
import mining.algorithms.discretisation.DiscretisationAlgorithm;
import mining.algorithms.discretisation.DiscretisationManager;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.discrimination.DiscriminationManager;
import mining.algorithms.mining.MiningAlgorithm;
import mining.algorithms.mining.MiningManager;
import mining.algorithms.mining.Sequence;
import mining.gui.logging.LoggerController;
import mining.gui.logging.LoggerHandler;
import mining.pipeline.MiningPipeline;
import mining.pipeline.PipelineResults;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Controller implements SequencesByCategoryDisplayController.OnSequenceTableRowSelectedListener, AlgorithmOptionsController.OnComputeListener {
    @FXML
    public MenuItem openFilesMenuItem;
    @FXML
    public MenuBar menuBar;
    @FXML
    public MenuItem saveMenuItem;
    @FXML
    public MenuItem closeMenuItem;
    @FXML
    private MenuItem loggerMenuItem;

    private Stage loggerStage;

    @FXML
    private SplitPane bottomSplitPane;
    @FXML
    private SplitPane topSplitPane;
    @FXML
    private Pane pane;
    @FXML
    private SplitPane horizontalSplitPane;

    private SequencesByCategoryDisplayController sequencesByCategoryDisplayController;
    private AlgorithmOptionsController algorithmOptionsController;
    private CurveVisualizationController curveVisualizationController;
    private ItemsetsVisualizationController itemsetsVisualizationController;

    private Task<PipelineResults> miningTask;
    private MiningRunData miningResults;
    private List<TimeSeries> data;

    private final BooleanProperty hasDataLoaded = new SimpleBooleanProperty(false);
    private final BooleanProperty hasMiningCompleted = new SimpleBooleanProperty(false);

    private Properties properties;

    @FXML
    public void initialize() {
        this.horizontalSplitPane.prefWidthProperty().bind(pane.widthProperty());
        this.horizontalSplitPane.prefHeightProperty().bind(pane.heightProperty());
        bottomSplitPane.prefHeightProperty().bind(bottomSplitPane.maxHeightProperty());

        addSequencesDisplayPanel();
        addOptionsPanel();
        addItemsetsVisPanel();
        addCurveVisPanel();
        setupLoggerStage();

        saveMenuItem.disableProperty().bind(Bindings.not(hasMiningCompleted));
        closeMenuItem.setOnAction(e -> Platform.exit());
        properties = new Properties();
        try (FileInputStream propInput = new FileInputStream("scaf.properties")) {
            properties.load(propInput);
        } catch (FileNotFoundException e) {
            MiningUtils.getMainLogger().warning("Properties file not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Save properties on exit
        pane.sceneProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue == null && newValue != null) {
                newValue.windowProperty().addListener((observable1, oldWin, newWin) -> {
                    if (newWin != null) {
                        newWin.setOnCloseRequest(e -> {
                            try (FileOutputStream fos = new FileOutputStream("scaf.properties", false)) {
                                properties.store(fos, "SCAF GUI properties");
                            } catch (IOException e1) {
                                MiningUtils.getMainLogger().severe("Could not save scaf.properties");
                                e1.printStackTrace();
                            }
                            Platform.exit();
                        });
                    }
                });
            }
        });
    }

    /**
     * Listener called when a row in the itemset table is selected.
     * For this to happen this instance must be bound to the correct controller.
     *
     * @param category the sequence's category
     * @param s        the sequence
     */
    @Override
    public void rowSelected(String category, Sequence s) {
        itemsetsVisualizationController.displayResults(s);
    }

    /**
     * Listener called when the user asks for a computation.
     * This instance must be registered to the algorithmOptions for it to be called.
     * If there already is a computation being performed, it is cancelled and this one is run instead.
     * The computation is performed on a different thread.
     * At the end of the computation, if there was no error, the resulting data is stored in {@link #data}.
     * Otherwise an error is displayed to the user.
     *
     * @param pipeline                the pipeline with which to process the data
     * @param discriminationAlgorithm
     * @param discretisationAlgorithm
     * @param miningAlgorithm
     */
    @Override
    public void compute(MiningPipeline pipeline, DiscriminationAlgorithm discriminationAlgorithm, DiscretisationAlgorithm discretisationAlgorithm, MiningAlgorithm miningAlgorithm) {
        Objects.requireNonNull(pipeline);
        Objects.requireNonNull(discretisationAlgorithm);
        Objects.requireNonNull(discriminationAlgorithm);
        Objects.requireNonNull(miningAlgorithm);

        if (miningTask != null && miningTask.isRunning()) {
            miningTask.cancel(true);
            Logger logger = MiningUtils.getMainLogger();
            logger.warning("Canceled mining task");
        }
        if (data == null) {
            Alert noDataErr = GUIUtils.makeError("No input data was loaded",
                    "You must load data to run mining",
                    "To load data, open the menu and select the \"Open\" option");
            noDataErr.showAndWait();
            return;
        }
        miningTask = new Task<PipelineResults>() {
            @Override
            protected PipelineResults call() {
                return pipeline.run(data, discriminationAlgorithm, discretisationAlgorithm, miningAlgorithm);
            }
        };
        miningTask.setOnSucceeded((event) -> {
            Pair<String, DiscriminationAlgorithm> discriPair = new Pair<>(
                    DiscriminationManager.INSTANCE.nameOf(discriminationAlgorithm).get(), discriminationAlgorithm);
            Pair<String, DiscretisationAlgorithm> discrePair = new Pair<>(
                    DiscretisationManager.INSTANCE.nameOf(discretisationAlgorithm).get(), discretisationAlgorithm);
            Pair<String, MiningAlgorithm> miningPair = new Pair<>(
                    MiningManager.INSTANCE.nameOf(miningAlgorithm).get(), miningAlgorithm);

            miningResults = new MiningRunData(discrePair, discriPair, miningPair, miningTask.getValue());
            hasMiningCompleted.setValue(true);
            sequencesByCategoryDisplayController.displayResults(miningTask.getValue());
            curveVisualizationController.displayResults(data, miningTask.getValue());
            itemsetsVisualizationController.clear();
        });
        miningTask.setOnFailed(event -> {
            Logger g = MiningUtils.getMainLogger();
            g.severe("=============Error while running mining=============");
            miningTask.getException().printStackTrace();
            g.severe(miningTask.getException().getMessage());
            for (final StackTraceElement s : miningTask.getException().getStackTrace()) {
                g.severe(s.toString());
            }
            //g.severe(resultTask.getException());
            GUIUtils.makeError("Something happened while mining data",
                    miningTask.getException().getMessage(),
                    "See the logger for details").showAndWait();
        });

        new Thread(miningTask).start();
    }

    private void loadData(List<String> filenames) {
        MiningUtils.getMainLogger().info("Loading data = " + filenames);
        data = MiningUtils.loadTimeSeries(filenames);
        newDataLoaded();
    }

    public void setFilesToLoad(List<String> filenames) {
        Task<Void> loadTask = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                while (curveVisualizationController == null ||
                        itemsetsVisualizationController == null ||
                        algorithmOptionsController == null ||
                        sequencesByCategoryDisplayController == null) {
                    Thread.sleep(100);
                }
                //Platform.runLater(() -> loadData(filenames));
                loadData(filenames);
                return null;
            }
        };
        new Thread(loadTask).start();

    }

    @FXML
    public void loadAction(Event e) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Time series files");
        if (properties.containsKey("openFileLocation")) {
            final String prop = properties.getProperty("openFileLocation");
            if(Files.exists(Paths.get(prop))) {
                File initDir = new File(prop);
                fileChooser.setInitialDirectory(initDir);
            }
        }
        final Window theStage = menuBar.getScene().getWindow();
        final List<File> files = fileChooser.showOpenMultipleDialog(theStage);

        if (files != null) {
            File firstFolder = files.get(0).getParentFile();
            properties.setProperty("openFileLocation", firstFolder.getAbsolutePath());
            final List<String> filenames = files.stream().map(File::getAbsolutePath).collect(Collectors.toList());
            loadData(filenames);
        }
    }

    @FXML
    public void saveAction(Event e) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save mining results as JSON");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON (*.json)", "*.json"));
        if (properties.containsKey("saveFileLocation")) {
            final String prop = properties.getProperty("saveFileLocation");
            if(Files.exists(Paths.get(prop))) {
                File initDir = new File(prop);
                fileChooser.setInitialDirectory(initDir);
            }
        }
        final Window theStage = menuBar.getScene().getWindow();
        File file = fileChooser.showSaveDialog(theStage);
        if (file != null) {
            properties.setProperty("saveFileLocation", file.getParentFile().getAbsolutePath());
            try {
                final JsonObject miningAsJson = MiningUtils.miningRunAsJson(
                        miningResults.discretisation,
                        miningResults.discrimination,
                        miningResults.mining,
                        miningResults.results
                );
                if (!file.getName().contains(".")) {
                    file = new File(file.getAbsolutePath() + ".json");
                }
                MiningUtils.saveJson(file.getAbsolutePath(), miningAsJson);
            } catch (IOException ioe) {
                GUIUtils.makeError("An error occurred while saving JSON", ioe.getMessage(), ioe.getLocalizedMessage());
            }
        }
    }

    /**
     * Method called when new input data is loaded. Displays the data in the interface
     */
    public void newDataLoaded() {
        curveVisualizationController.displayResults(data, null);

        hasDataLoaded.setValue(true);
        itemsetsVisualizationController.clear();
        sequencesByCategoryDisplayController.clear();
    }

    /**
     * A class to store the data related to a mining run, separately from the ui
     * This class is Immutable
     */
    private static final class MiningRunData {
        final Pair<String, DiscretisationAlgorithm> discretisation;
        final Pair<String, DiscriminationAlgorithm> discrimination;
        final Pair<String, MiningAlgorithm> mining;
        final PipelineResults results;

        /**
         * Saves a mining run and makes copy of the algorithms so that they cannot be modified afterwards.
         * The algorithms are paired with their name
         *
         * @param discretisation
         * @param discrimination
         * @param mining
         * @param results
         */
        public MiningRunData(Pair<String, DiscretisationAlgorithm> discretisation,
                             Pair<String, DiscriminationAlgorithm> discrimination,
                             Pair<String, MiningAlgorithm> mining, PipelineResults results) {

            this.discretisation = new Pair<>(discretisation.getKey(), MiningUtils.makeCopy(discretisation.getValue()));
            this.discrimination = new Pair<>(discrimination.getKey(), MiningUtils.makeCopy(discrimination.getValue()));
            this.mining = new Pair<>(mining.getKey(), MiningUtils.makeCopy(mining.getValue()));
            this.results = results;

        }

    }

    private void setupLoggerStage() {
        loggerStage = new Stage();
        loggerStage.setTitle("SCAF - Logger");
        Optional<Pair<LoggerController, Parent>> loggerOpt = GUIUtils.loadFXMLComponent("/logger.fxml", getClass());
        Pair<LoggerController, Parent> loggerPair = loggerOpt.orElseThrow(() -> new IllegalStateException("Couldn't open logger window"));
        final Logger g = LoggerController.GUI_LOGGER;
        g.addHandler(new LoggerHandler(loggerPair.getKey()));

        loggerStage.setScene(new Scene(loggerPair.getValue(), 600, 800));
        loggerStage.setOnCloseRequest(windowEvent -> {
            loggerStage.hide();
            windowEvent.consume();
        });
        loggerMenuItem.setOnAction(e -> loggerStage.show());
    }

    private void addSequencesDisplayPanel() {
        Optional<Pair<SequencesByCategoryDisplayController, Parent>> loaded =
                GUIUtils.loadFXMLComponent("/sequencesByCategory.fxml", getClass());

        Pair<SequencesByCategoryDisplayController, Parent> res = loaded.orElseThrow(() -> new IllegalStateException("Couldn't load sequence display pane"));
        sequencesByCategoryDisplayController = res.getKey();
        bottomSplitPane.getItems().add(res.getValue());
        sequencesByCategoryDisplayController.setRowListener(this);

    }

    private void addOptionsPanel() {
        Optional<Pair<AlgorithmOptionsController, Parent>> loaded = GUIUtils.loadFXMLComponent("/algorithmOptions.fxml", getClass());
        Pair<AlgorithmOptionsController, Parent> res = loaded.orElseThrow(() -> new IllegalStateException("Algorithm options panel couldn't be loaded"));
        algorithmOptionsController = res.getKey();
        bottomSplitPane.getItems().add(res.getValue());
        algorithmOptionsController.setOnComputeListener(this);

    }

    private void addCurveVisPanel() {
        Optional<Pair<CurveVisualizationController, Parent>> loaded = GUIUtils.loadFXMLComponent("/chartsVisualization.fxml", getClass());
        Pair<CurveVisualizationController, Parent> res = loaded.orElseThrow(() -> new IllegalStateException("Curves view panel couldn't be loaded"));
        curveVisualizationController = res.getKey();
        topSplitPane.getItems().add(res.getValue());
    }

    private void addItemsetsVisPanel() {
        Optional<Pair<ItemsetsVisualizationController, Parent>> loaded = GUIUtils.loadFXMLComponent("/itemsetsVisualization.fxml", getClass());
        Pair<ItemsetsVisualizationController, Parent> res = loaded.orElseThrow(() -> new IllegalStateException("Itemsets view panel couldn't be loaded"));
        itemsetsVisualizationController = res.getKey();
        topSplitPane.getItems().add(res.getValue());
    }

}
