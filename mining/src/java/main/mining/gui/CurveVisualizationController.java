package mining.gui;

import com.sun.javafx.scene.control.skin.ListViewSkin;
import framework.api.TimeSeries;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;
import mining.MiningUtils;
import mining.algorithms.discretisation.DiscretisationResults;
import mining.algorithms.discretisation.DiscretisedSeries;
import mining.gui.chart.LineChartWithItems;
import mining.pipeline.PipelineResults;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class CurveVisualizationController implements FilterController.FilterListener {
    private static final int MIN_CHART_SIZE = 250;
    @FXML
    AnchorPane anchorWrapper;
    @FXML
    Button filterButton;
    @FXML
    VBox root;
    @FXML
    ScrollPane scrollPane;
    @FXML
    ListView<SeriesChartData> chartsPane;

    private static class LVSKin extends ListViewSkin<SeriesChartData> {

        public LVSKin(ListView<SeriesChartData> listView) {
            super(listView);
        }
        public void refresh() {
            flow.recreateCells();
        }
    }

    private FilterController filterController;
    private Scene filterScene;

    private final List<SeriesChartData> charts = new ArrayList<>();

    public static class SeriesChartData {
        final LineChartWithItems lineChart;
        final String name;
        final String simId;

        public SeriesChartData(LineChartWithItems lineChart, String name, String simId) {
            this.lineChart = lineChart;
            this.name = name;
            this.simId = simId;
        }
    }

    public static class ChartDataCellView extends ListCell<SeriesChartData> {
        @Override
        public void updateItem(SeriesChartData item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                setGraphic(item.lineChart);
                setPrefHeight(MIN_CHART_SIZE);
            }
        }
    }

    @FXML
    public void initialize() {
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        chartsPane.setSkin(new LVSKin(chartsPane));
        scrollPane.prefHeightProperty().bind(root.heightProperty());
        scrollPane.prefWidthProperty().bind(root.widthProperty());
        //anchorWrapper.prefHeightProperty().bind(scrollPane.heightProperty());
        loadFilterPanel();
        filterButton.setOnAction(event -> {
            Stage s = new Stage();
            s.setScene(filterScene);
            s.show();
        });
        chartsPane.setCellFactory(param -> new ChartDataCellView());
    }

    private void loadFilterPanel() {
        Optional<Pair<FilterController, Parent>> loaded =
                GUIUtils.loadFXMLComponent("/filterPanel.fxml", getClass());

        Pair<FilterController, Parent> res = loaded.orElseThrow(() -> new IllegalStateException("Couldn't load sequence display pane"));
        filterController = res.getKey();
        filterScene = new Scene(res.getValue());
        filterController.setFilterListener(this);

    }

    private SeriesChartData createLineChart(TimeSeries serie, Optional<Pair<DiscretisedSeries, Map<Integer, DiscretisationResults.Bounds>>> discretisationInfo, int chartNumber) {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Time");
        yAxis.setLabel("Consumption");

        final List<Double> values = serie.toValuesList();
        final ArrayList<XYChart.Data<Number, Number>> seriesData = new ArrayList<>(values.size());

        for (int i = 0; i < values.size(); i++) {
            seriesData.add(new XYChart.Data<>(i * serie.step(), values.get(i)));
        }
        XYChart.Series<Number, Number> chartSerie = new XYChart.Series<>(serie.id() + " - " + serie.name(), FXCollections.observableArrayList(seriesData));
        final LineChartWithItems chart = new LineChartWithItems(xAxis, yAxis);
        chart.setCreateSymbols(false);
        customiseLineChart(chart, chartNumber);
        chart.addSeries(chartSerie, discretisationInfo);
        return new SeriesChartData(chart, serie.name(), serie.id());
    }

    void displayResults(List<TimeSeries> series, PipelineResults results) {
        final Logger logger = MiningUtils.getMainLogger();
        logger.info("Starting displaying curves");
        final long startTime = System.currentTimeMillis();
        charts.clear();
        chartsPane.getItems().clear();
        if (results == null) {
            for (int i = 0; i < series.size(); ++i) {
                final TimeSeries ts = series.get(i);
                final SeriesChartData chartData = createLineChart(ts, Optional.empty(), i);
                chartsPane.getItems().add(chartData);
                charts.add(chartData);
            }
        } else {
            List<DiscretisedSeries> discSeries = results.getDiscretisedSeries().getSeries();
            for (int i = 0; i < discSeries.size(); ++i) {
                final DiscretisedSeries ds = discSeries.get(i);
                final TimeSeries ts = ds.getOriginalSeries();
                final SeriesChartData chartData = createLineChart(ts, Optional.of(new Pair<>(ds, results.getDiscretisedSeries().getSymbolBounds())), i);
                charts.add(chartData);
                chartsPane.getItems().add(chartData);
            }
        }

        final Pair<Set<String>, Set<String>> namesAndIds = series.stream()
                .collect(() -> new Pair<Set<String>, Set<String>>(new HashSet<>(), new HashSet<>()),
                        (setSetPair, timeSeries) -> {
                            setSetPair.getKey().add(timeSeries.name());
                            setSetPair.getValue().add(timeSeries.id());
                        }, (setSetPair, setSetPair2) -> {
                            setSetPair.getKey().addAll(setSetPair2.getKey());
                            setSetPair.getValue().addAll(setSetPair2.getValue());
                        });
        filterController.newData(namesAndIds.getKey(), namesAndIds.getValue());
        ((LVSKin)chartsPane.getSkin()).refresh();
        logger.info("Finished displaying curves in " + (System.currentTimeMillis() - startTime) + "ms");
    }

    private void customiseLineChart(LineChart<Number, Number> chart, int chartNumber) {
        chart.setMinHeight(MIN_CHART_SIZE);
        //chart.setMinHeight(300);
        chart.prefWidthProperty().bind(chartsPane.widthProperty());
        chart.setStyle(lineChartStyle(chartNumber));
        chart.setCreateSymbols(false);
        chart.setPadding(new Insets(10, 0, 10, 5));
    }

    public static final List<String> colors = Arrays.asList(
            "#f44336",
            "#e91e63",
            "#9c27b0",
            "#673ab7",
            "#3f51b5",
            "#2196f3",
            "#03a9f4",
            "#00bcd4",
            "#009688",
            "#4caf50",
            "#8bc34a",
            "#ffc107",
            "#ff9800",
            "#ff5722"
    );

    private String lineChartStyle(int seriesNumber) {
        return "CHART_COLOR_1: " + colors.get(seriesNumber % colors.size()) + ";";
    }

    @Override
    public void onFiltered(Set<String> acceptedMachineNames, Set<String> acceptedSimIds) {
        //chartsPane.getChildren().clear();
        chartsPane.getItems().clear();
        chartsPane.getItems().setAll(charts.stream()
                        .filter(cd -> acceptedMachineNames.contains(cd.name) && acceptedSimIds.contains(cd.simId)).collect(Collectors.toList()));
    }
}


