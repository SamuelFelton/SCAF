package mining.gui;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.stage.Stage;

import java.util.*;

public class FilterController {
    @FXML Button applyButton;
    @FXML ListView<String> rejectedNamesListView;
    @FXML ListView<String> acceptedNamesListView;
    @FXML ListView<String> rejectedSimIdsListView;
    @FXML ListView<String> acceptedSimIdsListView;
    @FXML Button removeNamesButton;
    @FXML Button addNamesButton;
    @FXML Button removeSimIdsButton;
    @FXML Button addSimIdsButton;

    private FilterListener filterListener;

    private final ObservableList<String> rejectedNames = FXCollections.observableArrayList();
    private final ObservableList<String> rejectedSimIds = FXCollections.observableArrayList();
    private final ObservableList<String> acceptedNames = FXCollections.observableArrayList();
    private final ObservableList<String> acceptedSimIds = FXCollections.observableArrayList();



    interface FilterListener {
        void onFiltered(Set<String> acceptedMachineNames, Set<String> acceptedSimIds);
    }

    @FXML
    public void initialize() {

        rejectedSimIdsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        rejectedNamesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        acceptedSimIdsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        acceptedNamesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


        rejectedNamesListView.setItems(rejectedNames);
        acceptedNamesListView.setItems(acceptedNames);
        rejectedSimIdsListView.setItems(rejectedSimIds);
        acceptedSimIdsListView.setItems(acceptedSimIds);

        removeNamesButton.setOnAction(e -> transfer(acceptedNames, acceptedNamesListView, rejectedNames));
        addNamesButton.setOnAction(e -> transfer(rejectedNames, rejectedNamesListView, acceptedNames));

        removeSimIdsButton.setOnAction(e -> transfer(acceptedSimIds, acceptedSimIdsListView, rejectedSimIds));
        addSimIdsButton.setOnAction(e -> transfer(rejectedSimIds, rejectedSimIdsListView, acceptedSimIds));

        applyButton.setOnAction(e -> {
            final Set<String> acceptedNamesFwd = new HashSet<>(acceptedNames);
            final Set<String> acceptedSimIdsFwd = new HashSet<>(acceptedSimIds);

            if(filterListener != null) {
                filterListener.onFiltered(acceptedNamesFwd, acceptedSimIdsFwd);
            }

            Stage s = (Stage) applyButton.getScene().getWindow();
            s.hide();
        });

    }

    private void transfer(ObservableList<String> from, ListView<String> fromView, ObservableList<String> to) {
        ObservableList<String> selected = fromView.getSelectionModel().getSelectedItems();
        to.addAll(selected);
        from.removeAll(selected);
    }

    public void setFilterListener(FilterListener listener) {
        filterListener = listener;
    }

    public void newData(Set<String> names, Set<String> simIds) {
        rejectedSimIds.clear();
        rejectedNames.clear();
        List<String> nameList = new ArrayList<>(names);
        List<String> simList = new ArrayList<>(simIds);
        Collections.sort(nameList);
        Collections.sort(simList);
        acceptedNames.setAll(nameList);
        acceptedSimIds.setAll(simList);
    }

}
