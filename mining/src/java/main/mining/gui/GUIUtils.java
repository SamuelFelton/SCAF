package mining.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.util.Pair;

import java.io.IOException;
import java.util.Optional;

public class GUIUtils {
    public static Alert makeError(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        return alert;
    }

    public static <T, F extends Parent> Optional<Pair<T, F>> loadFXMLComponent(String path, Class<?> callerClass) {
        try {
            FXMLLoader loader = new FXMLLoader(callerClass.getResource(path));
            F component = loader.load();
            return Optional.of(new Pair<>(loader.getController(), component));
        } catch (IOException | ClassCastException e) {
            return Optional.empty();
        }
    }
}
