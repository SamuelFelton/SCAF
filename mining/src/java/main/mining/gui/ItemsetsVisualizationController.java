package mining.gui;

import framework.api.TimeSeries;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;
import javafx.util.Pair;
import javafx.scene.Cursor;
import javafx.scene.Node;
import com.sun.javafx.charts.Legend;
import javafx.scene.input.MouseButton;

import mining.MiningUtils;
import mining.algorithms.discretisation.DiscretisedSeries;
import mining.algorithms.mining.Sequence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

public class ItemsetsVisualizationController {

    @FXML
    LineChart<Number, Number> viewChart;
    @FXML
    AnchorPane viewPane;
    private ZoomManager<Number, Number> zoomManager;

    @FXML
    public void initialize() {
        //viewChart.getStylesheets().add("/curveChart.css");
    }

    void clear() {
        final ObservableList<XYChart.Series<Number, Number>> oldData = viewChart.getData();
        viewChart.setData(FXCollections.observableArrayList());
        oldData.clear();
        zoomManager = null;
    }

    void displayResults(Sequence s) {
        clear();
        final List<DiscretisedSeries> discSeriesList = s.getMatchingSeries();
        viewChart.setCreateSymbols(false);
        final Logger g = MiningUtils.getMainLogger();
        final Collection<XYChart.Series<Number, Number>> seriesList = discSeriesList.parallelStream().collect(ArrayList::new, (coll, ds) -> {
            final TimeSeries series = ds.getOriginalSeries();
            final List<Pair<Integer, Integer>> occurencesBounds = ds.getOccurrences(s);
            g.info(String.format("occurrence bounds for series = %s, sequence = %s", ds.getSymbols().toString(), s.getSeq().toString()));
            final double segmentSize = (series.endX() - series.startX()) / ds.getSymbols().size();
            g.info("Segment size = " + segmentSize);
            int c = 1;
            for (Pair<Integer, Integer> occur : occurencesBounds) {
                final double startX = occur.getKey() * segmentSize;
                final int startIdx = (int) (startX / series.step());
                final double endX = (occur.getValue() + 1) * segmentSize;
                final int endIdx = (int) (endX / series.step()) - 1;
                final double step = series.step();

                final List<Double> valuesList = series.toValuesList();
                final ArrayList<XYChart.Data<Number, Number>> data = new ArrayList<>(endIdx - startIdx + 1);
                for (int i = startIdx; i <= endIdx; i++) {
                    data.add(new XYChart.Data<>(i * step, valuesList.get(i)));
                }
                coll.add(new XYChart.Series<>(series.name() + "-" + series.id() + "-" + c, FXCollections.observableArrayList(data)));
                ++c;
            }
        }, ArrayList::addAll);

        zoomManager = new ZoomManager<>(viewPane, viewChart, seriesList);
    }
}
