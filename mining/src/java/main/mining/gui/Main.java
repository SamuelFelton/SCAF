package mining.gui;

import framework.api.TimeSeries;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mining.MiningUtils;
import mining.algorithms.discretisation.DiscretisationManager;
import mining.algorithms.discretisation.SAXAlgorithm;
import mining.algorithms.discrimination.DiscriminatedSeriesIndexesMap;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.discrimination.DiscriminationManager;
import mining.algorithms.mining.BIDEAlgorithm;
import mining.algorithms.mining.MiningManager;
import mining.parameters.BoundedIntParameter;
import mining.parameters.Parameter;
import mining.parameters.ParameterUtils;

import java.io.Serializable;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        registerAlgorithms();
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("/main.fxml"));
        primaryStage.setTitle("SCAF - Mining");
        Scene scene = new Scene(root, 900, 600);
        primaryStage.setScene(scene);
        primaryStage.show();


        List<String> args = getParameters().getUnnamed();
        if (args.size() > 0) {
            final List<String> filenames = new ArrayList<>();
            for (final String arg : args) {
                final Path p = Paths.get(arg.replaceAll("\\\\", "/"));
                filenames.addAll(Files.walk(p, 1, FileVisitOption.FOLLOW_LINKS)
                        .filter(subP -> !Files.isDirectory(subP))
                        .map(subP -> subP.toAbsolutePath().toString())
                        .collect(Collectors.toList()));

            }
            if (root.getUserData() instanceof Controller) {
                ((Controller) root.getUserData()).setFilesToLoad(filenames);
            } else {
                MiningUtils.getMainLogger().severe("Main Stage user data is not a controller instance");
            }
        }
    }

    private static void registerAlgorithms() {
        DiscriminationManager.INSTANCE.registerAlgorithm("Machine name",
                DiscriminationManager.fromMappingFunction(null, (Function<TimeSeries, String> & Serializable) TimeSeries::name));
        DiscriminationManager.INSTANCE.registerAlgorithm("Simulation ID",
                DiscriminationManager.fromMappingFunction(null, (Function<TimeSeries, String> & Serializable) TimeSeries::id));
        DiscriminationManager.INSTANCE.registerAlgorithm("Avg consumption", new DiscriminationAlgorithm() {
            Set<Parameter<?>> params = new HashSet<>(Collections.singletonList(new BoundedIntParameter("Grouping range", 100, 1, Integer.MAX_VALUE)));

            @Override
            public Set<Parameter<?>> parameters() {
                return params;
            }

            @Override
            public <T> T getParameterValue(String name) {
                return ParameterUtils.getParameterValueFromSet(params, name);
            }

            @Override
            public <T> void setParameterValue(String name, T value) {
                ParameterUtils.setParameterValueFromSet(params, name, value);
            }

            @Override
            public DiscriminatedSeriesIndexesMap discriminate(Collection<TimeSeries> series) {
                Integer groupRange = getParameterValue("Grouping range");
                final DiscriminatedSeriesIndexesMap res = new DiscriminatedSeriesIndexesMap(Collections.emptySet());
                Iterator<TimeSeries> it = series.iterator();
                int i = 0;
                while(it.hasNext()) {
                    TimeSeries s = it.next();
                    final List<Double> vals = s.toValuesList();
                    final double unitCount = vals.size();
                    double avg = s.toValuesList().stream().reduce(0.0, (a, b) -> a + b) / unitCount;
                    int lowerBucketVal = ((int) avg / groupRange) * groupRange;
                    int upperBucketVal = lowerBucketVal + groupRange;
                    String key = lowerBucketVal + "-" + upperBucketVal;
                    res.putIfAbsent(key, new ArrayList<>());
                    res.get(key).add(i);
                    ++i;
                }
                return res;
            }
        });
        DiscretisationManager.INSTANCE.registerAlgorithm("SAX", new SAXAlgorithm());
        MiningManager.INSTANCE.registerAlgorithm("BIDE+", new BIDEAlgorithm());
    }
}
