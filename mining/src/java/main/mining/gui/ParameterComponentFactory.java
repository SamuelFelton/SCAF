package mining.gui;

import javafx.scene.control.Control;
import javafx.scene.control.Spinner;
import mining.parameters.BoundedDoubleParameter;
import mining.parameters.BoundedIntParameter;
import mining.parameters.Parameter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class ParameterComponentFactory {
    private static final Map<Class<? extends Parameter>, ControlCreator<? extends Parameter>> creators;

    static {
        creators = new HashMap<>();
        ControlCreator<BoundedIntParameter> bipCreator = (p) -> {
            Spinner<Integer> s = new Spinner<>(p.getMin(), p.getMax(), p.value());
            s.setEditable(true);
            s.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) {
                    s.increment(0); // won't change value, but will commit editor
                }
            });
            s.valueProperty().addListener((obs, oldValue, newValue) -> p.setValue(newValue));
            return s;
        };
        ControlCreator<BoundedDoubleParameter> bidCreator = (p) -> {
            Spinner<Double> s = new Spinner<>(p.getMin(), p.getMax(), p.value(), p.getStep());
            s.setEditable(true);
            s.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) {
                    s.increment(0); // won't change value, but will commit editor
                }
            });
            s.valueProperty().addListener((obs, oldValue, newValue) -> p.setValue(newValue));
            return s;
        };

        creators.put(BoundedIntParameter.class, bipCreator);
        creators.put(BoundedDoubleParameter.class, bidCreator);
    }

    private ParameterComponentFactory() {
    }

    public static <T extends Parameter> Control controlFromParameter(T parameter) {
        @SuppressWarnings("unchecked")
        Optional<ControlCreator<T>> creatorOpt = Optional.ofNullable((ControlCreator<T>) creators.get(parameter.getClass()));
        if (!creatorOpt.isPresent()) {
            throw new IllegalArgumentException("No control is defined for class " + parameter.getClass());
        }
        return creatorOpt.get().create(parameter);
    }

    private interface ControlCreator<T extends Parameter> {
        Control create(T p);
    }
}
