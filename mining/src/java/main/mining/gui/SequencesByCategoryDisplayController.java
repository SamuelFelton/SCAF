package mining.gui;

import framework.api.TimeSeries;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mining.MiningUtils;
import mining.algorithms.mining.Sequence;
import mining.pipeline.PipelineResults;

import java.util.*;
import java.util.stream.Collectors;

public class SequencesByCategoryDisplayController {


    interface OnSequenceTableRowSelectedListener {
        void rowSelected(String category, Sequence s);
    }

    @FXML
    public TabPane categoryTabs;
    private OnSequenceTableRowSelectedListener rowListener;


    private TableView createTableForCategory(String cat, List<Sequence> sequences) {
        final TableView<Sequence> res = new TableView<>();
        res.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        final TableColumn<Sequence, String> sequenceCol = new TableColumn<>("Sequence");
        final TableColumn<Sequence, Integer> supportCol = new TableColumn<>("Support");

        sequenceCol.prefWidthProperty().bind(res.widthProperty().divide(5).multiply(4));
        supportCol.prefWidthProperty().bind(res.widthProperty().divide(5));

        sequenceCol.setCellValueFactory(p -> new ReadOnlyStringWrapper(p.getValue().stringRepresentation()));
        supportCol.setCellValueFactory(p -> new ReadOnlyObjectWrapper<>(p.getValue().getSupport()));
        res.setEditable(false);
        res.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null && rowListener != null) {
                rowListener.rowSelected(cat, newSelection);
            }
        });
        res.getColumns().setAll(sequenceCol, supportCol);

        res.setItems(FXCollections.observableList(sequences));
        return res;
    }

    public void displayResults(PipelineResults results) {
        Objects.requireNonNull(results);
        //categoryTabs.getTabs().clear();
        categoryTabs.getSelectionModel().selectedItemProperty().addListener(
                (ov, t, t1) -> {
                    if(t != null && t.getContent() instanceof TableView) {
                        ((TableView)t.getContent()).getSelectionModel().clearSelection();
                    }
                }
        );
        List<Map.Entry<String, List<Sequence>>> sortedCats = results.getSequencesByCategory().entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey)).collect(Collectors.toList());
        List<Tab> tabs = new ArrayList<>(sortedCats.size());
        for (Map.Entry<String, List<Sequence>> cat : sortedCats) {
            final Tab t = new Tab(cat.getKey());
            t.selectedProperty().addListener(o -> {
                if(t.isSelected() && t.getContent() == null) {
                    t.setContent(createTableForCategory(cat.getKey(), cat.getValue()));
                }
            });
            tabs.add(t);
        }
        categoryTabs.getTabs().setAll(tabs);

    }

    public void clear() {
        categoryTabs.getTabs().clear();
    }

    public void setRowListener(OnSequenceTableRowSelectedListener rowListener) {
        this.rowListener = rowListener;
    }
}



