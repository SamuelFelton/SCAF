/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package mining.gui;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.Cursor;
import com.sun.javafx.charts.Legend;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

public class ZoomManager<X, Y> {

    private ObservableList<Series<X, Y>> series;
    private XYChart<X, Y> chart;
    private volatile boolean zoomed;

    /**
     * Creates a new {@code ZoomManager}.
     * </p>
     * <b> Due to a bug, {@code series} must not be added to the chart! </b> If you
     * do so, the chart will be emtpy.
     * </p>
     *
     * @param chartParent the chart's parent {@link Pane}.
     * @param chart       the {@link XYChart} to which zooming functionality is added
     * @param series      collection of chart data to display
     * @throws IllegalArgumentException if chart data is empty or {@code null}
     */
    public ZoomManager(final Pane chartParent, final XYChart<X, Y> chart,
                       final Collection<? extends Series<X, Y>> series) {
        super();
        this.chart = Objects.requireNonNull(chart);

        if (series == null || series.isEmpty()) {
            throw new IllegalArgumentException("No chart data given");
        }
        this.series = FXCollections.observableArrayList(series);
        restoreData();
        final Rectangle zoomRect = new Rectangle();
        zoomRect.setManaged(false);
        zoomRect.setFill(Color.LIGHTSEAGREEN.deriveColor(0, 1, 1, 0.5));
        chartParent.getChildren().add(zoomRect);
        setUpZooming(zoomRect, chart);
    }

    static <X, Y> ObservableList<Series<X, Y>> deepCopySeries(final Collection<Series<X, Y>> data) {
        final ObservableList<Series<X, Y>> backup = FXCollections.observableArrayList();
        for (final Series<X, Y> s : data) {
            backup.add(deepCopySeries(s));
        }
        return backup;
    }

    static <X, Y> Series<X, Y> deepCopySeries(final Series<X, Y> series) {
        final Series<X, Y> result = new Series<>();
        result.setName(series.getName());
        result.setData(deepCopySeriesData(series.getData()));
        return result;
    }

    static <X, Y> ObservableList<Data<X, Y>> deepCopySeriesData(
            final Collection<? extends Data<X, Y>> data) {
        final ObservableList<Data<X, Y>> result = FXCollections.observableArrayList();
        for (final Data<X, Y> i : data) {
            result.add(new Data<>(i.getXValue(), i.getYValue()));
        }
        return result;
    }

    private void doZoom(final boolean x, final Number n1, final Number n2) {
        final double min = Math.min(n1.doubleValue(), n2.doubleValue());
        final double max = Math.max(n1.doubleValue(), n2.doubleValue());
        if (max - min > 1) {
            zoomed = true;
            final Iterator<Series<X, Y>> it = chart.getData().iterator();
            while (it.hasNext()) {
                final Series<X, Y> s = it.next();
                final Iterator<Data<X, Y>> it2 = s.getData().iterator();
                while (it2.hasNext()) {
                    final Data<X, Y> d = it2.next();
                    final Object value;
                    if (x) {
                        value = d.getXValue();
                    } else {
                        value = d.getYValue();
                    }
                    if (value instanceof Number) {
                        final Number n = (Number) value;
                        final double dd = n.doubleValue();
                        if (dd < min || dd > max) {
                            it2.remove();
                        } else {
                        }
                    }
                    if (s.getData().isEmpty()) {
                        it.remove();
                    }
                }
            }
        } else {
            // System.out.println("Skip tiny zoom");
        }

    }

    private <T> void setVAxisFromPos(Axis<T> axis, double lower, double upper) {
        if (axis instanceof ValueAxis) {
            ValueAxis<Number> vaxis = (ValueAxis) axis;
            vaxis.setAutoRanging(false);
            vaxis.setLowerBound(vaxis.getValueForDisplay(lower).doubleValue());
            vaxis.setUpperBound(vaxis.getValueForDisplay(upper).doubleValue());
        }
    }

    private synchronized void restoreData() {
        // make a tmp variable of data, since we will modify it but need to be
        // able to restore
        chart.setAnimated(false);
        chart.setData(FXCollections.observableArrayList());
        final ObservableList<Series<X, Y>> backup2 = deepCopySeries(series);
        chart.getData().addAll(backup2);
        for (Node n : chart.getChildrenUnmodifiable()) {
            if (n instanceof Legend) {
                Legend l = (Legend) n;
                System.out.println(l);
                for (Legend.LegendItem li : l.getItems()) {
                    for (XYChart.Series<X, Y> ss : chart.getData()) {
                        if (ss.getName().equals(li.getText())) {
                            li.getSymbol().setCursor(Cursor.HAND); // Hint user that legend symbol is clickable
                            li.getSymbol().setOnMouseClicked(me -> {
                                if (me.getButton() == MouseButton.PRIMARY) {
                                    System.out.println(String.format("%s vs %s%n", ss.getName(), li.getText()));

                                    ss.getNode().setVisible(!ss.getNode().isVisible()); // Toggle visibility of line
                                    for (XYChart.Data<X,Y> d : ss.getData()) {
                                        if (d.getNode() != null) {
                                            d.getNode().setVisible(ss.getNode().isVisible()); // Toggle visibility of every node in the series
                                        }
                                    }
                                }
                            });


                            break;
                        }
                    }
                }
            }
        }
        chart.setAnimated(true);
    }

    private void setUpZooming(final Rectangle rect, final XYChart<X, Y> chart) {
        setUpZoomingRectangle(rect);
    }

    /**
     * Displays a colored rectangle that will indicate zooming boundaries
     *
     * @param rect
     */
    private void setUpZoomingRectangle(final Rectangle rect) {

        /*
         * The chart background. Needed to get the correct rectangle boundaries.
         */
        final Node chartBackground = chart.lookup(".chart-plot-background");
        final ObjectProperty<Point2D> mouseAnchor = new SimpleObjectProperty<>();
        chart.setOnMousePressed(event -> {
            mouseAnchor.set(new Point2D(event.getX(), event.getY()));
        });
        chart.setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY) &&
                    zoomed && event.getClickCount() == 2) {
                restoreData();
                chart.getXAxis().setAutoRanging(true);
                chart.getYAxis().setAutoRanging(true);
                zoomed = false;
                event.consume();
                
            }
        });
        chart.setOnMouseDragged(event -> {
            final double x = event.getX();
            final double y = event.getY();

            rect.setX(Math.min(x, mouseAnchor.get().getX()));
            rect.setY(Math.min(y, mouseAnchor.get().getY()));
            rect.setWidth(Math.abs(x - mouseAnchor.get().getX()));
            rect.setHeight(Math.abs(y - mouseAnchor.get().getY()));
        });
        chart.setOnMouseReleased(event -> {

            final Bounds bb = chartBackground.sceneToLocal(chart.localToScene(rect.getBoundsInLocal()));

            final double minx = bb.getMinX();
            final double maxx = bb.getMaxX();
            final double miny = bb.getMinY();
            final double maxy = bb.getMaxY();

            final Axis<X> xAxis = chart.getXAxis();
            final Axis<Y> yAxis = chart.getYAxis();
            if (maxx - minx > 1 && maxy - miny > 1) {
                setVAxisFromPos(xAxis, minx, maxx);
                setVAxisFromPos(yAxis, maxy, miny);
                zoomed = true;
            }

            rect.setWidth(0);
            rect.setHeight(0);
        });
    }

}
