package mining.gui.chart;

import framework.api.TimeSeries;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.util.Pair;
import mining.algorithms.discretisation.DiscretisationResults;
import mining.algorithms.discretisation.DiscretisedSeries;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class LineChartWithItems extends LineChart<Number, Number> {
    private final List<Data<Number, Number>> symbolPos = new ArrayList<>();
    private final List<Data<Number, Number>> segmentSeparators = new ArrayList<>();
    public LineChartWithItems(Axis<Number> numberAxis, Axis<Number> numberAxis2) {
        super(numberAxis, numberAxis2);
    }

    @Override
    protected void layoutPlotChildren() {
        super.layoutPlotChildren();

        for (Data<Number, Number> d : symbolPos) {
            double x = getXAxis().getDisplayPosition(d.getXValue());
            double y = getYAxis().getDisplayPosition(d.getYValue());
            Text t = (Text) d.getNode();
            t.setLayoutX(x);
            t.setLayoutY(y);
        }

        for (Data<Number, Number> d : segmentSeparators) {
            double x = getXAxis().getDisplayPosition(d.getXValue());
            Line l = (Line) d.getNode();
            l.setStartX(x);
            l.setEndX(x);
            l.setStartY(0.0);
            l.setEndY(10000.0);
        }

    }

    public void addSeries(Series<Number, Number> s, Optional<Pair<DiscretisedSeries, Map<Integer, DiscretisationResults.Bounds>>> discretisedSeriesInfoOpt) {
        getData().add(s);
        if (discretisedSeriesInfoOpt.isPresent()) {
            addDiscretisationDisplay(discretisedSeriesInfoOpt.get());
        }
    }

    public void addDiscretisationDisplay(Pair<DiscretisedSeries, Map<Integer, DiscretisationResults.Bounds>> disc) {
        final DiscretisedSeries ds = disc.getKey();
        final Map<Integer, DiscretisationResults.Bounds> bounds = disc.getValue();
        final List<Integer> symbols = ds.getSymbols();
        final TimeSeries ts = ds.getOriginalSeries();
        final double segmentWidth = (ts.endX() - ts.startX()) / symbols.size();
        for (int i = 0; i < symbols.size(); ++i) {
            final Integer sVal = symbols.get(i);
            DiscretisationResults.Bounds symbolBounds = bounds.get(sVal);
            double xTextVal = ts.startX() + segmentWidth * i + segmentWidth / 2.0;
            double yTextVal = (symbolBounds.lower + symbolBounds.upper) / 2.0;
            if (Double.isInfinite(symbolBounds.lower)) {
                yTextVal = ts.valueAt(xTextVal);
                yTextVal -= yTextVal / 10.0;
            } else if (Double.isInfinite(symbolBounds.upper)) {
                yTextVal = ts.valueAt(xTextVal);
                yTextVal += yTextVal / 10.0;
            }
            final LineChart.Data<Number, Number> textPos = new Data<>(xTextVal, yTextVal);
            final LineChart.Data<Number, Number> vertLinePos = new Data<>(ts.startX() + segmentWidth * i, 0.0);

            final Text symText = new Text(sVal.toString());
            final Line verticalSegmentSeparator = new Line();

            textPos.setNode(symText);
            vertLinePos.setNode(verticalSegmentSeparator);

            getPlotChildren().add(symText);
            getPlotChildren().add(verticalSegmentSeparator);

            symbolPos.add(textPos);
            segmentSeparators.add(vertLinePos);
        }
    }

    public void removeDiscretisationDisplay() {
        List<Node> nodesToRemove = new ArrayList<>(symbolPos.size() + segmentSeparators.size());
        for(Data<Number, Number> sp: symbolPos) {
            nodesToRemove.add(sp.getNode());
        }
        for(Data<Number, Number> sm: segmentSeparators) {
            nodesToRemove.add(sm.getNode());
        }
        getPlotChildren().removeAll(nodesToRemove);
    }
}
