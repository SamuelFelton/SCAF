package mining.gui.logging;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.util.logging.Logger;

public class LoggerController {
    public static final Logger GUI_LOGGER = Logger.getLogger("GUI_LOGGER");
    @FXML
    private TextFlow loggerArea;

    public void appendText(Text t) {
        Platform.runLater(() -> loggerArea.getChildren().add(t));
    }

    public void clearTextArea() {
        Platform.runLater(() -> loggerArea.getChildren().clear());
    }
}
