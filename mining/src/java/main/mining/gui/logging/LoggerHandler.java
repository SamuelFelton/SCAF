package mining.gui.logging;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class LoggerHandler extends Handler {
    LoggerController controller;

    public LoggerHandler(LoggerController controller) {
        this.controller = controller;
    }

    @Override
    public void publish(LogRecord record) {
        Text t = new Text(record.getMessage() + "\n");
        t.setFont(new Font(13));
        Color c = Color.BLACK;
        Level l = record.getLevel();
        if (l.equals(Level.SEVERE)) {
            c = Color.RED;
        } else if (l.equals(Level.WARNING)) {
            c = Color.ORANGE;
        }
        t.setFill(c);
        controller.appendText(t);
    }

    @Override
    public void flush() {
        controller.clearTextArea();
    }

    @Override
    public void close() throws SecurityException {

    }
}
