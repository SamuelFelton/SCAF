package mining.parameters;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

import java.util.Objects;

public class BoundedDoubleParameter extends BoundedParameter<Double> {
    private static final long serialVersionUID = -8497694167703899202L;
    private final Double step;

    /**
     * @param name  the name of the parameter
     * @param value the starting value
     * @param min   the minimum possible value (inclusive)
     * @param max   the maximum possible value (inclusive)
     */
    public BoundedDoubleParameter(String name, Double value, Double min, Double max, Double step) {
        super(name, value, min, max);
        Objects.requireNonNull(step);
        if (step <= 0.0) throw new IllegalArgumentException("step must be positive");
        this.step = step;
    }

    public Double getStep() {
        return step;
    }

    @Override
    public JsonElement valueAsJson() {
        return new JsonPrimitive(value());
    }
}
