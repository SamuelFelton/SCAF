package mining.parameters;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

public class BoundedIntParameter extends BoundedParameter<Integer> {
    private static final long serialVersionUID = 2652481522085844196L;

    public BoundedIntParameter(String name, Integer value, Integer min, Integer max) {
        super(name, value, min, max);
    }

    @Override
    public JsonElement valueAsJson() {
        return new JsonPrimitive(value());
    }
}
