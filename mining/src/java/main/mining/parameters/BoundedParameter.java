package mining.parameters;

import java.util.Objects;

/**
 * A numeric parameter that has both a lower and upper bound
 * @param <T> the numeric type
 */
public class BoundedParameter<T extends Number & Comparable<T>> extends ParameterImpl<T> {
    private static final long serialVersionUID = -3272322737427555228L;
    private final T min;
    private final T max;

    BoundedParameter(String name, T value, T min, T max) {
        super(name, value);
        Objects.requireNonNull(min);
        Objects.requireNonNull(max);
        if (min.compareTo(max) > 0) throw new IllegalArgumentException("min must be less than (or equal) to max");
        if (value.compareTo(min) < 0 || value.compareTo(max) > 0)
            throw new IllegalArgumentException("Initial value must be between min and max");
        this.min = min;
        this.max = max;
    }

    public T getMin() {
        return min;
    }

    public T getMax() {
        return max;
    }
}
