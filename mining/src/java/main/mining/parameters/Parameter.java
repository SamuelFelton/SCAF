package mining.parameters;


import com.google.gson.JsonElement;

import java.io.Serializable;

/**
 * A generic parameter for an algorithm
 * @param <T>
 */
public interface Parameter<T> extends Serializable {
    /**
     * The current value of the parameter
     * @return
     */
    T value();

    /**
     * Set the value of the parameter to {@code value}.
     * @param value
     */
    void setValue(T value);

    /**
     * The name of the parameter
     * @return the name of the parameter
     */
    String name();

    /**
     * The parameter as a JSON Object
     * @return the parameter in JSON
     */
    JsonElement valueAsJson();
}
