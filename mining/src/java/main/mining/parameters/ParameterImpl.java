package mining.parameters;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

/**
 * Default generic implementation of the interface {@link Parameter}.
 * @param <T>
 */
public class ParameterImpl<T> implements Parameter<T> {
    private static final long serialVersionUID = 8662949118033414494L;
    private final String n;
    private T val;

    /**
     * Constructor which states a name and a default starting value for the parameter
     * @param name the name of the parameter
     * @param startingValue the starting value
     */
    ParameterImpl(String name, T startingValue) {
        n = name;
        val = startingValue;
    }

    @Override
    public T value() {
        return val;
    }

    @Override
    public void setValue(T value) {
        val = value;
    }

    @Override
    public String name() {
        return n;
    }

    @Override
    public JsonElement valueAsJson() {
        return new JsonPrimitive(val.toString());
    }


}
