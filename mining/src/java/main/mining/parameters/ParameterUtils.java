package mining.parameters;

import java.util.Optional;
import java.util.Set;

public class ParameterUtils {
    /**
     * Finds the value of a given parameter in a Set. if it could not be found, an {@link IllegalArgumentException} is thrown.
     * @param params the parameter set
     * @param name the name to lookup
     * @param <T> the type of the sought parameter. if it is not correct, a {@link ClassCastException} will be thrown.
     * @return the value of the parameter
     */
    public static <T> T getParameterValueFromSet(Set<Parameter<?>> params, String name) {
        Optional<Parameter<?>> param = params.stream().filter(p -> p.name().equals(name)).findFirst();
        if (param.isPresent()) {
            return (T) param.get().value();
        } else {
            throw new IllegalArgumentException("Queried an invalid parameter. Parameter name : " + name);
        }
    }

    /**
     * Sets the value of a parameter which is in a Set. if it could not be found, an {@link IllegalArgumentException} is thrown.
     * @param params the parameter set
     * @param name the name to lookup
     * @param val the new value of the parameter
     * @param <T> the type of the sought parameter. if it is not correct, a {@link ClassCastException} will be thrown.
     */
    public static <T> void setParameterValueFromSet(Set<Parameter<?>> params, String name, T val) {
        Optional<Parameter<?>> param = params.stream().filter(p -> p.name().equals(name)).findFirst();
        if (param.isPresent()) {
            ((Parameter<T>) param.get()).setValue(val);
        } else {
            throw new IllegalArgumentException("Tried to set the value of an absent parameter. Parameter name : " + name);
        }
    }
}
