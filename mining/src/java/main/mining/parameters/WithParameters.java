package mining.parameters;

import java.io.Serializable;
import java.util.Set;

/**
 * Interface to represent an object that has parameters which are of the type {@link Parameter}
 */
public interface WithParameters extends Serializable {
    /**
     * The set of parameters
     * @return the set of parameters
     */
    Set<Parameter<?>> parameters();

    /**
     * Gets the value of a parameter. If it is not found, an {@link IllegalArgumentException} should be thrown
     * @param name the name of the parameter
     * @param <T> the type of the sought parameter
     * @return the value of the parameter
     */
    <T> T getParameterValue(String name);

    /**
     * Sets the value of a parameter. If it is not found, an {@link IllegalArgumentException} should be thrown
     * @param name the name of the parameter
     * @param value the new value of the parameter
     * @param <T> the type of the sought parameter
     */
    <T> void setParameterValue(String name, T value);
}
