package mining.pipeline;

import framework.api.TimeSeries;
import mining.MiningUtils;
import mining.algorithms.discretisation.DiscretisationAlgorithm;
import mining.algorithms.discretisation.DiscretisationResults;
import mining.algorithms.discretisation.DiscretisedSeries;
import mining.algorithms.discrimination.DiscriminatedSeriesIndexesMap;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.mining.MiningAlgorithm;
import mining.algorithms.mining.Sequence;
import mining.algorithms.mining.SequenceMap;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The default mining pipeline.
 * Runs the algorithms in the following order:
 *  discretise -> mining -> discriminate
 */
public class DefaultMiningPipeline implements MiningPipeline {
    @Override
    public PipelineResults run(Collection<TimeSeries> series,
                               DiscriminationAlgorithm discriminateAlgo,
                               DiscretisationAlgorithm discretiseAlgo,
                               MiningAlgorithm miningAlgo) {
        Logger logger = MiningUtils.getMainLogger();
        logger.info("Entering Default pipeline");
        final DiscretisationResults discretisationRes = discretiseAlgo.discretise(series);
        logger.info("Pipeline: finished discretisation");
        final List<Sequence> miningRes = miningAlgo.mine(discretisationRes);
        logger.warning(miningRes.toString());
        logger.info("Pipeline: finished mining");
        SequenceMap result = new SequenceMap();
        result.put("All", miningRes);

        final DiscriminatedSeriesIndexesMap discriminationRes = discriminateAlgo.discriminate(series);
        logger.info("Pipeline: finished discrimination");

        for (final Map.Entry<String, List<Integer>> entry : discriminationRes.entrySet()) {
            logger.warning("entry = " + entry);
            Set<DiscretisedSeries> correspondingDiscr = entry.getValue().stream().map(i -> discretisationRes.getSeries().get(i)).collect(Collectors.toSet());

            List<Sequence> sequencesOfCat = miningRes.stream()
                    .map(seq -> {
                        Set<DiscretisedSeries> s2 = new HashSet<>(seq.getMatchingSeries());
                        s2.retainAll(correspondingDiscr);
                        return new Sequence(seq.getSeq(), s2.size(), new ArrayList<>(s2));
                    })
                    .filter(seq -> seq.getSupport() > 0) //if it is in both collections then add it
                    .collect(Collectors.toList());
            result.put(entry.getKey(), sequencesOfCat);
        }
        logger.info("Pipeline: finished assembling results");
        return new PipelineResults(result, discretisationRes);
    }
}
