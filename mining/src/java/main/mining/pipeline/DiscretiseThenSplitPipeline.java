package mining.pipeline;

import framework.api.TimeSeries;
import mining.MiningUtils;
import mining.algorithms.discretisation.DiscretisationAlgorithm;
import mining.algorithms.discretisation.DiscretisationResults;
import mining.algorithms.discretisation.DiscretisedSeries;
import mining.algorithms.discrimination.DiscriminatedSeriesIndexesMap;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.mining.MiningAlgorithm;
import mining.algorithms.mining.Sequence;
import mining.algorithms.mining.SequenceMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DiscretiseThenSplitPipeline implements MiningPipeline {
    @Override
    public PipelineResults run(Collection<TimeSeries> series, DiscriminationAlgorithm discriminateAlgo, DiscretisationAlgorithm discretiseAlgo, MiningAlgorithm miningAlgo) {
        final Logger logger = MiningUtils.getMainLogger();
        long startTime = System.currentTimeMillis();
        logger.info("Starting Discretise then split pipeline");
        final DiscriminatedSeriesIndexesMap discriminated = discriminateAlgo.discriminate(series);
        final DiscretisationResults discretised = discretiseAlgo.discretise(series);
        final List<DiscretisedSeries> discretisedSeries = discretised.getSeries();

        final SequenceMap minedByCat = new SequenceMap();
        for(Map.Entry<String, List<Integer>> discrimCat: discriminated.entrySet()) {
            final DiscretisationResults discretCat = new DiscretisationResults(discretised.getSymbolBounds(),
                    discrimCat.getValue().stream().map(discretisedSeries::get).collect(Collectors.toList()));
            final List<Sequence> mined = miningAlgo.mine(discretCat);
            minedByCat.put(discrimCat.getKey(), mined);
        }
        logger.info("Finished pipeline in " + (System.currentTimeMillis() - startTime) + "ms");
        return new PipelineResults(minedByCat, discretised);

    }
}
