package mining.pipeline;

import framework.api.TimeSeries;
import mining.algorithms.discretisation.DiscretisationAlgorithm;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.mining.MiningAlgorithm;

import java.util.Collection;

/**
 * Interface representing a Mining pipeline.
 * The pipeline is responsible for running the three algorithms and piecing their results together
 */
public interface MiningPipeline {
    /**
     * Runs the pipeline on the series with the algorithms given in parameters.
     * @param series
     * @param discriminateAlgo
     * @param discretiseAlgo
     * @param miningAlgo
     * @return the results from running this pipeline
     */
    PipelineResults run(Collection<TimeSeries> series,
                        DiscriminationAlgorithm discriminateAlgo,
                        DiscretisationAlgorithm discretiseAlgo,
                        MiningAlgorithm miningAlgo);
}
