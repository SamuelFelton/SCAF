package mining.pipeline;

import mining.algorithms.discretisation.DiscretisationResults;
import mining.algorithms.mining.SequenceMap;

/**
 * A class representing the data obtained from running a pipeline. Contains the {@link mining.algorithms.mining.Sequence}s by category, aswell as the discretised series.
 */
public class PipelineResults {
    private final SequenceMap sequencesByCategory;
    private final DiscretisationResults discretisedSeries;

    public PipelineResults(SequenceMap sequencesByCategory, DiscretisationResults discretisedSeries) {
        this.sequencesByCategory = sequencesByCategory;
        this.discretisedSeries = discretisedSeries;
    }

    public SequenceMap getSequencesByCategory() {
        return sequencesByCategory;
    }

    public DiscretisationResults getDiscretisedSeries() {
        return discretisedSeries;
    }
}
