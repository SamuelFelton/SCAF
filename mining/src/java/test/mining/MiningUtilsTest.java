package mining;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import framework.api.TimeSeries;
import framework.impl.TimeSeriesImpl;
import javafx.util.Pair;
import mining.algorithms.discretisation.DiscretisationAlgorithm;
import mining.algorithms.discretisation.SAXAlgorithm;
import mining.algorithms.discrimination.DiscriminationAlgorithm;
import mining.algorithms.discrimination.DiscriminationManager;
import mining.algorithms.mining.BIDEAlgorithm;
import mining.algorithms.mining.MiningAlgorithm;
import mining.pipeline.DiscretiseThenSplitPipeline;
import mining.pipeline.PipelineResults;
import org.junit.Test;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class MiningUtilsTest {
    private final String l1 = "1/0.2/1.0;2.1;3;2;";
    private final String l2 = "2/0.4/1.0;5";
    private final String mName = "m1";
    private final String simBase = "s1";
    
    private boolean l1IsCorrect(TimeSeries t1) {
        return t1.name().equals(mName) &&
                t1.id().equals(simBase + "_1") &&
                t1.step() == 0.2 &&
                t1.endX() == 0.8;
    }

    private boolean l2IsCorrect(TimeSeries t2) {
        return t2.name().equals(mName) &&
                t2.id().equals(simBase + "_2") &&
                t2.step() == 0.4 &&
                t2.endX() == 0.8;
    }

    @Test
    public void readGeneratorCorrectLineInputTest() {
        TimeSeries t1 = MiningUtils.readGeneratorInputLine(l1, mName, simBase);
        TimeSeries t2 = MiningUtils.readGeneratorInputLine(l2, mName, simBase);
        assertThat(l1IsCorrect(t1)).isEqualTo(true);
        assertThat(l2IsCorrect(t2)).isEqualTo(true);
    }

    @Test
    public void readStringStreamGeneratorInputTest() {
        Stream<String> ss = Stream.of(l1, l2);
        List<TimeSeries> ll = MiningUtils.readAllGeneratorInputFileLines(ss, mName, simBase);
        assertThat(ll.size()).isEqualTo(2);
        assertThat(l1IsCorrect(ll.get(0))).isTrue();
        assertThat(l2IsCorrect(ll.get(1))).isTrue();
    }

    @Test
    public void makeCopyCorrect1Test() {
        String orig = "ddzanfia";
        String v = MiningUtils.makeCopy(orig);

        assertThat(v == orig).isFalse();
        assertThat(v).isEqualTo(orig);
    }

    @Test
    public void saveAsJsonTest() {
        final Pair<String, DiscretisationAlgorithm> discrePair = new Pair<>("SAX", new SAXAlgorithm());
        final Pair<String, DiscriminationAlgorithm> discriPair = new Pair<>("Machine", DiscriminationManager.fromMappingFunction(null,
                (Function<TimeSeries, String> & Serializable) t -> t.name()));
        final Pair<String, MiningAlgorithm> miningPair = new Pair<>("BIDE+", new BIDEAlgorithm());
        TimeSeries t1 = new TimeSeriesImpl(Arrays.asList(1.0, 2.0, 3.0, 10.0, 35.0, 0.2), 0.5, "m1", "id1");
        TimeSeries t2 = new TimeSeriesImpl(Arrays.asList(1.0, 2.0, 4.0, 1.0, 2.0, 3.0, 1.0), 0.5, "m2", "id1");

        PipelineResults res = new DiscretiseThenSplitPipeline().run(Arrays.asList(t1, t2), discriPair.getValue(), discrePair.getValue(), miningPair.getValue());

        JsonObject resJson = MiningUtils.miningRunAsJson(discrePair, discriPair, miningPair, res);
        assertThat(resJson.has("discretisation")).isTrue();
        assertThat(resJson.has("discrimination")).isTrue();
        assertThat(resJson.has("mining")).isTrue();
        JsonObject categoriesJson = resJson.getAsJsonObject("categories");
        assertThat(categoriesJson.has("m1")).isTrue();
        assertThat(categoriesJson.has("m2")).isTrue();

        JsonObject m1Json = categoriesJson.getAsJsonObject("m1");
        assertThat(m1Json.has("sequences") && m1Json.get("sequences").isJsonArray()).isTrue();
        JsonArray m1SeqJson = m1Json.getAsJsonArray("sequences");
        m1SeqJson.forEach(o -> {
            assertThat(o.isJsonObject()).isTrue();
            JsonObject oo = o.getAsJsonObject();
            assertThat(oo.has("support")).isTrue();
            assertThat(oo.has("sequence")).isTrue();
            assertThat(oo.has("matching")).isTrue();
        });

    }
}
