package mining.algorithms;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

public class AlgorithmManagerTest {
    private AlgorithmManager<Function<String, String>> manager;
    private Function<String, String> unit = (s) -> s;

    @Before
    public void setup() {
        manager = new AlgorithmManager<>();
    }

    @Test
    public void checkRegisterCorrect() {
        Function<String, String> withA = (s) -> s + "a";
        Function<String, String> withB = (s) -> s + "b";

        manager.registerAlgorithm("a", withA);
        manager.registerAlgorithm("b", withB);

        Optional<Function<String, String>> aOpt = manager.get("a");
        Optional<Function<String, String>> bOpt = manager.get("b");
        assertThat(aOpt.isPresent()).isTrue();
        assertThat(bOpt.isPresent()).isTrue();
        String e = "";
        assertThat(aOpt.get().apply(e)).isEqualTo("a");
        assertThat(bOpt.get().apply(e)).isEqualTo("b");
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkThatCannotRegisterAlgorithmTwice() {
        manager.registerAlgorithm("unit", unit);
        manager.registerAlgorithm("unit", unit);
    }

    @Test(expected = NullPointerException.class)
    public void checkThatCannotHaveNullName() {
        manager.registerAlgorithm(null, unit);
    }

    @Test(expected = NullPointerException.class)
    public void checkThatCannotHaveNullAlgorithm() {
        manager.registerAlgorithm("unit", null);
    }

    @Test
    public void checkClearAlgorithm() {
        manager.registerAlgorithm("unit", unit);
        manager.clearAlgorithms();

        assertThat(manager.algorithmNames().isEmpty()).isTrue();
    }

    @Test
    public void checkAlgorithmNamesSetIsEmptyAtStart() {
        assertThat(manager.algorithmNames().isEmpty()).isTrue();
    }

    @Test
    public void checkAlgorithmNamesSetWithAdd() {
        Set<String> expectedNames = new HashSet<>(Arrays.asList("a", "b", "c"));
        expectedNames.forEach(s -> manager.registerAlgorithm(s, unit));
        assertThat(manager.algorithmNames()).isEqualTo(expectedNames);
    }


}
