package mining.algorithms.discretisation;

import framework.api.TimeSeries;
import framework.impl.TimeSeriesImpl;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SAXAlgorithmTest {
    @Test
    public void baseTest() {
        TimeSeries ecg1 = new TimeSeriesImpl(Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0), 1.0, "ecg1", "1");
        TimeSeries ecg2 = new TimeSeriesImpl(Arrays.asList(1.5, 2.5, 10.0, 9.0, 8.0, 7.0, 6.0, 5.0), 1.0, "ecg2", "1");
        TimeSeries ecg3 = new TimeSeriesImpl(Arrays.asList(-1.0, -2.0, -3.0, -4.0, -5.0), 1.0, "ecg3", "1");
        TimeSeries ecg4 = new TimeSeriesImpl(Arrays.asList(-2.0, -3.0, -4.0, -5.0, -6.0), 1.0, "ecg4", "1");
        List<TimeSeries> base = Arrays.asList(ecg1, ecg2, ecg3, ecg4);

        DiscretisationAlgorithm sax = new SAXAlgorithm();
        sax.setParameterValue("symbolCount", 4);
        sax.setParameterValue("segmentCount", 3);
        DiscretisationResults res = sax.discretise(base);

        List<DiscretisedSeries> resultingSequences = res.getSeries();
        assertThat(resultingSequences.get(0).getSymbols()).isEqualTo(Arrays.asList(2, 3, 4));
        assertThat(resultingSequences.get(0).getOriginalSeries()).isEqualTo(ecg1);
        assertThat(resultingSequences.get(1).getSymbols()).isEqualTo(Arrays.asList(3, 4, 4));
        assertThat(resultingSequences.get(1).getOriginalSeries()).isEqualTo(ecg2);
        assertThat(resultingSequences.get(2).getSymbols()).isEqualTo(Arrays.asList(1, 1, 1));
        assertThat(resultingSequences.get(2).getOriginalSeries()).isEqualTo(ecg3);
        assertThat(resultingSequences.get(3).getSymbols()).isEqualTo(Arrays.asList(1, 1, 1));
        assertThat(resultingSequences.get(3).getOriginalSeries()).isEqualTo(ecg4);

    }
}
