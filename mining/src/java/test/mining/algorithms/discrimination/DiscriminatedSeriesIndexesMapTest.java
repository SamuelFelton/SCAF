package mining.algorithms.discrimination;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class DiscriminatedSeriesIndexesMapTest {
    @Test
    public void checkThatCategoriesArePresentInMap() {
        Set<String> cats = new HashSet<>(Arrays.asList("a", "b", "c"));
        DiscriminatedSeriesIndexesMap d = new DiscriminatedSeriesIndexesMap(cats);
        for (String c : cats) {
            assertThat(d.get(c)).isEqualTo(Collections.emptyList());
        }
    }
}
