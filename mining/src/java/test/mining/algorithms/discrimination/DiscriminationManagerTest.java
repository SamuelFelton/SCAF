package mining.algorithms.discrimination;

import fixtures.TimeSeriesFixture;
import framework.api.TimeSeries;
import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

public class DiscriminationManagerTest {
    private List<TimeSeries> seriesList;
    private Set<String> names = new HashSet<>(Arrays.asList("a", "b", "c", "d"));

    @Before
    public void setup() {
        seriesList = TimeSeriesFixture.randomTimeSeriesList(names);
    }

    @Test
    public void checkThatFromMappingWithOnlyOneCatIsCorrect() {
        Set<String> cats = new HashSet<>(Collections.singletonList("one"));
        DiscriminationAlgorithm algo = DiscriminationManager.fromMappingFunction(cats, (Function<TimeSeries, String> & Serializable) s -> "one");
        DiscriminatedSeriesIndexesMap map = algo.discriminate(seriesList);
        assertThat(map.keySet()).isEqualTo(cats);
        assertThat(map.get("one"))
                .isEqualTo(Arrays.asList(0, 1, 2, 3));
    }

    @Test
    public void checkIdentityMapping() {
        DiscriminationAlgorithm algo = DiscriminationManager.fromMappingFunction(names, (Function<TimeSeries, String> & Serializable) TimeSeries::name);
        DiscriminatedSeriesIndexesMap map = algo.discriminate(seriesList);
        assertThat(map.keySet()).isEqualTo(names);
        assertThat(map.entrySet()
                .stream()
                .allMatch(e -> e.getValue().size() == 1 && seriesList.get(e.getValue().get(0)).name().equals(e.getKey())));
    }

    @Test
    public void checkThatFromMappingHasNoParameter() {
        DiscriminationAlgorithm algo = DiscriminationManager.fromMappingFunction(names, (Function<TimeSeries, String> & Serializable) TimeSeries::name);
        assertThat(algo.parameters().isEmpty());
    }
}
