package mining.algorithms.mining;

import mining.algorithms.discretisation.DiscretisationResults;
import mining.algorithms.discretisation.DiscretisedSeries;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BIDEAlgorithmTest {
    BIDEAlgorithm algo;

    @Before
    public void setup() {
        algo = new BIDEAlgorithm();
    }

    @Test
    public void checkParametersHasMinsup() {
        Double minsup = algo.getParameterValue("minSup");
        assertThat(minsup).isNotNull();
    }

    @Test
    public void checkOnlyOneParameter() {
        assertThat(algo.parameters().size()).isEqualTo(1);
    }

    @Test
    public void checkBaseTest() {
        List<DiscretisedSeries> input = Arrays.asList(
                new DiscretisedSeries(Arrays.asList(1, 2, 3, 4, 6), null),
                new DiscretisedSeries(Arrays.asList(4, 3, 2, 5), null),
                new DiscretisedSeries(Arrays.asList(5, 2, 6, 3, 2), null),
                new DiscretisedSeries(Arrays.asList(5, 7, 6, 3, 2, 3), null)
        );
        algo.setParameterValue("minSup", 0.75);

        List<Sequence> res = algo.mine(new DiscretisationResults(null, input));

        /*
            SPMF output
            2 -1 3 -1 #SUP: 3 #SID: 0 2 3
            2 -1 #SUP: 4 #SID: 0 1 2 3
            3 -1 2 -1 #SUP: 3 #SID: 1 2 3
            3 -1 #SUP: 4 #SID: 0 1 2 3
            5 -1 #SUP: 3 #SID: 1 2 3
            6 -1 #SUP: 3 #SID: 0 2 3
        */

    }
}
