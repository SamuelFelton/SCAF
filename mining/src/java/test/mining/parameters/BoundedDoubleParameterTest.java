package mining.parameters;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class BoundedDoubleParameterTest {

    @Test(expected = NullPointerException.class)
    public void testInstanciationWithNullStepFails() {
        new BoundedDoubleParameter("whatever", 1.0, 0.0, 1.2, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanciationWithNegativeStepFails() {
        new BoundedDoubleParameter("whatever", 1.0, 0.0, 1.2, -1.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanciationWithZeroStepFails() {
        new BoundedDoubleParameter("whatever", 1.0, 0.0, 1.2, 0.0);
    }

    @Test
    public void testCorrectConstructor() {
        final double val = 1.0, min = 0.0, max = 2.0, step = 0.1;

        BoundedDoubleParameter bdp = new BoundedDoubleParameter("what", val, min, max, step);

        assertThat(bdp.value()).isEqualTo(val);
        assertThat(bdp.getMin()).isEqualTo(min);
        assertThat(bdp.getMax()).isEqualTo(max);
        assertThat(bdp.getStep()).isEqualTo(step);
    }
}
