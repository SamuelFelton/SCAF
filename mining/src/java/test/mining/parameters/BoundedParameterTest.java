package mining.parameters;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class BoundedParameterTest {
    @Test(expected = IllegalArgumentException.class)
    public void testInstanciationWithMinGtMaxFails() {
        new BoundedParameter<>("whatever", 0, 2, -2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanciationWithValueLtMinFails() {
        new BoundedParameter<>("whatever", -1, 0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstanciationWithValueGtMaxFails() {
        new BoundedParameter<>("whatever", 2, 0, 1);
    }

    @Test(expected = NullPointerException.class)
    public void testInstanciationWithNullMinFails() {
        new BoundedParameter<>("whatever", 2, null, 1);
    }

    @Test(expected = NullPointerException.class)
    public void testInstanciationWithNullMaxFails() {
        new BoundedParameter<>("whatever", 2, 0, null);
    }

    @Test
    public void correctConstructor() {
        int initial = 1;
        int min = 0;
        int max = 2;
        BoundedParameter<Integer> bip = new BoundedParameter<>("yes", initial, min, max);
        assertThat(bip.getMin()).isEqualTo(min);
        assertThat(bip.getMax()).isEqualTo(max);
    }

}
