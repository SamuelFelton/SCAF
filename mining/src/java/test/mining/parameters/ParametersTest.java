package mining.parameters;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ParametersTest {
    private Parameter<Integer> intParam;
    private Parameter<String> stringParam;

    @Before
    public void setUp() {
        intParam = new ParameterImpl<>("intParam", 0);
        stringParam = new ParameterImpl<>("stringParam", "default");
    }

    @Test
    public void checkThatNamesAreCorrect() {
        assertThat(intParam.name()).isEqualTo("intParam");
        assertThat(stringParam.name()).isEqualTo("stringParam");
    }

    @Test
    public void checkThatDefaultValuesAreCorrect() {
        assertThat(intParam.value()).isEqualTo(0);
        assertThat(stringParam.value()).isEqualTo("default");
    }

    @Test
    public void checkValueSetter() {
        intParam.setValue(1);
        stringParam.setValue("newVal");
        assertThat(intParam.value()).isEqualTo(1);
        assertThat(stringParam.value()).isEqualTo("newVal");
    }
}
