# Simulation de Consommation par Automates pour la Fouille de données
uteurs
Ce projet est réalisé dans le cadre de nos études à l'INSA de Rennes durant le module Projet 4INFO avec notre encadrante Mme. Laurence ROZÉ pour le compte de M. Maël GUILLEMÉ.

Auteurs:
* Lucas BONNAL
* Guillaume COURTET  
* Samuel FELTON
* Martin GOUBET
* Jean-Baptiste LEPRINCE
* Léandre LE POLLES--POTIN
* Nathan LE SOMMER  
* Marvin YONG

## Utilisations
Ce projet utilise [Gradle](https://gradle.org)
- Pour construire le projet :
`gradle build` ;
- Pour clean les éléments produits :
`gradle clean` ;
- Pour associer ces deux tâches :
`gradle clean build` ;
- Pour lancer le projet :
`gradle run` ;
- Pour exécuter une tâche spécifique :
`gradle <task name>` (e.g. `gradle jacocoTestReport`).

Plugins installés pour la vérification du code : Jacoco, FindBugs.
Documentation Gradle : [ici](https://docs.gradle.org/current/dsl/index.html).

Libraries installées pour les tests : JUnit (pas conseillée), AssertJ (conseillée car les tests sont plus clairs), Mockito (pour simuler des classes et leur comportement).

## Modules

## Développement


## Licence

A remplir
