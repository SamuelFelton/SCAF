package fixtures;

import framework.api.TimeSeries;
import framework.impl.TimeSeriesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TimeSeriesFixture {
    private final static Random r = new Random();
    public static TimeSeries randomTimeSeries(String name) {
        double step = r.nextDouble() * 100.0;
        int size = r.nextInt(20);
        return new TimeSeriesImpl(IntStream.range(0, size)
                .mapToDouble(i -> r.nextDouble() * 100.0).boxed().collect(Collectors.toList()),
                step,
                name, "1");
    }
    public static List<TimeSeries> randomTimeSeriesList(Set<String> names) {
        return names.stream().map(TimeSeriesFixture::randomTimeSeries).collect(Collectors.toList());
    }
}
