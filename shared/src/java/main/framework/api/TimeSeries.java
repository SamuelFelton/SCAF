package framework.api;

import java.util.List;

/**
 * Interface representing a Time Series, with real-valued ordinates
 */
public interface TimeSeries {
    /**
     * Returns the ordinate for the given abscissa.
     * if the abscissa is not in the Time series, an exception will be thrown.
     * @param x the abscissa. should be between startX() and endX() (both inclusive)
     * @return the ordinate value at position X.
     */
    double valueAt(double x);

    /**
     * Returns an array representing the set of ordinates (sorted by their abscissa) as they appear every step
     * @return
     */
    double[] toValuesArray();

    List<Double> toValuesList();

    /**
     * The step between the time series registered values (the x increment).
     * @return the step
     */
    double step();

    /**
     * The max value in the list, if there is one. Otherwise, {@link java.util.NoSuchElementException} will be thrown
     * @return the max ordinate value
     */
    double max();

    /**
     * the min value in the list, if there is one. Otherwise, {@link java.util.NoSuchElementException} will be thrown
     * @return the minimal ordinate value
     */
    double min();

    /**
     * Returns the abscissa value for the start of time series.
     * @return the abscissa of the start of the time series
     */
    double startX();

    /**
     * Returns the abscissa value for the end of time series. this method should only be called if there is a value present in the series
     * @return the abscissa of the end of the series
     */
    double endX();

    /**
     * Returns the name of the time series
     * @return the name of the time series
     */
    String name();
    /**
     * Returns the id of the time series
     * @return the id of the time series
     */
    String id();

}