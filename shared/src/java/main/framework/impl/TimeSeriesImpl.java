package framework.impl;

import framework.api.TimeSeries;

import java.util.ArrayList;
import java.util.List;

public class TimeSeriesImpl implements TimeSeries {
    private final List<Double> values;
    private final double step;
    private final String seriesName;
    private final String seriesId;
    public TimeSeriesImpl(List<Double> values, double step, String name, String id)
    {
        this.values = new ArrayList<>(values);
        this.step = step;
        this.seriesName = name;
        this.seriesId = id;
    }

    @Override
    public double valueAt(double x) {
        if(x > endX()) {
            throw new IllegalArgumentException("x must be <= to endX");
        }
        if(x < startX()) {
            throw new IllegalArgumentException("x must be >= to startX");
        }
        int lowerIdx = (int)(x / step);
        int upperIdx = lowerIdx + 1;

        double lowerX = lowerIdx * step;
        double upperX = upperIdx * step;
        double lowerValue = values.get(lowerIdx);
        double upperValue = values.get(upperIdx);

        double interp = (upperValue - lowerValue) * ((x - lowerX) / (upperX - lowerX));

        return lowerValue + interp;
    }

    @Override
    public double[] toValuesArray() {
        double[] a = new double[values.size()];
        for(int i = 0; i < values.size(); ++i) {
            a[i] = values.get(i);
        }
        return a;
    }

    @Override
    public List<Double> toValuesList() {
        return values;
    }

    @Override
    public double step() {
        return step;
    }

    @Override
    public double max() {
        return values.stream().max((a, b) -> a < b ? -1 : 1).get();
    }

    @Override
    public double min() {
        return values.stream().min((a, b) -> a < b ? -1 : 1).get();
    }

    @Override
    public double startX() {
        return 0.0;
    }

    @Override
    public double endX() {
        return step * values.size();
    }

    @Override
    public String name() { return seriesName; }
    @Override
    public String id() { return seriesId; }
}