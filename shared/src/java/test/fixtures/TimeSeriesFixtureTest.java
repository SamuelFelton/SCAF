package fixtures;

import framework.api.TimeSeries;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class TimeSeriesFixtureTest {
    @Test
    public void checkRandomTimeSeries() {
        TimeSeries t = TimeSeriesFixture.randomTimeSeries("t");
        assertThat(t).isNotNull();
    }
    @Test
    public void checkrRandomTimeSeriesList() {
        Set<String> ns = new HashSet<>(Arrays.asList("a", "b", "c"));
        List<TimeSeries> rs = TimeSeriesFixture.randomTimeSeriesList(ns);
        for(String s : ns) {
            assertThat(rs.stream().filter(t -> t.name().equals(s)).findFirst()).isPresent();
        }
    }
}
