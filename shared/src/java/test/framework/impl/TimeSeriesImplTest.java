package framework.impl;

import framework.api.TimeSeries;
import org.assertj.core.data.Offset;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class TimeSeriesImplTest {
    private TimeSeries t1;
    private List<Double> values;

    @Before
    public void setUp() {
        values = Arrays.asList(1.5, 0.0, 1.0, 0.2, 2.0, 3.0, 4.0, 5.0);
        t1 = new TimeSeriesImpl(values, 1.0, "t1", "1");
    }

    @Test
    public void checkThatStepReturnsCorrectValue() {
        assertThat(t1.step()).isEqualTo(1.0);
    }

    @Test
    public void checkThatMaxReturnsMaximumValue() {
        assertThat(t1.max()).isEqualTo(5.0);
    }

    @Test
    public void chechkThatMinReturnsMinimumValue() {
        assertThat(t1.min()).isEqualTo(0.0);
    }

    @Test
    public void checkThatStartXIsCorrect() {
        assertThat(t1.startX()).isEqualTo(0.0);
    }

    @Test
    public void checkThatEndXIsCorrect() {
        assertThat(t1.endX()).isEqualTo(t1.step() * values.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueAtShouldThrowExceptionWithParameterBelowZero() {
        t1.valueAt(-1.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueAtShouldThrowExceptionWithParameterAboveEndX() {
        t1.valueAt(t1.endX() + 0.01);
    }
    @Test
    public void valueAtShouldReturnCorrectValues() {
        assertThat(t1.valueAt(0.0)).isEqualTo(1.5);
        assertThat(t1.valueAt(1.0)).isEqualTo(0.0);
        assertThat(t1.valueAt(0.5)).isCloseTo(0.75, Offset.offset(0.01));
    }
}
