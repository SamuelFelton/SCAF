# Simulateur de système de production : how to

Ce manuel passe en revue plusieurs façons d'utiliser le **simulateur de systèmes de production**. Il explique en détail les points clefs de l'édition des fichiers de configuration XML, que ce soit pour la configuration du système de production ou celle de la simulation désirée.

## Notes sur le fonctionnement du Simulateur
* Le simulateur de système de production fonctionne de la manière suivante :
	* Le simulateur lance les machines à une date choisie aléatoirement entre les deux bornes définies par la simulation. Pour lancer les machines, le simulateur tente d'effectuer une transition depuis l'état défini comme état initial pour cette machine.
	* Lorsqu'une machine atteint un nouvel état, le simulateur calcule la durée durant laquelle elle y restera en choisissant une valeur au hasard entre les deux bornes définies pour cet état.
	* Une fois cette durée écoulée, le simulateur teste successivement les transitions qui partent de cet état jusqu'à en trouver une dont les conditions soient satisfaites.
	* Lorsqu'une machine atteint un état déclaré comme final, la machine est arrêtée. Elle ne sera plus traitée par la suite de la simulation.
	* Les transitions des machines sont traitées par ordre chronologique.

## Fichier d'entrée : Descripteur du Système de production 

### Aperçu général

Le fichier d'entrée doit avoir le format suivant :
```xml

<PRODSYS name="system">
    <VARIABLE name="variable" init="1"/>
	<MACHINE name="machine">
		...
		<STATE name="start" initial="true" dur="9m;11m"/>
		<STATE name="middle" dur="4m;6m" function="1x"/>
		<STATE name="end" final="true" dur="0s"/>
		...
		<TRANSITION name="start_middle" from="mange_pomme" to="mange_poire" effect="poire = poire - 1;"/>
		<TRANSITION name="middle_end" from="mange_poire" to="mange_pomme" cond="poire gt 5;" eff="pomme = pomme - 1;"/>
		...
	</MACHINE>
	<MACHINE name="machine2">
		...
	</MACHINE>
</PRODSYS>
```

### Système de production
* Un système de production est représenté par une balise ```<PRODSYS>```. Chaque fichier descripteur ne peut contenir qu'un seul fichier de production.

### Machine
* Une machine d'un système de production est représentée par une balise ```<MACHINE>```. Le nombre de machines dans un système donné n'est pas limité.
* Le nom d'une machine (attribut **name**) doit être unique pour un système de production donné

### Variable
* Une variable d'un système de production est représentée par une balise ```<VARIABLE>```. Le nombre de variables dans un système donné n'est pas limité.
* Le nom d'une variable (attribut **name**) doit être unique pour un système de production donné
* Bien que toutes les variables déclarées soient globales (elles peuvent être utilisées par tout le système de production, il est peut être pertinent de déclarer les variables à l'intérieur de la machine qui leur correspond le plus, pour plus rapidement les identifier. Il faut cependant être vigilant au fait qu'une variable doit impérativement être déclarée **avant** d'être mentionnée dans une formule. 

* L'attribut obligatoire **init** indique la valeur initiale prise par la variable. Elle peut être flottante.


#### Référence à une variable déclarée

* Une variable ne peut être référencée dans une formule que si elle à été  déclarée plus haut dans le fichier à l'aide d'une balise ```<VARIABLE>```. 
* Pour faire référence à une variable, il suffit d'utiliser son nom (valeur de l'attribut **name**) dans une formule.

### État
* Un état d'une machine est représentée par une balise ```<STATE>```. Le nombre d'états dans une machine donnée n'est pas limité.
* Le nom d'un état (attribut **name**) doit être unique pour une machine donnée
* L'attribut **dur** permet de configurer la durée de l'état. Il est possible d'indiquer deux durée dans cet attribut, en les séparant par un point-virgule. A chaque fois que la machine entrera dans l'état, une durée sera tirée aléatoirement entre ces deux durées.
* Les attributs **initial** et **final** permettent d'indiquer qu'un état est initial ou final respectivement. **Attention : un état ne peut pas être à la fois initial et final.** De plus, puisque l'automate est déterministe, un seul état peut être l'état initial d'une machine.
* La simulation d'une machine s'arrête dès lors qu'un état final est atteint. Il n'est donc pas nécessaire de préciser la durée de ceux-ci.
* L'attribut **cons** permet de configurer la consommation de l'état, qui sera transmise dans les fichiers du générateur. Toute expression fonctionelle pour le générateur convient ici, cela inclut celles utilisant les variables **x** et **b**.
* Les attributs **tran** et **dila** permettent de configurer les transformations qui pourront être appliquées par le générateur sur la consommation de l'état. Elles prennent comme arguments une ou deux valeur flottantes (séparées par un point-virgule) et correspondent respectivement aux transformations **translation verticale** et **dilatation verticale** (pour plus d'informations, consultez la section **Utilisation de la librairie mXparser** du générateur).

#### Expression d'une durée
* Une durée peut être exprimée de deux façons, soit comme une constante, soit comme une variable.
* Si l'on souhaite exprimer une durée comme une constante, il faut utiliser le format ```XdYhZmYs```. Ici, *X, Y, Z* et *T* sont des **entiers** qui représentent le nombre de jours, heures, minutes et secondes de la durée respectivement. Voici quelques exemples de formules valides exprimant une durée :
	* 1m30s
	* 10h10s
	* 1d2h3m4s

* On peut également exprimer une durée sous la forme d'une variable. Pour cela, faut employer une expression MXParser. Aussi, un format valide pour le générateur le sera aussi pour le simulateur, à la différence que les variables **x** et **b** telles que décrites dans le générateur n'existent pas dans le contexte du simulateur, il est donc impossible des les utiliser (pour plus d'informations, consultez la section **Utilisation de la librairie mXparser** de la documentation du générateur)

### Transition
* Une transition d'une machine est représentée par une balise ```<TRANSITION>```. Le nombre de transitions dans une machine donnée n'est pas limité.
* Le nom d'une transition (attribut **name**) doit être unique pour une machine donnée
* Les attributs **from** et **to** permettent de configurer l'état d'origine et de destination respectivement. Plusieurs *Transitions* peuvent avoir les mêmes états d'origine et de destination. Il est nécessaire de déclarer les états utilisés par la transition avant de déclarer la transition, il suffit alors de préciser le nom (**name**) de l'état pour y faire référence. Ces deux états peuvent être identiques (dans le cas d'une boucle).

#### Expression des conditions d'une transition
* L'attribut **cond** d'une *Transition* permet d'exprimer les conditions nécessaires pour permettre que la transition soit effectuée. Celles-ci sont exprimées sous forme de formules dont le format est détaillé plus bas.
* Plusieurs conditions peuvent être exprimées dans l'attribut **cond**, il suffit de séparer les différentes conditions par un point-virgule.
* Il faut que toutes les conditions décrites dans **cond** soient vérifiées pour que la condition soit validée (ET logique). Pour avoir plusieurs façons différentes d'exécuter la même transition (OU logique), il faut déclarer deux transitions différentes.
* Une condition nécessite d'employer un *opérateur de comparaison*. Voici la liste des opérateurs possibles :
	* *eq* - égal à
	* *gt* - strictement supérieur à
	* *gte* - supérieur ou égal à
	* *lt* - strictement inférieur à
	* *lte* - inférieur ou égal à
* Une condition est composée du nom de la variable qu'elle est teste, d'un opérateur de comparaison et d'une expression MXParser.
  * Voici quelques exemples de conditions valides :
  	* *Foo* eq 1.2
  	* *Foo* lte Bar + 5
  	* *Foo* gt 2 * Bar + Oof / 2
  	
#### Expression des effets d'une transition
* L'attribut **eff** d'une *Transition* permet d'exprimer les effets de la traversée de cette dernière. Un effet est une opération modifiant la valeur d'une variable. Celle-ci sont exprimées sous forme de formules dont le format est détaillé plus bas.
* Plusieurs effets peuvent être décrits dans l'attribut **eff**, il suffit de séparer les différents effets par un point-virgule.
* Lorsque la *Transition* est parcourue, tous ses effets sont exécutés.
* Une opération est exprimée à l'aide du nom de la variable qu'elle est ammenée à modifer, d'un signe **=** et d'une expression MXParser.
* Voici quelques exemples d'opérations valides :
	* *Foo* = 1.2
	* *Foo* = Bar + 5
	* *Foo* = 2 * Bar + Oof / 2

## Fichier d'entrée : Descripteur de Simulation
### Aperçu général

Le fichier d'entrée doit avoir le format suivant :
```xml

<SIMULATION count="2" step="1m">  
	 <LAUNCH machine="machine" date="7h;8h"/>  
	 ...
	 <EVOLUTION name="evolution" effect="Foo = Foo + 1;" dur="1h;2h"/>  
	 ...
	 <ACTION name="action" effect="Foo = Foo * 2;" date="8h;9h" />  
	 ...
</SIMULATION>
```
### Simulation
* Une simulation est représentée par une balise `<SIMULATION>`. Chaque fichier doit contenir une et une seule balise `<SIMULATION>`.
* L'attribut **count** permet de configurer le nombre de simulations qui seront effectuées par le simulateur. Chaque simulation possède les mêmes paramètres initiaux que toutes les autres, mais tous les choix aléatoires sont recalculés.
* L'attribut **step** permet simplement de préciser quelle sera la valeur du pas utilisé par le générateur (cf. documentation du générateur). Par défaut, la pas est de une minute.

### Lancement des machines
* Les heures de lancement des machines sont paramétrés à l'aide de la balise `<LAUNCH>`. Pour paramétrer le lancement d'une *Machine* dans un intervalle de temps, il suffit de préciser le nom de la machine dans l'attribut **machine** tel que déclaré dans le fichier précédent, puis d'indiquer la date à l'aide de l'attribut **date**. Les dates doivent être exprimées de la même façon que des durées (cf. section **Expression d'une durée**), à l'exception près qu'elles doivent impérativement être constantes (elles ne peuvent pas dépendre des variables de la simulation).
### Actions faites sur le système pendant la simulation
* Pour pouvoir influer sur le système pendant qu'une simulation est en cours, il faut utiliser les *Actions* et les *Evolutions*.
* Une *Action* est représentée par une balise `<ACTION>`. Une action est une série d'effets qui sont réalisés une fois, à une date précisée. Pour configurer une *Action*, il suffit de préciser ses effets en utilisant la même syntaxe que pour les transitions (cf. la section **Expression des effets d'une transition**) et l'attribut **eff**, puis de préciser les bornes de la date à laquelle l'*Action* est effectuée (attribut **date**, au format identique à celui de la balise `<LAUNCH>`).
* Une *Evolution* est une *Action* qui n'a pas de date connue à l'avance, mais qui se répète au bout d'une certaine durée. Est calculée en partant de l'heure de début de la simulation. Pour configurer une *Evolution*, il suffit de préciser ses effets avec l'attibut **eff**, puis la durée entre deux exécutions de l'évolution (attribut **dur**, au format identique à ceux d'une durée)

## Lancer le simulateur


Pour lancer le simulateur, il est nécessaire d'utiliser la ligne de commande suivante :

`$ java -jar -prodsys_input system.xml -simulation_input simulation.xml -output logs -g`

où :
*"system.xml"* est le chemin du fichier descripteur du système de production
*"simulation.xml"* est le chemin du fichier descripteur de la simulation
*"logs"* est le chemin du fichier du dossier de sortie des résultats de la simulation

* Si les fichiers d'entrée du générateur ne sont pas désirés, on peut ommettre -g.

* Il est possible d'ajouter -w à la ligne de commander pour éviter les alertes de boucles infinies (alerte automatique qui évite que les logs d'une boucle infinie remplissent débordent). Il est fortement recommandé d'utiliser cette option uniquement si le système de production a déjà été testé.

## Résultat de la simulation

* Les fichiers résultant sont soit au format texte (.log) pour les logs de la simulation ou des machines, soit au format xml (.xml) pour les fichiers prévus pour le générateur.
* Les fichiers logs contiennent une description exhaustive des événements de chaque simulation, en verbose. Les fichiers xml sont formatés pour être utilisables par le générateur, chacun représentant alors la consumption d'une machine pendant une simulation.
* Ces fichiers sont nommés de la façon suivante :
	* s_*[nom du système]*_*[numéro de la simulation]*.log : les logs des simulations entières.
	* m_*[nom de la machine]*_*[numéro de la simulation]*.log : les logs des machines.
	* generator/*[nom de la machine]*_*[numéro de la simulation]*.xml : les fichiers d'entrée du générateur.

**Exemple de résultat de simulation :**
TODO

