package simulator;

import simulator.model.productionsystem.ProductionSystem;
import simulator.model.productionsystem.ProductionSystemSetupException;
import simulator.model.simulation.Simulation;
import simulator.model.simulation.SimulationSetupException;
import simulator.parser.SimulatorParser;

import java.io.File;
import java.io.IOException;

// TODO : name of MX methods/objects
// TODO : m --> min

/**
 * Main class of simulator
 * Parse the production system (-ps), the simulation (-s) then execute the simulation and generate logs files (-o)
 * and generator input files if desired (-o)
 */
public class Main {

    public static void main(String[] args) {

        boolean generateGenerator = false;
        boolean ignoreWarnings = false;
        String prodsys = null;
        String simulation = null;
        String output = null;

        try {
            if (args.length < 6)
                throw new IllegalArgumentException("Too few parameters defined");
            if (args.length > 8)
                throw new IllegalArgumentException("Too many parameters defined");

            for (int i = 0; i < args.length; i++) {
                final String arg = args[i];

                switch (arg) {
                    case "-prodsys_input":
                    case "-ps":
                        if (++i == args.length) throw new IllegalArgumentException("Missing production system filepath");
                        if (prodsys != null) throw new IllegalArgumentException("Multiple production system filepaths");
                        prodsys = args[i];
                        break;
                    case "-simulation_input":
                    case "-s":
                        if (++i == args.length) throw new IllegalArgumentException("Missing simulation filepath");
                        if (simulation != null) throw new IllegalArgumentException("Multiple simlation filepaths");
                        simulation = args[i];
                        break;
                    case "-output":
                    case "-o":
                        if (++i == args.length) throw new IllegalArgumentException("Missing output folder path");
                        output = args[i];
                        break;
                    case "-generator":
                    case "-g":
                        generateGenerator = true;
                        break;
                    case "-warnings":
                    case "-w":
                        ignoreWarnings = true;
                        break;
                    default:
                        throw new IllegalArgumentException(String.format("Unknown parameter %s", arg));
                }
            }
            if (prodsys == null) throw new IllegalArgumentException("Missing production system descriptor filepath");
            if (simulation == null) throw new IllegalArgumentException("Missing simulation descriptor filepath");
            if (output == null) throw new IllegalArgumentException("Missing output folder path");
        } catch (IllegalArgumentException e) {
            System.out.println("Program parameters error : " + e.getMessage());
            System.out.println("Parameters format : -prodsys_input (or -ps) \"system.xml\" -simulation_input (or -s) \"simulation.xml\" -output \"folderPath\" -g (optional : writes generator input files) -w (optional : disables warnings)");
            return;
        }

        final File dir = new File(output);
        if (!dir.mkdir() && !dir.isDirectory()) {
            System.out.println("Error : Could not create/find log directory");
            return;
        }

        ProductionSystem ps;
        Simulation sim;

        try {
            ps = SimulatorParser.generateProductionSystem(prodsys);
            sim = SimulatorParser.generateSimulation(simulation, ps);
            sim.simulate(output, generateGenerator, ignoreWarnings);
        } catch (SimulationSetupException e) {
            System.out.printf("Error with the simulation : %s%n", e.getMessage());
            System.exit(1);
        } catch (ProductionSystemSetupException e) {
            System.out.printf("Error with the production system : %s%n", e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.out.printf("IO error during simulation : %s%n", e.getMessage());
            System.exit(1);
        }

    }
}
