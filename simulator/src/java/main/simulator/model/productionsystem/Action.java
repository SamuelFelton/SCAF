package simulator.model.productionsystem;

import java.util.List;

/**
 * An Action is a named list of operations.
 */
public class Action {

    private final String name;
    private final List<Operation> operations;

    /**
     * An action can be one of two types :
     * - A simple action, that should be executed once.
     * - A recurring evolution, that should be executed regularly as long as the simulation is running.
     */
    private final Type type;

    public Action(String name, List<Operation> operations, Type type) {
        this.name = name;
        this.operations = operations;
        this.type = type;
    }

    /**
     * Executes every operation of this action.
     */
    public void execute() {
        for (Operation operation : operations) {
            operation.execute();
        }
    }

    public String getName() {
        return name;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        ACTION,
        EVOLUTION
    }

}
