package simulator.model.productionsystem;

/**
 * A Condition represents an evaluable condition.
 */
public interface Condition {

    /**
     * Evaluates this condition
     * @return true if this condition is validated, false otherwise
     */
    boolean evaluate();
}
