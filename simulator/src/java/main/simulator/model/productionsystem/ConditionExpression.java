package simulator.model.productionsystem;


import org.mariuszgromada.math.mxparser.Expression;

/**
 * Implements Condition by comparing a variable to a mXparser expression.
 * It contains a variable, a type (operator), and an expression.
 * It must have a reference to the production system in order to calculate the value of the expression.
 */
public class ConditionExpression implements Condition {

    private final ProductionSystem system;
    private final Variable variable;
    private final Type type;
    private final Expression value;


    public ConditionExpression(ProductionSystem system, Variable variable, Type type, Expression value) {
        this.system = system;
        this.variable = variable;
        this.type = type;
        this.value = value;
    }

    /**
     * Evaluates this Condition.
     * @return true if this condition is validated, false otherwise.
     */
    @Override
    public boolean evaluate() {
        double value = system.calculate(this.value);

        switch (type) {
            case GREATER:
                return variable.getValue() > value;
            case GREATER_EQUAL:
                return variable.getValue() >= value;
            case LESSER:
                return variable.getValue() < value;
            case LESSER_EQUAL:
                return variable.getValue() <= value;
            case EQUAL:
                return variable.getValue() == value;
            case NOT_EQUAL:
                return variable.getValue() != value;
            default:
                throw new IllegalArgumentException("Should never happen : Condition has unknown type");
        }
    }

    @Override
    public String toString() {
        return String.format("ConditionExpression{variable=%s, type=%s, value=%s}", variable, type, value);
    }

    public enum Type {
        GREATER,
        GREATER_EQUAL,
        LESSER,
        LESSER_EQUAL,
        EQUAL,
        NOT_EQUAL
    }
}
