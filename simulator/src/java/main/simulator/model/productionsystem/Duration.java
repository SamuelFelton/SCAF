package simulator.model.productionsystem;

/**
 * A Duration represents a finite amount of time
 */
public abstract class Duration implements Comparable {

    /**
     * @return this Duration expressed in seconds.
     */
    public abstract int toSeconds();

    /**
     * @return this Duration formatted as a date.
     */
    public abstract String toDate();

    /**
     * Compares Duration object with the specified Duration for order.
     * @param o the Duration to be this Duration will be compared to.
     * @return negative integer, zero, or a positive integer as this Duration is less
     * than, equal to, or greater than the specified Duration.
     */
    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Duration)) throw new ClassCastException("Can only be compared to a Duration");
        return toSeconds() - ((Duration) o).toSeconds();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Duration duration = (Duration) o;
        return toSeconds() == duration.toSeconds();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
