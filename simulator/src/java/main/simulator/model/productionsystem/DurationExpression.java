package simulator.model.productionsystem;

import org.mariuszgromada.math.mxparser.Expression;

/**
 * Implements Duration with an mXparser Expression.
 * It must have a reference to the production system in order to calculate the value of the expression.
 */
public class DurationExpression extends Duration {

    private final ProductionSystem system;
    private final Expression expression;

    public DurationExpression(ProductionSystem system, Expression expression) {
        this.system = system;
        this.expression = expression;
    }

    @Override
    public int toSeconds() {
        return Double.valueOf(system.calculate(expression)).intValue();
    }

    @Override
    public String toDate() {
        return new DurationNumeric(this.toSeconds()).toDate();
    }

    @Override
    public String toString() {
        return String.format("DurationVariable{expression=%s}", expression.getExpressionString());
    }
}
