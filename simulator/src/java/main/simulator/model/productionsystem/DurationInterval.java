package simulator.model.productionsystem;

import java.util.Objects;
import java.util.Random;

/**
 * A DurationInterval represents an interval of durations.
 */
public class DurationInterval {

    private final Duration durationMin;
    private final Duration durationMax;

    private final Random random = new Random(System.nanoTime());

    public DurationInterval(int durationMin, int durationMax) {
        this(new DurationNumeric(durationMin), new DurationNumeric(durationMax));
    }

    public DurationInterval(Duration durationMin, Duration durationMax) {
        this.durationMin = durationMin;
        this.durationMax = durationMax;
    }

    public DurationNumeric computeDuration() {
//        if (durationMin.compareTo(durationMax) > 0) throw new IllegalArgumentException("Minimum duration must not be higher than maximum duration");
        if (durationMin.equals(durationMax)) return new DurationNumeric(durationMin.toSeconds());
        return new DurationNumeric((int) (durationMin.toSeconds() + random.nextDouble() * (durationMax.toSeconds() - durationMin.toSeconds())));
    }

    public boolean contains(Duration duration) {
        return (duration.compareTo(durationMin) >= 0) && (durationMax.compareTo(duration) >= 0);
    }

    public Duration getDurationMin() {
        return durationMin;
    }

    public Duration getDurationMax() {
        return durationMax;
    }

    @Override
    public String toString() {
        return String.format(
                "DurationInterval{" +
                        "durationMin='%s', " +
                        "durationMax='%s'}",
                durationMin,
                durationMax);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DurationInterval that = (DurationInterval) o;
        return Objects.equals(durationMin, that.durationMin) &&
                Objects.equals(durationMax, that.durationMax);
    }

    @Override
    public int hashCode() {
        return Objects.hash(durationMin, durationMax);
    }
}
