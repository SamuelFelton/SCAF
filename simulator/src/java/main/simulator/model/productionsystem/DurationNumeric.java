package simulator.model.productionsystem;

/**
 * Implements Duration with a final integer value (split into fours parts : days, hours, minutes, and seconds).
 */
public class DurationNumeric extends Duration {

    private final int days;
    private final int hours;
    private final int minutes;
    private final int seconds;

    public DurationNumeric(int seconds) {
        if (seconds < 0) throw new IllegalArgumentException("A DurationNumeric cannot be negative");
        int leftover = seconds;
        this.days = leftover / 86400;
        leftover = leftover % 86400;
        this.hours = leftover / 3600;
        leftover = leftover % 3600;
        this.minutes = leftover / 60;
        leftover = leftover % 60;
        this.seconds = leftover;
    }

    public DurationNumeric(int days, int hours, int minutes, int seconds) {
        this(days * 86400 + hours * 3600 + minutes * 60 + seconds);
    }

    public DurationNumeric(int hours, int minutes, int seconds) {
        this(hours * 3600 + minutes * 60 + seconds);
    }

    public DurationNumeric(int minutes, int seconds) {
        this(minutes * 60 + seconds);
    }

    /**
     * Creates a new DurationNumeric equal to the sum of this DurationNumeric and the current value in seconds of the
     * specified Duration.
     * @param duration the Duration the be added to this DurationNumeric.
     * @return a new Duration equal to the sum of this DurationNumeric
     * and the current value in seconds of the specified Duration.
     */
    public DurationNumeric plus(Duration duration) {
        return new DurationNumeric(this.toSeconds() + duration.toSeconds());
    }

    public int getDays() {
        return days;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    @Override
    public int toSeconds() {
        return (days * 86400 + hours * 3600 + minutes * 60 + seconds);
    }

    @Override
    public String toDate() {
        return String.format("%s%s'%s\"%s", days > 0 ? days + ":" : "", hours < 10 ? "0" + hours : hours, minutes < 10 ? "0" + minutes : minutes, seconds < 10 ? "0" + seconds : seconds);
    }

    @Override
    public String toString() {
        if (toSeconds() == 0) return "0s";
        return String.format("%s%s%s%s", days == 0 ? "" : days + "d", hours == 0 ? "" : hours + "h", minutes == 0 ? "" : minutes + "m", seconds == 0 ? "" : seconds + "s");
    }
}
