package simulator.model.productionsystem;

import java.util.*;

/**
 * A machine represent a single deterministic finite automaton.
 * It contains a set of states and a set of transitions (both mapped by their names).
 * It has a singular initial state, a set of final states, and a name.
 */
public class Machine {

    private final String name;

    private final Map<String, State> states;
    private final Map<String, Transition> transitions;
    private final Set<State> finalStates;
    private State initialState;
    private State currentState;

    Machine(String name) {
        this.name = name;
        this.states = new HashMap<>();
        this.transitions = new HashMap<>();
        finalStates = new HashSet<>();
    }

    /**
     * Sets the current state of this machine to be the initial state of this machine.
     * @throws ProductionSystemSetupException if no initial state was declared
     */
    public void launch() throws ProductionSystemSetupException {
        if (initialState == null) {
            throw new ProductionSystemSetupException("No initial state for " + toString());
        }
        currentState = initialState;
    }

    /**
     * @return true if this machine is on a state declared as final, false otherwise
     */
    public boolean isStopped() {
        return (currentState == null || finalStates.contains(currentState));
    }

    /**
     * Checks every transition for the current state of this machine until it finds one that is validated,
     * then executes it.
     * Also updates the value of the current state of this machine.
     * @return the executed transition
     * @throws ProductionSystemSetupException if no validated transition was found for the current state
     */
    public Transition transition() throws ProductionSystemSetupException {
        if (currentState == null)
            throw new ProductionSystemSetupException(toString() + " was not properly initialized");
        if (!isStopped()) {
            for (Transition transition : transitions.values()) {
                if (transition.from() == currentState && transition.evaluateConditions()) {
                    transition.executeEffects();
                    currentState = transition.to();
                    return transition;
                }
            }
        }
        return null;
    }

    /**
     * Computes a duration for the current state of this machine.
     * @return the computed duration.
     */
    public DurationNumeric computeCurrentStateDuration() {
        if (isStopped()) return new DurationNumeric(0);
        return currentState.computeDuration();
    }

    /**
     * Creates a new State.
     * The name must not already be in use for this Machine.
     * @param name the name of the new State.
     * @param durationMin a bound of the possible durations for the state.
     * @param durationMax the other bound of the possible durations for the state.
     * @return the created State object.
     */
    public State createState(String name, Duration durationMin, Duration durationMax) {
        if (states.containsKey(name)) {
            throw new IllegalArgumentException(String.format("State name %s already in use for the machine %s", name, this.name));
        }
        final State state = new State(name, durationMin, durationMax);
        states.put(name, state);
        return state;
    }

    /**
     * Creates a new Transition.
     * The name must not already be in use for this Machine.
     * @param name the name of the new Transition.
     * @param from the origin state of the Transition. Must have been created through the Machine.
     * @param to the destination of the Transition. Must have been created through the Machine.
     * @return the created Transition object.
     */
    public Transition createTransition(String name, State from, State to) {
        if (transitions.containsKey(name)) {
            throw new IllegalArgumentException(String.format("Transition name %s already in use for machine %s", name, this.name));
        }
        if (!states.containsValue(from) || !states.containsValue(to))
            throw new IllegalArgumentException(String.format("Transition %s uses a State unknown to the Machine %s", name, this.name));
        final Transition transition = new Transition(name, from, to);
        transitions.put(name, transition);
        return transition;
    }

    // Getters

    public String getName() {
        return name;
    }

    public State getState(String name) {
        return states.get(name);
    }

    public Set<State> getStates() {
        return Collections.unmodifiableSet(new HashSet<>(states.values()));
    }

    public Transition getTransition(String name) {
        return transitions.get(name);
    }

    public Set<Transition> getTransitions() {
        return Collections.unmodifiableSet(new HashSet<>(transitions.values()));
    }

    public State getCurrentState() {
        return currentState;
    }

    public State getInitialState() {
        return initialState;
    }

    public void setInitialState(State initialState) {
        if (!states.containsValue(initialState))
            throw new IllegalArgumentException(String.format("Machine %s does not contain State %s", name, initialState.getName()));
        this.initialState = initialState;
    }

    public void addFinalState(State finalState) {
        if (!states.containsValue(finalState))
            throw new IllegalArgumentException(String.format("Machine %s does not contain State %s", name, finalState.getName()));
        finalStates.add(finalState);
    }

    public Set<State> getFinalStates() {
        return Collections.unmodifiableSet(finalStates);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Machine machine = (Machine) o;
        return Objects.equals(name, machine.name) &&
                Objects.equals(states, machine.states) &&
                Objects.equals(transitions, machine.transitions) &&
                Objects.equals(initialState, machine.initialState) &&
                Objects.equals(finalStates, machine.finalStates) &&
                Objects.equals(currentState, machine.currentState);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return String.format("Machine{name=%s, states=%s, transitions=%s}", name, states, transitions);
    }

}
