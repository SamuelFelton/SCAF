package simulator.model.productionsystem;

import java.util.List;

/**
 * An operation modifies the value of one or more variables.
 */
public interface Operation {

    /**
     * Executes this Operation
     */
    void execute();

    /**
     * @return a List containing every variable modified by this Operation
     */
    List<Variable> variables();
}
