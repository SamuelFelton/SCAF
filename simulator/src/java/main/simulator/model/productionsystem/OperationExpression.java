package simulator.model.productionsystem;

import org.mariuszgromada.math.mxparser.Expression;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Implements Operation by setting the value of a single variable to the current value of a mXparser expression.
 * It contains a variable and an expression.
 * It must have a reference to the production system in order to calculate the value of the expression.
 */
public class OperationExpression implements Operation {

    private final ProductionSystem system;
    private final Variable variable;
    private final Expression expression;

    public OperationExpression(ProductionSystem system, Variable variable, Expression expression) {
        this.system = system;
        this.variable = variable;
        this.expression = expression;
    }

    @Override
    public void execute() {
        variable.setValue(system.calculate(expression));
    }

    @Override
    public List<Variable> variables() {
        return Collections.singletonList(variable);
    }
}
