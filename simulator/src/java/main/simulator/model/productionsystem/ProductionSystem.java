package simulator.model.productionsystem;

import org.mariuszgromada.math.mxparser.Expression;

import java.util.*;

/**
 * Model for the Simulator.
 * A production system represents an set of machines that can run simultaneously.
 * It contains a set of machines and a set of variables (both mapped by their names).
 * It has a name.
 */
public class ProductionSystem {

    private final String name;

    private final Map<String, Machine> machines;
    private final Map<String, Variable> variables;


    public ProductionSystem(String name) {
        this.name = name;
        this.machines = new HashMap<>();
        this.variables = new HashMap<>();
    }

    /**
     * Launches every machine and initializes every value.
     * @throws ProductionSystemSetupException if a machine fails to launch (no initial state declared).
     */
    public void launch() throws ProductionSystemSetupException {
        for (Machine machine : machines.values()) {
            machine.launch();
        }
        for (Variable variable : variables.values()) {
            variable.initialize();
        }
    }

    /**
     * Calculates the current value of an expression by replacing the variables by their current values.
     * The expression arguments must have been correctly declared before-hand,
     * and only contain variables known to this ProductionSystem.
     * @param expression the expression that whose value will be returned.
     * @return the current value of the expression.
     */
    public double calculate(Expression expression) {
        for (int i = 0; i < expression.getArgumentsNumber(); i++) {
            String argument = expression.getArgument(i).getArgumentName();
            Variable variable = getVariable(argument);
            if (variable == null)
                throw new IllegalArgumentException(String.format("Unknown variable %s used in expression %s", argument, expression.getExpressionString()));
            expression.setArgumentValue(argument, variable.getValue());
        }
        return expression.calculate();
    }

    /**
     * Creates a new Machine.
     * The name must not already be in use for this ProductionSystem.
     * @param name the name of the new Machine.
     * @return the created Machine object.
     */
    public Machine createMachine(String name) {
        if (machines.containsKey(name)) {
            throw new IllegalArgumentException(String.format("Machine name %s already in use for this production system %s",name, this.name));
        }
        final Machine machine = new Machine(name);
        machines.put(name, machine);
        return machine;
    }

    /**
     * Creates a new Variable.
     * The name must not already be in use for this ProductionSystem.
     * @param name the name of the new Variable.
     * @param value the initial value of the new Variable.
     * @return the created Variable object.
     */
    public Variable createVariable(String name, double value) {
        if (variables.containsKey(name)) {
            throw new IllegalArgumentException(String.format("Variable name %s already in use for production system %s", name, this.name));
        }
        final Variable variable = new Variable(name, value);
        variables.put(name, variable);
        return variable;
    }

    // Getters

    public String getName() {
        return name;
    }

    public Machine getMachine(String name) {
        return machines.get(name);
    }

    public Set<Machine> getMachines() {
        return Collections.unmodifiableSet(new HashSet<>(machines.values()));
    }


    public Variable getVariable(String name) {
        return variables.get(name);
    }

    public Set<Variable> getVariables() {
        return Collections.unmodifiableSet(new HashSet<>(variables.values()));
    }

    @Override
    public String toString() {
        return String.format("ProductionSystem{name=%s, machines=%s, variables=%s}", name, machines, variables);
    }


}
