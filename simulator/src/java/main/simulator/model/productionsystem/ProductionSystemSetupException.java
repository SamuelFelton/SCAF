package simulator.model.productionsystem;

/**
 * Exception thrown when an error occurs while setting up or launching the production system
 */
public class ProductionSystemSetupException extends Exception {

    public ProductionSystemSetupException(String s) {
        super(s);
    }
}
