package simulator.model.productionsystem;

import org.mariuszgromada.math.mxparser.Expression;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A State represents an automaton state.
 * It has a interval of durations representing all its possible durations, a expression representing its consumption,
 * a list of allowed transformations to be applied to this expression, and a name.
 */
public class State {

    private final String name;

    private final DurationInterval duration;

    private Expression consumption;

    /**
     * Transformations are stored as follows : translation minimum, translation maximum,
     * dilatation minimum, dilatation maximum.
     */
    private List<Double> transformations;

    State(String name, Duration durationMin, Duration durationMax) {
        this.name = name;
        this.duration = new DurationInterval(durationMin, durationMax);
        this.consumption = new Expression("0");
        transformations = Arrays.asList(0., 0., 1., 1.);
    }

    /**
     * Randomly chooses a duration; in between the declared duration interval of this State;
     * @return the chosen duration
     */
    public DurationNumeric computeDuration() {
        return duration.computeDuration();
    }

    public String getName() {
        return name;
    }

    public Expression getConsumption() {
        return consumption;
    }

    public void setConsumption(Expression consumption) {
        this.consumption = consumption;
    }

    public List<Double> getTransformations() {
        return Collections.unmodifiableList(transformations);
    }

    public void setTransformations(List<Double> transformations) {
        if (transformations.size() != 4)
            throw new IllegalArgumentException("Transformations must be a list of 4 doubles : dilatation min/max and translation min/max");
        this.transformations = transformations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return Objects.equals(name, state.name) &&
                Objects.equals(duration, state.duration) &&
                Objects.equals(consumption, state.consumption) &&
                Objects.equals(transformations, state.transformations);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return String.format("State{name=%s, consumption=%s, duration=(%s;%s)}", name, consumption.getExpressionString(), duration.getDurationMin(), duration.getDurationMax());
    }
}
