package simulator.model.productionsystem;

import java.util.*;

/**
 * A Transition represents an automaton transition.
 * It has an origin state, a destination state (they can be the same).
 * It contains a list of conditions and operations.
 */
public class Transition {

    private final String name;

    private final State from;

    private final State to;

    private final List<Condition> conditions;

    private final List<Operation> effects;

    Transition(String name, State from, State to) {
        this.name = name;
        this.from = from;
        this.to = to;
        conditions = new LinkedList<>();
        effects = new LinkedList<>();
    }

    /**
     * @return true if every condition of this Transition is validated, false otherwise.
     */
    public boolean evaluateConditions() {
        if (conditions.size() != 0) {
            for (Condition condition : conditions) {
                if (!condition.evaluate()) return false;
            }
        }
        return true;
    }

    /**
     * Executes every operation of this transition.
     */
    public void executeEffects() {
        for (Operation effect : effects) {
            effect.execute();
        }
    }

    public void addCondition(Condition condition) {
        this.conditions.add(condition);
    }

    public void addEffect(Operation effect) {
        this.effects.add(effect);
    }

    public void addCondition(Collection<Condition> conditions) {
        this.conditions.addAll(conditions);
    }

    public void addEffect(Collection<Operation> effects) {
        this.effects.addAll(effects);
    }

    public String getName() {
        return name;
    }

    public State from() {
        return from;
    }

    public State to() {
        return to;
    }

    public Collection<Condition> getConditions() {
        return Collections.unmodifiableList(conditions);
    }

    public Collection<Operation> getEffects() {
        return Collections.unmodifiableList(effects);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transition that = (Transition) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(from, that.from) &&
                Objects.equals(to, that.to) &&
                Objects.equals(conditions, that.conditions) &&
                Objects.equals(effects, that.effects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return String.format("Transition{name=%s, from=%s, to=%s}", name, from.getName(), to.getName());
    }
}
