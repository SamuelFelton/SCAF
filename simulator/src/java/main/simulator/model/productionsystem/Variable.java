package simulator.model.productionsystem;

import java.util.Objects;

/**
 * A variable stores a single double value during a simulation.
 * It has a an initial value, a current value, and a name.
 */
public class Variable {

    private final String name;
    private final double initialValue;
    private double value;

    Variable(String name, double value) {
        this.name = name;
        this.initialValue = value;
        initialize();
    }

    /**
     * Sets this initial value of the Variable as the current value of this Variable.
     */
    public void initialize() {
        value = initialValue;
    }

    public double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("Variable{name=%s, initialValue=%f, value=%f}", name, initialValue, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variable variable = (Variable) o;
        return Double.compare(variable.initialValue, initialValue) == 0 &&
                Double.compare(variable.value, value) == 0 &&
                Objects.equals(name, variable.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String getName() {
        return name;
    }
}
