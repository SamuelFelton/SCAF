package simulator.model.simulation;

import simulator.model.productionsystem.DurationNumeric;
import simulator.model.productionsystem.State;

import java.util.List;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * A formatter that formats log records of every machine-related event of a simulation
 * into a usable generator input file (xml format).
 */
class MachineGeneratorInputFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        switch (record.getMessage()) {
            case Simulation.TRANSITION: {
                final Object[] params = record.getParameters();
                final State state = (State) params[0];
                final DurationNumeric duration = (DurationNumeric) params[1];
                if (duration.toSeconds() == 0) return "";
                List<Double> transformations = state.getTransformations();
                StringBuilder result = new StringBuilder();
                result.append(String.format(
                        "\t\t<SEQUENCE durationMin=\"%d\" durationMax=\"%d\">%n" +
                                "\t\t\t<FUNCTION value=\"%s\"/>%n",
                        duration.toSeconds(),
                        duration.toSeconds(),
                        state.getConsumption().getExpressionString()));
                if (transformations.size() != 0) {
                    if (transformations.get(0) != 0. || transformations.get(1) != 0.) {
                        result.append(String.format(
                                "\t\t\t<TRANSFORMATION type=\"verticalTranslation\" min=\"%s\" max=\"%s\"/>%n",
                                transformations.get(0),
                                transformations.get(1)));
                    }
                    if (transformations.get(2) != 1. || transformations.get(3) != 1.) {
                        result.append(String.format(
                                "\t\t\t<TRANSFORMATION type=\"verticalDilatation\" min=\"%s\" max=\"%s\"/>%n",
                                transformations.get(2),
                                transformations.get(3)));
                    }
                }
                return result.append(String.format("\t\t</SEQUENCE>%n")).toString();
            }
            case Simulation.START: {
                final Object[] params = record.getParameters();
                return String.format(
                        "<MACHINE step=\"%s\" nbRepetitions=\"1\" name=\"%s\" id=\"%s\">\n" +
                                "\t<MOTIF name=\"motif\" nbRepetitions=\"1\">\n",
                        ((DurationNumeric) params[0]).toSeconds(),
                        params[1],
                        params[4]);
            }
            case Simulation.STOP: {
                return "\t</MOTIF>\n" +
                        "</MACHINE>";
            }
            default:
                throw new RuntimeException(String.format("Unknown log message : %s", record.getMessage()));
        }
    }

}