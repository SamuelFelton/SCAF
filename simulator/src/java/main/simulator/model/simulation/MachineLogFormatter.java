package simulator.model.simulation;

import simulator.model.productionsystem.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * A formatter that formats log records of every machine-related event of a simulation
 * into a readable verbose log file.
 */
public class MachineLogFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        switch (record.getMessage()) {
            case Simulation.TRANSITION: {
                final Object[] params = record.getParameters();
                Transition transition = (Transition)params[2];

                StringBuilder res = new StringBuilder(String.format(
                        "[%s]: reached %s through %s, will last %s.",
                        ((Duration) params[3]).toDate(),
                        ((State) params[0]).getName(),
                        transition.getName(),
                        params[1].toString()));

                List<Variable> variables = new LinkedList<>();

                for (Operation effect : transition.getEffects()) {
                    for (Variable variable : effect.variables()) {
                        if (!variables.contains(variable)) variables.add(variable);
                    }
                }


                for (Variable variable : variables) {
                    res.append(String.format(" %s = %s;", variable.getName(), variable.getValue()));
                }

                res.append(String.format("%n"));
                return res.toString();
            }
            case Simulation.START: {
                final Object[] params = record.getParameters();
                return String.format(
                        "[%s]: Started in state %s%n",
                        ((DurationNumeric) params[2]).toDate(),
                        ((State) params[3]).getName());
            }
            case Simulation.STOP: {
                final Object[] params = record.getParameters();
                return String.format(
                        "[%s]: Stopped%n",
                        ((DurationNumeric) params[0]).toDate());
            }
            default:
                throw new RuntimeException(String.format("Unknown log message : %s", record.getMessage()));
        }
    }
}
