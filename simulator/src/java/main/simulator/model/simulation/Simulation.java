package simulator.model.simulation;

import simulator.model.productionsystem.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Simulation describes the context of a production system run.
 * It contains a reference to the production system, as well as a map of machine launch dates,
 * a map of actions scheduled dates (or delays in case of evolutions), a integer indicating
 * how many simulations must be performed with said context, and a step.
 */
public class Simulation {

    public static final String START = "start";
    public static final String STOP = "stop";
    public static final String MACHINE_STOP = "machineStop";
    public static final String MACHINE_START = "machineStart";
    public static final String TRANSITION = "transition";
    public static final String ABORT = "abort";
    public static final String MANUAL_ABORT = "manualAbort";
    public static final String ACTION = "action";
    private static final String GENERATOR_DIR = "generator";

    private final ProductionSystem productionSystem;
    private final int simulationCount;
    private final Map<Machine, DurationInterval> machinesLaunchDates;
    private final Map<Action, DurationInterval> actions;
    private final DurationNumeric step;

    Simulation(ProductionSystem productionSystem, int simulationCount, Map<Machine, DurationInterval> machinesLaunchDates, Map<Action, DurationInterval> actions, DurationNumeric step) {
        this.productionSystem = productionSystem;
        this.simulationCount = simulationCount;
        this.machinesLaunchDates = machinesLaunchDates;
        this.actions = actions;
        this.step = step;
    }

    /**
     * Runs as many simulations as this Simulation count.
     * @param folderpath path of folder used to store logs and simulations results.
     * @param generateGeneratorInputFiles a boolean indicating whether generator input files are requested or not.
     * @param ignoreWarning a boolean indicating whether infinite loops warnings should be ignored or not.
     * @throws ProductionSystemSetupException when the production system fails to launch.
     * @throws IOException when the log files cannot be written.
     */
    public void simulate(String folderpath, boolean generateGeneratorInputFiles, boolean ignoreWarning) throws ProductionSystemSetupException, IOException {

        int nbSim = 0;
        int daysBeforeWarning = ignoreWarning ? Integer.MAX_VALUE : 25;

        if (generateGeneratorInputFiles) {
            final File genFilepath = new File(String.format("%s/%s", folderpath, GENERATOR_DIR));
            if (!genFilepath.mkdir() && !genFilepath.isDirectory())
                throw new RuntimeException("Could not create log directory");
        }

        while (nbSim++ < simulationCount) {

            DurationNumeric time;

            final TreeMap<DurationNumeric, List<Machine>> runningMachines = new TreeMap<>();
            final TreeMap<DurationNumeric, Action> scheduledActions = new TreeMap<>();

            final Set<Machine> launchedMachines = new HashSet<>();

            // Create a logger for the simulation
            final Logger simulationLogger = Logger.getLogger(Simulation.class.getName());
            final SimulationFileHandler fileHandler = new SimulationFileHandler(String.format("%s/s_%s_%d.log", folderpath, productionSystem.getName(), nbSim));
            fileHandler.setFormatter(new SimulationLogFormatter());
            simulationLogger.addHandler(fileHandler);
            simulationLogger.setLevel(Level.FINE);

            final Map<Machine, Logger> machineLoggers = new HashMap<>();

            productionSystem.launch();

            // for each machine with a specified launch date, assign it a random launch date + prepare a logger
            for (Map.Entry<Machine, DurationInterval> entry : machinesLaunchDates.entrySet()) {
                final Machine machine = entry.getKey();
                final DurationNumeric launchDate = entry.getValue().computeDuration();
                if (!runningMachines.containsKey(launchDate)) {
                    runningMachines.put(launchDate, new ArrayList<>(Collections.singletonList(machine)));
                } else {
                    runningMachines.get(launchDate).add(machine);
                }

                final Logger machineLogger = Logger.getLogger("machine." + machine.getName());
                final SimulationFileHandler machineFileHandler = new SimulationFileHandler(String.format("%s/m_%s_%d.log", folderpath, machine.getName(), nbSim));
                machineFileHandler.setFormatter(new MachineLogFormatter());
                machineLogger.addHandler(machineFileHandler);
                if (generateGeneratorInputFiles) {
                    final SimulationFileHandler machineGeneratorInputHandler = new SimulationFileHandler(String.format("%s/%s/%s_%d.xml", folderpath, GENERATOR_DIR, machine.getName(), nbSim));
                    machineGeneratorInputHandler.setFormatter(new MachineGeneratorInputFormatter());
                    machineLogger.addHandler(machineGeneratorInputHandler);
                }
                machineLogger.setLevel(Level.FINE);

                machine.launch();
                machineLogger.log(Level.FINE, START, new Object[]{step, machine.getName(), launchDate, machine.getCurrentState(), nbSim});

                machineLoggers.put(machine, machineLogger);
            }

            time = runningMachines.firstKey();

            // for each action with a scheduled date, assign it a random date
            // an action may be the very first event of the simulation (they will NOT be ignored)
            for (Map.Entry<Action, DurationInterval> entry : actions.entrySet()) {
                Action action = entry.getKey();
                switch (action.getType()) {
                    case ACTION:
                        scheduledActions.put(entry.getValue().computeDuration(), entry.getKey());
                        break;
                    case EVOLUTION:
                        scheduledActions.put(time.plus(entry.getValue().computeDuration()), entry.getKey());
                        break;
                }
            }

            Machine selectedMachine;

            // Simulation logging
            simulationLogger.log(Level.FINE, START, new Object[]{time, nbSim});

            while (!runningMachines.isEmpty()) {

                // Advance time
                time = runningMachines.firstEntry().getKey();

                // Endless loop detection
                if (time.getDays() >= daysBeforeWarning) {
                    String answer = "";
                    BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
                    while (!answer.equals("a") && !answer.equals("c")) {
                        System.out.printf("Simulation #%d has reached day %d, possible endless loop detected. Abort ? (a/c)%n", nbSim, daysBeforeWarning);
                        answer = consoleReader.readLine();
                    }
                    if (answer.equals("a")) {
                        // Aborting
                        simulationLogger.log(Level.FINE, MANUAL_ABORT, new Object[]{time});

                        // Closing all loggers
                        for (Map.Entry<Machine, Logger> entry : machineLoggers.entrySet()) {
                            ((SimulationFileHandler) entry.getValue().getHandlers()[0]).closeAndDelete();
                        }
                        final SimulationFileHandler simulationHandler = (SimulationFileHandler) simulationLogger.getHandlers()[0];
                        simulationHandler.close();
                        System.out.printf("Simulation aborted, see logs at %s%n", simulationHandler.getFilepath());
                        return;
                    } else {
                        daysBeforeWarning *= 2;
                    }
                }

                // If time passed the scheduled time of an action, execute it
                if (!scheduledActions.isEmpty()) {
                    DurationNumeric nextActionDate = scheduledActions.firstEntry().getKey();
                    while (!scheduledActions.isEmpty() && nextActionDate.compareTo(time) < 0) {
                        Action nextAction = scheduledActions.pollFirstEntry().getValue();
                        for (Operation effect : nextAction.getOperations()) {
                            effect.execute();
                        }

                        // Simulation logging
                        simulationLogger.log(Level.FINE, ACTION, new Object[]{nextActionDate, nextAction});

                        if (nextAction.getType() == Action.Type.EVOLUTION) {
                            scheduledActions.put(nextActionDate.plus(actions.get(nextAction).computeDuration()), nextAction);
                        }

                        if (scheduledActions.isEmpty()) break;
                        nextActionDate = scheduledActions.firstEntry().getKey();
                    }
                }

                // Poll the next machine

                List<Machine> elligibleMachines = runningMachines.pollFirstEntry().getValue();

                selectedMachine = elligibleMachines.get(0);
                elligibleMachines.remove(0);
                if (!elligibleMachines.isEmpty()) runningMachines.put(time, elligibleMachines);

                // Change the machine state
                final Transition executedTransition = selectedMachine.transition();

                if (executedTransition == null) {
                    // Aborting
                    //Simulation logging
                    simulationLogger.log(Level.FINE, ABORT, new Object[]{time, selectedMachine, productionSystem});
                    throw new ProductionSystemSetupException(String.format("No transiton could be executed for machine %s on state %s", selectedMachine.getName(), selectedMachine.getCurrentState().getName()));
                }

                // Simulation logging
                if (!launchedMachines.contains(selectedMachine)) {
                    simulationLogger.log(Level.FINE, MACHINE_START, new Object[]{time, selectedMachine});
                    launchedMachines.add(selectedMachine);
                }
                simulationLogger.log(Level.FINE, TRANSITION, new Object[]{time, executedTransition, selectedMachine, selectedMachine.getCurrentState()});

                final DurationNumeric stateDuration = selectedMachine.computeCurrentStateDuration();

                // Machine logging
                final Logger machineLogger = machineLoggers.get(selectedMachine);
                machineLogger.log(Level.FINE, TRANSITION, new Object[]{selectedMachine.getCurrentState(), stateDuration, executedTransition, time});

                if (!selectedMachine.isStopped()) {
                    DurationNumeric nextDate = time.plus(stateDuration);
                    if (!runningMachines.containsKey(nextDate)) {
                        runningMachines.put(nextDate, new ArrayList<>(Collections.singletonList(selectedMachine)));
                    } else {
                        runningMachines.get(nextDate).add(selectedMachine);
                    }
                } else {

                    // Simulation logging
                    simulationLogger.log(Level.FINE, MACHINE_STOP, new Object[]{time, selectedMachine});

                    // Machine logging
                    machineLogger.log(Level.FINE, STOP, new Object[]{time});
                }

            }

            simulationLogger.log(Level.FINE, STOP, new Object[]{time, nbSim, productionSystem});

            for (final Map.Entry<Machine, Logger> entry : machineLoggers.entrySet()) {
                for (final Handler handler : entry.getValue().getHandlers()) {
                    handler.close();
                }
            }

            for (final Handler handler : simulationLogger.getHandlers()) {
                handler.close();
            }
        }

        System.out.printf("Simulation completed succesfully, see logs at %s%n", folderpath);

    }

    //Getters

    public int getSimulationCount() {
        return simulationCount;
    }

    public Map<Machine, DurationInterval> getMachinesLaunchDates() {
        return Collections.unmodifiableMap(machinesLaunchDates);
    }

    public Map<Action, DurationInterval> getActions() {
        return Collections.unmodifiableMap(actions);
    }

    public DurationNumeric getStep() {
        return step;
    }

    @Override
    public String toString() {
        return String.format("Simulation{productionSystem=%s}", productionSystem);
    }
}
