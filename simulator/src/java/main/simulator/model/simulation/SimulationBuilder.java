package simulator.model.simulation;

import simulator.model.productionsystem.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Builder for the Simulation class.
 */
public class SimulationBuilder {

    private final ProductionSystem productionSystem;
    private final Map<Machine, DurationInterval> machineLaunchDates = new HashMap<>();
    private final Map<Action, DurationInterval> actions = new HashMap<>();
    private int simulationCount = 1;
    private DurationNumeric step = new DurationNumeric(60);

    public SimulationBuilder(ProductionSystem productionSystem) {
        this.productionSystem = productionSystem;
    }

    public SimulationBuilder setSimulationCount(int simulationCount) {
        this.simulationCount = simulationCount;
        return this;
    }

    public SimulationBuilder addMachineLaunchDate(String machineName, DurationNumeric dureeMin, DurationNumeric dureeMax) {
        Machine machine = productionSystem.getMachine(machineName);
        if (machine == null) {
            throw new IllegalArgumentException(String.format("There is no machine named %s in the production system %s", machineName, productionSystem.getName()));
        }
        this.machineLaunchDates.put(machine, new DurationInterval(dureeMin, dureeMax));
        return this;
    }

    public SimulationBuilder addEvolution(String name, List<Operation> effects, Duration dureeMin, Duration dureeMax) {
        actions.put(new Action(name, effects, Action.Type.EVOLUTION), new DurationInterval(dureeMin, dureeMax));
        return this;
    }

    public SimulationBuilder addAction(String name, List<Operation> effects, Duration dureeMin, Duration dureeMax) {
        actions.put(new Action(name, effects, Action.Type.ACTION), new DurationInterval(dureeMin, dureeMax));
        return this;
    }

    public SimulationBuilder setStep(DurationNumeric step) {
        this.step = step;
        return this;
    }

    public Simulation createSimulation() throws SimulationSetupException {
        if (machineLaunchDates.isEmpty())
            throw new SimulationSetupException("No machine launch specified : cannot launch simulation");
        return new Simulation(productionSystem, simulationCount, machineLaunchDates, actions, step);
    }
}