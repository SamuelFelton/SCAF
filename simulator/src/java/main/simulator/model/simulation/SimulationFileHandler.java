package simulator.model.simulation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.FileHandler;

/**
 * Customized FileHandler to support log file deletion before handler closing.
 */
public class SimulationFileHandler extends FileHandler {

    private final String filepath;

    SimulationFileHandler(String pattern) throws IOException, SecurityException {
        super(pattern);
        this.filepath = pattern;
    }

    public String getFilepath() {
        return filepath;
    }

    public void closeAndDelete() {
        try {
            Files.delete(Paths.get(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        close();
    }
}
