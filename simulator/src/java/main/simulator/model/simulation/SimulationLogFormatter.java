package simulator.model.simulation;

import simulator.model.productionsystem.*;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * A formatter that formats log records of every event of a simulation into a readable verbose log file.
 */
class SimulationLogFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        switch (record.getMessage()) {
            case Simulation.TRANSITION: {
                final Object[] params = record.getParameters();
                final Transition transition = (Transition)params[1];

                StringBuilder res = new StringBuilder(String.format(
                        "[%s]: %s reached %s through %s.",
                        ((Duration) params[0]).toDate(),
                        ((Machine) params[2]).getName(),
                        ((State) params[3]).getName(),
                        transition.getName()));

                List<Variable> variables = extractVariables(transition.getEffects());

                for (Variable variable : variables) {
                    res.append(String.format(" %s = %s;", variable.getName(), variable.getValue()));
                }

                res.append(String.format("%n"));
                return res.toString();

            }
            case Simulation.MACHINE_STOP: {
                final Object[] params = record.getParameters();
                return String.format(
                        "[%s]: %s stopped%n",
                        ((Duration) params[0]).toDate(),
                        ((Machine) params[1]).getName());
            }
            case Simulation.MACHINE_START: {
                final Object[] params = record.getParameters();
                return String.format(
                        "[%s]: %s started%n",
                        ((Duration) params[0]).toDate(),
                        ((Machine) params[1]).getName());
            }
            case Simulation.ACTION: {
                final Object[] params = record.getParameters();
                final Action action = (Action) params[1];

                StringBuilder res = new StringBuilder(String.format(
                        "[%s]: %s executed.",
                        ((Duration) params[0]).toDate(),
                        action.getName()));

                List<Variable> variables = extractVariables(action.getOperations());

                for (Variable variable : variables) {
                    res.append(String.format(" %s = %s;", variable.getName(), variable.getValue()));
                }

                res.append(String.format("%n"));
                return res.toString();
            }
            case Simulation.START: {
                final Object[] params = record.getParameters();
                return String.format(
                        "[%s]: Simulation #%s started%n",
                        ((Duration) params[0]).toDate(),
                        String.valueOf(params[1]));
            }
            case Simulation.STOP: {

                final Object[] params = record.getParameters();
                StringBuilder res = new StringBuilder(String.format(
                        "[%s]: Simulation #%s ended%nVariables final values :",
                        ((Duration) params[0]).toDate(),
                        String.valueOf(params[1])));

                final ProductionSystem system = (ProductionSystem)params[2];

                for (Variable variable : system.getVariables()) {
                    res.append(String.format(" %s = %s;", variable.getName(), variable.getValue()));
                }

                res.append(String.format("%n"));
                return res.toString();
            }
            case Simulation.ABORT: {
                final Object[] params = record.getParameters();
                final Machine machine = ((Machine) params[1]);
                StringBuilder res = new StringBuilder(String.format(
                        "[%s]: No transition could be executed on machine %s in state %s, aborting simulation%nVariables values :",
                        ((Duration) params[0]).toDate(),
                        machine.getName(),
                        machine.getCurrentState().getName()));

                final ProductionSystem system = (ProductionSystem)params[2];

                for (Variable variable : system.getVariables()) {
                    res.append(String.format(" %s = %s;", variable.getName(), variable.getValue()));
                }

                res.append(String.format("%n"));
                return res.toString();
            }
            case Simulation.MANUAL_ABORT: {
                final Object[] params = record.getParameters();
                return String.format(
                        "[%s]: Simulation aborted manually%n",
                        ((Duration) params[0]).toDate());
            }
            default:
                throw new RuntimeException(String.format("Unknown log message : %s", record.getMessage()));
        }
    }

    private List<Variable> extractVariables(Collection<Operation> operations) {
        List<Variable> variables = new LinkedList<>();

        for (Operation operation : operations) {
            for (Variable variable : operation.variables()) {
                if (!variables.contains(variable)) variables.add(variable);
            }
        }

        return variables;
    }

}
