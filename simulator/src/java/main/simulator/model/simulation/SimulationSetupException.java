package simulator.model.simulation;

/**
 * Exception thrown when an error occurs while setting up the simulation
 */
public class SimulationSetupException extends Exception {

    public SimulationSetupException(String s) {
        super(s);
    }
}
