package simulator.parser;

import org.mariuszgromada.math.mxparser.Expression;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import simulator.model.productionsystem.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ProductionSystemHandler extends SimulatorBaseHandler {

    private static final String PRODSYS = "prodsys";
    private static final String MACHINE = "machine";
    private static final String VARIABLE = "variable";
    private static final String STATE = "state";
    private static final String INITIAL = "initial";
    private static final String FINAL = "final";
    private static final String TRANSITION = "transition";
    private static final String FROM = "from";
    private static final String TO = "to";
    private static final String INIT = "init";
    private static final String CONSUMPTION = "cons";
    private static final String TRANSLATION = "tran";
    private static final String DILATATION = "dila";

    private Machine machineTemp;
    private boolean done = false;

    ProductionSystemHandler() {
        authorizedAttributes.put(PRODSYS, Collections.singletonList(NAME));
        authorizedAttributes.put(VARIABLE, Arrays.asList(NAME, INIT));
        authorizedAttributes.put(MACHINE, Collections.singletonList(NAME));
        authorizedAttributes.put(STATE, Arrays.asList(NAME, DURATION, INITIAL, FINAL, CONSUMPTION, TRANSLATION, DILATATION));
        authorizedAttributes.put(TRANSITION, Arrays.asList(NAME, FROM, TO, CONDITIONS, EFFECTS));
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ((system == null || done) && !qName.toLowerCase().equals(PRODSYS)) {
            throw new IllegalArgumentException(String.format("%s is declared outside of the production system", qName));
        }
        switch (qName.toLowerCase()) {
            case PRODSYS:
                prodSysHandler(qName, attributes);
                break;
            case MACHINE:
                machineHandler(qName, attributes);
                break;
            case VARIABLE:
                variableHandler(qName, attributes);
                break;
            case STATE:
                stateHandler(qName, attributes);
                break;
            case TRANSITION:
                transitionHandler(qName, attributes);
                break;
            default:
                throw new SAXException("Production system file contains invalid XML tag : " + qName);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName.toLowerCase()) {
            case PRODSYS:
                done = true;
                break;
            case MACHINE:
                if (machineTemp.getInitialState() == null) {
                    throw new IllegalArgumentException(String.format("No state was defined as initial in machine %s", machineTemp.getName()));
                }
                if (machineTemp.getFinalStates().isEmpty()) {
                    throw new IllegalArgumentException(String.format("No states were defined as final in machine %s", machineTemp.getName()));
                }
                machineTemp = null;
                break;
        }
    }

    /**
     * Parses and creates a ProductionSystem.
     * @param qName the qualified name of the machine tag.
     * @param attributes the attributes of the machine tag.
     */
    private void prodSysHandler(String qName, Attributes attributes) {
        if (attributes.getIndex(NAME) == -1) {
            throw new IllegalArgumentException(String.format("%s is missing a name", qName));
        }
        final String name = attributes.getValue(NAME);
        if (system != null) {
            throw new IllegalArgumentException("Multiple production systems declared in a single file");
        }
        checkTagArguments(qName, attributes, name);
        system = new ProductionSystem(name);
    }

    /**
     * Parses a Machine and creates it using the ProductionSystem.
     * @param qName the qualified name of the machine tag.
     * @param attributes the attributes of the machine tag.
     */
    private void machineHandler(String qName, Attributes attributes) {
        if (machineTemp != null) {
            throw new IllegalArgumentException(String.format("Machine declared inside another Machine (%s)", machineTemp.getName()));
        }
        if (attributes.getIndex(NAME) == -1) {
            throw new IllegalArgumentException(String.format("%s is missing a name", qName));
        }
        final String name = attributes.getValue(NAME);

        checkTagArguments(qName, attributes, name);

        machineTemp = system.createMachine(name);

    }

    /**
     * Parses a Variable and creates it using the ProductionSystem.
     * @param qName the qualified name of the variable tag
     * @param attributes the attributes of the variable tag
     */
    private void variableHandler(String qName, Attributes attributes) {
        if (attributes.getIndex(NAME) == -1) {
            throw new IllegalArgumentException(String.format("%s is missing a name", qName));
        }

        final String name = attributes.getValue(NAME);

        checkTagArguments(qName, attributes, name);

        if (attributes.getIndex(INIT) == -1) {
            throw new IllegalArgumentException("Missing init argument in Variable " + name);
        }
        system.createVariable(name, Double.parseDouble(attributes.getValue(INIT)));
    }

    /**
     * Parses a Transition and creates it using the ProductionSystem and the lastly opened machine tag.
     * @param qName the qualified name of the transition tag
     * @param attributes the attributes of the transition tag
     */
    private void transitionHandler(String qName, Attributes attributes) {
        if (machineTemp == null) {
            throw new IllegalArgumentException("Transition declared outside of a machine");
        }

        if (attributes.getIndex(NAME) == -1) {
            throw new IllegalArgumentException("A transition of machine " + machineTemp.getName() + "does not have a name.");
        }
        if (attributes.getIndex(FROM) == -1) {
            throw new IllegalArgumentException("A transition of machine " + machineTemp.getName() + "does not have an origin state.");
        }
        if (attributes.getIndex(TO) == -1) {
            throw new IllegalArgumentException("A transition of machine " + machineTemp.getName() + "does not have a destination state.");
        }

        final String name = attributes.getValue(NAME);
        final String from = attributes.getValue(FROM);
        final String to = attributes.getValue(TO);

        checkTagArguments(qName, attributes, name);

        //Looking for states
        final State fromState = machineTemp.getState(from);
        final State toState = machineTemp.getState(to);
        final Transition transition = machineTemp.createTransition(name, fromState, toState);

        if (attributes.getIndex(CONDITIONS) != -1) {
            final String cond = attributes.getValue(CONDITIONS);
            transition.addCondition(parseConditions(cond));
        }
        if (attributes.getIndex(EFFECTS) != -1) {
            final String effect = attributes.getValue(EFFECTS);
            transition.addEffect(parseOperations(effect));
        }
    }

    /**
     * Parses a State and creates it using the ProductionSystem and the lastly opened machine tag.
     * @param qName the qualified name of the state tag
     * @param attributes the attributes of the state tag
     */
    private void stateHandler(String qName, Attributes attributes) {
        if (attributes.getIndex(NAME) == -1) {
            throw new IllegalArgumentException(String.format("%s is missing a name", qName));
        }
        final String name = attributes.getValue(NAME);

        checkTagArguments(qName, attributes, name);

        boolean isFinal = false;
        if (attributes.getIndex(FINAL) != -1) {
            switch (attributes.getValue(FINAL).toLowerCase()) {
                case "true":
                    isFinal = true;
                    break;
                case "false":
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Attribute %s of tag %s can only be true or false", FINAL, qName));
            }
        }

        boolean isInitial = false;
        if (attributes.getIndex(INITIAL) != -1) {
            switch (attributes.getValue(INITIAL).toLowerCase()) {
                case "true":
                    isInitial = true;
                    break;
                case "false":
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Attribute %s of tag %s can only be true or false", INITIAL, qName));
            }
        }

        if (isFinal && isInitial) {
            throw new IllegalArgumentException("State " + name + " cannot be both final and initial");
        }

        Duration durationMin = new DurationNumeric(0);
        Duration durationMax = new DurationNumeric(0);

        if (attributes.getIndex(DURATION) == -1 && !isFinal) {
            throw new IllegalArgumentException(String.format("Cannot parse duration for state %s", name));
        } else if (!isFinal) {
            final Duration[] durations = parseDurations(attributes.getValue(DURATION));

            durationMin = durations[0];
            durationMax = durations[1];
        }

        final State state = machineTemp.createState(name, durationMin, durationMax);

        if (attributes.getIndex(CONSUMPTION) != -1) {
            final Expression consumption = parseMX(attributes.getValue(CONSUMPTION), true);
            state.setConsumption(consumption);
        }

        List<Double> transformations = Arrays.asList(0., 0., 1., 1.);
        if (attributes.getIndex(TRANSLATION) != -1) {
            final String[] translation = attributes.getValue(TRANSLATION).split(";");
            if (translation.length > 2)
                throw new IllegalArgumentException("Translation has incorrect format : " + attributes.getValue(TRANSLATION));
            transformations.set(0, Double.parseDouble(translation[0]));
            transformations.set(1, translation.length == 2 ? Double.parseDouble(translation[1]) : transformations.get(0));
        }
        if (attributes.getIndex(DILATATION) != -1) {
            final String[] translation = attributes.getValue(DILATATION).split(";");
            if (translation.length > 2)
                throw new IllegalArgumentException("Dilatation has incorrect format : " + attributes.getValue(DILATATION));
            transformations.set(2, Double.parseDouble(translation[0]));
            transformations.set(3, translation.length == 2 ? Double.parseDouble(translation[1]) : transformations.get(0));
        }
        state.setTransformations(transformations);

        if (isInitial) {
            if (machineTemp.getInitialState() != null) {
                throw new IllegalArgumentException(String.format("Multiple states declared as initial in machine %s", machineTemp.getName()));
            }
            machineTemp.setInitialState(state);
        } else if (isFinal) {
            machineTemp.addFinalState(state);
        }

    }
}

