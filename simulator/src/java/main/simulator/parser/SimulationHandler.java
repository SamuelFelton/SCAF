package simulator.parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import simulator.model.productionsystem.Duration;
import simulator.model.productionsystem.DurationNumeric;
import simulator.model.productionsystem.Operation;
import simulator.model.productionsystem.ProductionSystem;
import simulator.model.simulation.SimulationBuilder;

import java.util.Arrays;
import java.util.List;

public class SimulationHandler extends SimulatorBaseHandler {

    private static final String SIMULATION = "simulation";
    private static final String LAUNCH = "launch";
    private static final String EVOLUTION = "evolution";
    private static final String ACTION = "action";
    private static final String MACHINE_NAME = "machine";
    private static final String COUNT = "count";
    private static final String STEP = "step";

    private final SimulationBuilder builder;
    private boolean done = false;
    private boolean foundSimulation = false;

    SimulationHandler(ProductionSystem productionSystem) {
        this.builder = new SimulationBuilder(productionSystem);
        this.system = productionSystem;

        authorizedAttributes.put(SIMULATION, Arrays.asList(COUNT, STEP));
        authorizedAttributes.put(LAUNCH, Arrays.asList(MACHINE_NAME, DATE));
        authorizedAttributes.put(EVOLUTION, Arrays.asList(NAME, EFFECTS, DURATION));
        authorizedAttributes.put(ACTION, Arrays.asList(NAME, EFFECTS, DATE));
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ((!foundSimulation || done) && !qName.toLowerCase().equals(SIMULATION)) {
            throw new IllegalArgumentException(String.format("%s is declared outside of the simulation", qName));
        }
        switch (qName.toLowerCase()) {
            case SIMULATION:
                simulationHandler(qName, attributes);
                break;
            case LAUNCH:
                launchHandler(qName, attributes);
                break;
            case EVOLUTION:
                evolutionHandler(qName, attributes);
                break;
            case ACTION:
                actionHandler(qName, attributes);
                break;
            default:
                throw new SAXException("Production system file contains invalid XML tag : " + qName);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName.toLowerCase()) {
            case SIMULATION:
                done = true;
        }
    }

    /**
     * Parses the desired step and count and configures the Simulation using the SimulationBuilder.
     * @param qName the qualified name of the action tag.
     * @param attributes the attributes of the tag.
     */
    private void simulationHandler(String qName, Attributes attributes) {
        checkTagArguments(qName, attributes, "racine");

        if (foundSimulation) {
            throw new IllegalArgumentException("Multiple simulations declared in a single file");
        } else {
            foundSimulation = true;
        }
        if (attributes.getIndex(COUNT) == -1) {
            throw new IllegalArgumentException("Missing simulation count in " + qName);
        }
        builder.setSimulationCount(Integer.parseInt(attributes.getValue("count")));
        if (attributes.getIndex(STEP) == -1) {
            builder.setStep(new DurationNumeric(1, 0));
        }
        Duration step = parseDuration(attributes.getValue("step"));
        if (!(step instanceof DurationNumeric))
            throw new IllegalArgumentException("Step must be defined as a constant");
        builder.setStep((DurationNumeric) step);
    }

    /**
     * Parses launch dates for a machine and configures the Simulation
     * using the SimulationBuilder and the desired dates.
     * @param qName the qualified name of the launch tag.
     * @param attributes the attributes of the tag.
     */
    private void launchHandler(String qName, Attributes attributes) {
        if (attributes.getIndex(MACHINE_NAME) == -1) {
            throw new IllegalArgumentException(String.format("%s is missing a machine name", qName));

        }

        final String machineName = attributes.getValue(MACHINE_NAME);

        checkTagArguments(qName, attributes, machineName);

        if (attributes.getIndex(DATE) == -1) {
            throw new IllegalArgumentException(String.format("Cannot parse launch dates for machine %s", machineName));
        }

        final Duration[] durations = parseDurations(attributes.getValue(DATE));

        final Duration durationMin = durations[0];
        final Duration durationMax = durations[1];

        if (!(durationMin instanceof DurationNumeric) || !(durationMax instanceof DurationNumeric))
            throw new IllegalArgumentException("Launch dates must be defined as constants");

        builder.addMachineLaunchDate(machineName, (DurationNumeric) durationMin, (DurationNumeric) durationMax);
    }

    /**
     * Parses an Action and configures the Simulation using the SimulationBuilder and the desired dates.
     * @param qName the qualified name of the action tag.
     * @param attributes the attributes of the tag.
     */
    private void actionHandler(String qName, Attributes attributes) {
        if (attributes.getIndex(NAME) == -1) {
            throw new IllegalArgumentException(String.format("%s is missing a name", qName));
        }

        final String name = attributes.getValue(NAME);

        checkTagArguments(qName, attributes, name);


        if (attributes.getIndex(DATE) == -1) {
            throw new IllegalArgumentException(String.format("Cannot parse date for action %s", name));
        }

        final Duration[] durations = parseDurations(attributes.getValue(DATE));

        final Duration durationMin = durations[0];
        final Duration durationMax = durations[1];

        if (attributes.getIndex(EFFECTS) == -1) {
            throw new IllegalArgumentException("Missing action effects for " + name);
        }

        final List<Operation> effects = parseOperations(attributes.getValue(EFFECTS));

        builder.addAction(name, effects, durationMin, durationMax);
    }

    /**
     * Parses an Evolution and configures the Simulation using the SimulationBuilder and the desired delays.
     * @param qName the qualified name of the evolution tag.
     * @param attributes the attributes of the tag.
     */
    private void evolutionHandler(String qName, Attributes attributes) {
        if (attributes.getIndex(NAME) == -1) {
            throw new IllegalArgumentException(String.format("%s is missing a name", qName));
        }
        final String name = attributes.getValue(NAME);

        checkTagArguments(qName, attributes, name);

        if (attributes.getIndex(DURATION) == -1) {
            throw new IllegalArgumentException(String.format("Cannot parse delay (%s) for evolution %s", DURATION, name));
        }

        final Duration[] durations = parseDurations(attributes.getValue(DURATION));

        final Duration durationMin = durations[0];
        final Duration durationMax = durations[1];

        if (attributes.getIndex(EFFECTS) == -1) {
            throw new IllegalArgumentException(String.format("Missing evolution effects for %s", name));
        }

        final List<Operation> effects = parseOperations(attributes.getValue(EFFECTS));

        builder.addEvolution(name, effects, durationMin, durationMax);
    }

    public SimulationBuilder getBuilder() {
        return builder;
    }
}
