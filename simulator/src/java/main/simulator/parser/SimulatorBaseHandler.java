package simulator.parser;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import simulator.model.productionsystem.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstract superclass of ProductionSystemHandler and SimulatorHandler.
 * Contains methods and constants relevant to both.
 */
public abstract class SimulatorBaseHandler extends DefaultHandler {

    static final String NAME = "name";
    static final String DURATION = "dur";
    static final String DATE = "date";
    static final String CONDITIONS = "cond";
    static final String EFFECTS = "eff";

    protected ProductionSystem system = null;

    static final Map<String, List<String>> authorizedAttributes = new HashMap<>();

    @Override
    public abstract void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException;

    @Override
    public abstract void endElement(String uri, String localName, String qName);

    /**
     * Checks that attributes contains no unexpected qNames.
     * Field authorizedAttributes must already have an entry for tag qName.
     * @param qName the tag currently being checked
     * @param attributes the attributes of the tag being checked
     * @param name name assigned to the tag (used to give context in case of an error)
     */
    void checkTagArguments(String qName, Attributes attributes, String name) {
        List<String> authorized = authorizedAttributes.get(qName.toLowerCase());
        for (int i = 0; i < attributes.getLength(); i++) {
            if(!authorized.contains(attributes.getQName(i).toLowerCase())) throw new IllegalArgumentException(String.format("Unknown attribute %s in tag %s (%s)", attributes.getQName(i), qName, name));
        }
    }

    /**
     * Parses a mXparser Expression from a String. Detects and configures variables used as mXparser Arguments.
     * @param expressionAsString the String representing the mXparser Expression.
     * @param generatorContext boolean indicating if 'x' and 'b' should be ignored
     * @return the parsed and configured Expression
     */
    Expression parseMX(String expressionAsString, boolean generatorContext) {

        List<String> constants = new ArrayList<>(Arrays.asList("pi", "e"));
        if (generatorContext) constants.addAll(Arrays.asList("b", "x"));

        expressionAsString = expressionAsString.replaceAll("\\s", "");
        Expression expression = new Expression(expressionAsString);

        final Pattern pattern = Pattern.compile("(\\w*[a-zA-Z]\\w*)([^\\w(]|$)");
        final Matcher matcher = pattern.matcher(expressionAsString);

        while (matcher.find()) {
            String variable = matcher.group(1);
            if (system.getVariable(variable) == null && !constants.contains(variable))
                throw new IllegalArgumentException(String.format("There is no variable named %s in %s", variable, system.getName()));

            if (expression.getArgument(variable) == null && !constants.contains(variable))
                expression.addArguments(new Argument(variable));
        }

        if (!expression.checkLexSyntax())
            throw new IllegalArgumentException("Expression " + expression.getExpressionString() + " is invalid (see generator documentation for more infomation on expression format)");

        return expression;
    }

    /**
     * Parses a list of OperationExpression objects from a String.
     * @param operations the String representing the operations list
     * @return the parsed list of OperationExpression objects.
     */
    List<Operation> parseOperations(String operations) {

        final List<Operation> operationList = new ArrayList<>();

        if (operations.charAt(operations.length() - 1) != ';') operations += ';';

        final Pattern pattern = Pattern.compile("([\\w]+)\\s*=([^;]*);");
        final Matcher matcher = pattern.matcher(operations);

        while (matcher.find()) {

            Variable var = system.getVariable(matcher.group(1));

            if (Objects.isNull(var)) {
                throw new IllegalArgumentException(String.format("There is no variable named %s in %s", matcher.group(1), system.getName()));
            }

            operationList.add(new OperationExpression(system, var, parseMX(matcher.group(2), false)));
        }

        return operationList;
    }

    /**
     * Parses a list of ConditionExpression objects from a String.
     * @param conditions the String representing the conditions list.
     * @return the parsed list of ConditionExpression objects.
     */
    List<Condition> parseConditions(String conditions) {

        final List<Condition> conditionList = new ArrayList<>();

        if (conditions.charAt(conditions.length() - 1) != ';') conditions += ';';

        final Pattern pattern = Pattern.compile("([\\w]+)\\s+([a-zA-Z]+)\\s+([^;]*);");
        final Matcher matcher = pattern.matcher(conditions);

        while (matcher.find()) {

            final Variable var = system.getVariable(matcher.group(1));
            ConditionExpression.Type type;


            if (Objects.isNull(var)) {
                throw new IllegalArgumentException(String.format("There is no variable named %s in %s", matcher.group(1), system.getName()));
            }

            switch (matcher.group(2).toLowerCase()) {
                case "gt":
                    type = ConditionExpression.Type.GREATER;
                    break;
                case "gte":
                    type = ConditionExpression.Type.GREATER_EQUAL;
                    break;
                case "lt":
                    type = ConditionExpression.Type.LESSER;
                    break;
                case "lte":
                    type = ConditionExpression.Type.LESSER_EQUAL;
                    break;
                case "eq":
                    type = ConditionExpression.Type.EQUAL;
                    break;
                case "neq":
                    type = ConditionExpression.Type.NOT_EQUAL;
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Unexpected operator %s in %s", matcher.group(2), matcher.group()));
            }

            conditionList.add(new ConditionExpression(system, var, type, parseMX(matcher.group(3), false)));
        }

        return conditionList;
    }

    /**
     * Parses up to two different Duration objects from a String.
     * @param durations the String representing the duration(s).
     * @return an array containing up to two Duration objects.
     */
    Duration[] parseDurations(String durations) {
        final String[] durationArray = durations.split(";");
        if (durationArray.length > 2) {
            throw new IllegalArgumentException(String.format("More than two durations found in %s", durations));
        }
        final Duration duration = parseDuration(durationArray[0]);
        return new Duration[]{duration, durationArray.length == 1 ? duration : parseDuration(durationArray[1])};
    }

    /**
     * Parses a DurationNumeric or a DurationExpression from a String.
     * @param duration the String representing the duration.
     * @return the parsed Duration object.
     */
    Duration parseDuration(String duration) {

        final Pattern pattern = Pattern.compile("((\\d+)d)?((\\d+)h)?((\\d+)m)?((\\d+)s)?");
        final Matcher matcher = pattern.matcher(duration);

        if (matcher.find() && matcher.group().length() > 0) {
            int days = 0, hours = 0, minutes = 0, seconds = 0;
            if (matcher.group(2) != null) days = Integer.parseInt(matcher.group(2));
            if (matcher.group(4) != null) hours = Integer.parseInt(matcher.group(4));
            if (matcher.group(6) != null) minutes = Integer.parseInt(matcher.group(6));
            if (matcher.group(8) != null) seconds = Integer.parseInt(matcher.group(8));
            return new DurationNumeric(days, hours, minutes, seconds);
        }
        return new DurationExpression(system, parseMX(duration, false));

    }

    public ProductionSystem getSystem() {
        return system;
    }

}
