package simulator.parser;

import org.xml.sax.SAXException;
import simulator.model.productionsystem.ProductionSystem;
import simulator.model.productionsystem.ProductionSystemSetupException;
import simulator.model.simulation.Simulation;
import simulator.model.simulation.SimulationSetupException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class SimulatorParser {

    /**
     * Create a ProductionSystem object from the xml file
     *
     * @return A ProductionSystem configured accordingly with the xml file.
     */
    public static ProductionSystem generateProductionSystem(String filepath) throws ProductionSystemSetupException {
        final File file = new File(filepath);
        final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        final ProductionSystemHandler psHandler = new ProductionSystemHandler();
        final SAXParser saxParser;
        try {
            saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(file, psHandler);
        } catch (SAXException e) {
            throw new ProductionSystemSetupException(String.format("A XML error occured while parsing the production system file : %s", e.getMessage()));
        } catch (ParserConfigurationException e) {
            throw new ProductionSystemSetupException(String.format("An error occured while preparing the production system parser : %s", e.getMessage()));
        } catch (IOException e) {
            throw new ProductionSystemSetupException(String.format("An IO error occured while accessing the production system file : %s", e.getMessage()));
        } catch (IllegalArgumentException e) {
            throw new ProductionSystemSetupException(String.format("An error was detected in the production system file : %s", e.getMessage()));
        }
        return psHandler.getSystem();

    }

    public static Simulation generateSimulation(String filepath, ProductionSystem ps) throws SimulationSetupException {
        final File file = new File(filepath);
        final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        final SimulationHandler sHandler = new SimulationHandler(ps);
        try {
            final SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(file, sHandler);
        } catch (SAXException e) {
            throw new SimulationSetupException(String.format("A XML error occured while parsing the simulation file : %s", e.getMessage()));
        } catch (ParserConfigurationException e) {
            throw new SimulationSetupException(String.format("An error occured while preparing the simulation parser : %s", e.getMessage()));
        } catch (IOException e) {
            throw new SimulationSetupException(String.format("An IO error occured while accessing the simulation system file : %s", e.getMessage()));
        }  catch (IllegalArgumentException e) {
            throw new SimulationSetupException(String.format("An error was detected in the simulation file : %s", e.getMessage()));
        }
        return sHandler.getBuilder().createSimulation();
    }


}