package simulator;

import org.junit.Test;
import simulator.model.productionsystem.ProductionSystemSetupException;
import simulator.model.simulation.SimulationSetupException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class MainTest {

    private void print(String content, String filepath) {
        try (PrintStream out = new PrintStream(new FileOutputStream(filepath))) {
            out.print(content);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void main() {
        String content_system = "<PRODSYS name=\"Cuisine\">\n" +
                "\t<VARIABLE name=\"poire\" init=\"10\"/>\n" +
                "\t<VARIABLE name=\"pomme\" init=\"10\"/>\n" +
                "\t<MACHINE name=\"Dany\">\n" +
                "\t\t<STATE name=\"mange_pomme\" initial=\"true\" dur=\"poire;pomme\"/>\n" +
                "\t\t<STATE name=\"mange_poire\" cons=\"b+3*x+pi\" dila=\"0.5;0.3\" tran=\"0.7;0.9\" dur=\"4m;6m\"/>\n" +
                "\t\t<STATE name=\"plus_faim\" final=\"true\" dur=\"0s\"/>\n" +
                "\t\t<TRANSITION name=\"miam_poire\" from=\"mange_pomme\" to=\"mange_poire\" eff=\"poire = poire - 1;\"/>\n" +
                "\t\t<TRANSITION name=\"miam_pomme\" from=\"mange_poire\" to=\"mange_pomme\" cond=\"poire gt 5;\" eff=\"pomme = pomme - 1;\"/>\n" +
                "\t\t<TRANSITION name=\"plus_faim\" from=\"mange_poire\" to=\"plus_faim\" cond=\"poire eq 5;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>\n";
        String content_simulation = "<SIMULATION count=\"10\" step=\"1m\">\n" +
                "\t<LAUNCH machine=\"Dany\" date=\"7h;8h\"/>\n" +
                "\t<EVOLUTION name=\"pomme_pourrie\" eff=\"pomme = pomme - 1;\" dur=\"1h;1h\"/>\n" +
                "\t<ACTION name=\"achat\" eff=\"poire = poire + 1;\" date=\"8h;9h\" />\n" +
                "</SIMULATION>";

        print(content_system, "test_system.xml");
        print(content_simulation, "test_simulation.xml");


        Main.main(new String[] {"-ps", "test_system.xml", "-s", "test_simulation.xml", "-o", "logs", "-g"});
        // I'm so sorry...
        File file = new File("logs/s_Cuisine_10.log");
        assertTrue(System.currentTimeMillis() - file.lastModified() < 1000);
        file = new File("logs/m_Dany_10.log");
        assertTrue(System.currentTimeMillis() - file.lastModified() < 1000);
        file = new File("logs/generator/Dany_10.xml");
        assertTrue(System.currentTimeMillis() - file.lastModified() < 1000);

        try {
            Files.delete(Paths.get("test_system.xml"));
            Files.delete(Paths.get("test_simulation.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}