package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Expression;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ActionTest {

    private ProductionSystem system;
    private Variable v1;
    private Variable v2;

    @Before
    public void setUp()  {
        system = new ProductionSystem("Test system please ignore");
        v1 = system.createVariable("Var1", 0.);
        v2 = system.createVariable("Var2", 0.);
    }

    @Test
    public void execute() {
        final List<Operation> effects = Arrays.asList(new OperationExpression(system, v1, new Expression("1")), new OperationExpression(system, v1, new Expression("2")), new OperationExpression(system, v2, new Expression("2")));
        final Action action = new Action("Foo", effects, Action.Type.ACTION);
        action.execute();
        assertEquals(2., v1.getValue(), 0.);
        assertEquals(2., v2.getValue(), 0.);
    }
}