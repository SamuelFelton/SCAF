package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConditionExpressionTest {

    private ConditionExpression conditionExpression;
    private ProductionSystem system;
    private Variable v1;
    private Variable v2;

    @Before
    public void setUp() {
        system = new ProductionSystem("Test system please ignore");
        v1 = system.createVariable("v1", 2.);
        v2 = system.createVariable("v2", 3.);
    }

    @Test
    public void executeEqual() {
        system.createVariable("v3", 10.);

        final Expression expression = new Expression("v2+v3*2+max(7,5)");
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.EQUAL, expression);

        assertFalse(conditionExpression.evaluate());
        v1.setValue(30.);
        assertTrue(conditionExpression.evaluate());
        v1.setValue(35.);
        assertFalse(conditionExpression.evaluate());
    }

    @Test
    public void executeGreater() {
        system.createVariable("v3", 10.);

        final Expression expression = new Expression("v2+v3*2+max(7,5)");
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.GREATER, expression);

        assertFalse(conditionExpression.evaluate());
        v1.setValue(30.);
        assertFalse(conditionExpression.evaluate());
        v1.setValue(35.);
        assertTrue(conditionExpression.evaluate());
    }

    @Test
    public void executeGreaterEqual() {
        system.createVariable("v3", 10.);

        final Expression expression = new Expression("v2+v3*2+max(7,5)");
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.GREATER_EQUAL, expression);

        assertFalse(conditionExpression.evaluate());
        v1.setValue(30.);
        assertTrue(conditionExpression.evaluate());
        v1.setValue(35.);
        assertTrue(conditionExpression.evaluate());
    }

    @Test
    public void executeLesser() {
        system.createVariable("v3", 10.);

        final Expression expression = new Expression("v2+v3*2+max(7,5)");
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.LESSER, expression);

        assertTrue(conditionExpression.evaluate());
        v1.setValue(30.);
        assertFalse(conditionExpression.evaluate());
        v1.setValue(35.);
        assertFalse(conditionExpression.evaluate());
    }

    @Test
    public void executeLesserEqual() {
        system.createVariable("v3", 10.);

        final Expression expression = new Expression("v2+v3*2+max(7,5)");
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.LESSER_EQUAL, expression);

        assertTrue(conditionExpression.evaluate());
        v1.setValue(30.);
        assertTrue(conditionExpression.evaluate());
        v1.setValue(35.);
        assertFalse(conditionExpression.evaluate());
    }

    @Test
    public void executeNotEqual() {
        system.createVariable("v3", 10.);

        final Expression expression = new Expression("v2+v3*2+max(7,5)");
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.NOT_EQUAL, expression);

        assertTrue(conditionExpression.evaluate());
        v1.setValue(30.);
        assertFalse(conditionExpression.evaluate());
        v1.setValue(35.);
        assertTrue(conditionExpression.evaluate());
    }

    @Test
    public void executeConstant() {
        final Expression expression = new Expression("v2+pi-pi");
        expression.addArguments(new Argument("v2"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.EQUAL, expression);

        assertFalse(conditionExpression.evaluate());
        v1.setValue(3.);
        assertTrue(conditionExpression.evaluate());
    }

    @Test(expected = IllegalArgumentException.class)
    public void executeUnknownVariable() {
        final Expression expression = new Expression("v4");
        expression.addArguments(new Argument("v4"));

        conditionExpression = new ConditionExpression(system, v1, ConditionExpression.Type.EQUAL, expression);
        conditionExpression.evaluate();
    }
}