package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;

import static org.junit.Assert.assertEquals;

public class DurationExpressionTest {

    private DurationExpression durationExpression;
    private ProductionSystem system;
    private Variable v1;
    private Variable v2;

    @Before
    public void setUp() {
        system = new ProductionSystem("Test system please ignore");
        v1 = system.createVariable("v1", 15.);
        v2 = system.createVariable("v2", 20.);
    }

    @Test
    public void toSeconds() {
        final Expression expression = new Expression("v1+v2*2+max(60,50)");
        expression.addArguments(new Argument("v1"));
        expression.addArguments(new Argument("v2"));

        durationExpression = new DurationExpression(system, expression);

        assertEquals(115, durationExpression.toSeconds());
        v2.setValue(5.);
        assertEquals(85, durationExpression.toSeconds());
    }

    @Test
    public void toDate() {
        final Variable v3 = system.createVariable("v3", 1.);

        final Expression expression = new Expression("v1+v2*2+max(60,50)+90000*v3");
        expression.addArguments(new Argument("v1"));
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        durationExpression = new DurationExpression(system, expression);

        assertEquals("1:01'01\"55", durationExpression.toDate());
        v2.setValue(5.);
        v3.setValue(0.);
        assertEquals("00'01\"25", durationExpression.toDate());
    }
}