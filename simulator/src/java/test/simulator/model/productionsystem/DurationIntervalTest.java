package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DurationIntervalTest {

    private Duration d1;
    private Duration d2;
    private DurationInterval d;

    @Before
    public void setUp() {
        d1 = new DurationNumeric(300);
        d2 = new DurationNumeric(600);
        d = new DurationInterval(d1,d2);
    }

    @Test
    public void computeDurationTest() {
        final DurationNumeric d3 = d.computeDuration();
        assertTrue(d3.compareTo(d1) >= 0 && d2.compareTo(d3) >= 0);
    }

    @Test
    public void containsTest(){
        assertFalse(d.contains(new DurationNumeric(750)));
        assertTrue(d.contains(new DurationNumeric(450)));
        assertFalse(d.contains(new DurationNumeric(150)));
    }

}