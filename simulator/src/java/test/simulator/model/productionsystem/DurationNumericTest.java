package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DurationNumericTest {

    private DurationNumeric d1;
    private DurationNumeric d2;

    @Before
    public void setUp() {
        d1 = new DurationNumeric(1, 2, 3, 4);
    }

    @Test
    public void getters() {
        d2 = new DurationNumeric(1,25, 61,61);
        assertEquals(2 , d2.getDays());
        assertEquals(2 , d2.getHours());
        assertEquals(2 , d2.getMinutes());
        assertEquals(1 , d2.getSeconds());
    }

    @Test
    public void toSeconds() {
        assertEquals(86400 + 2 * 3600 + 3 * 60 + 4, d1.toSeconds());
    }

    @Test
    public void compareTo() {
        d2 = new DurationNumeric(1, 2, 2, 5);
        assertTrue(d1.compareTo(d2) > 0);
        d2 = new DurationNumeric(1, 1, 64, 0);
        assertTrue(d1.compareTo(d2) < 0);
    }

    @Test(expected = ClassCastException.class)
    public void compareToNotInstanceOfDuration() {
        d1 = new DurationNumeric(0);
        DurationInterval d2 = new DurationInterval(0,1);
        d1.compareTo(d2);
    }

    @Test
    public void plus() {
        d2 = new DurationNumeric(4, 3, 2, 1);
        DurationNumeric d3 = d1.plus(d2);
        assertEquals(new DurationNumeric(5, 5, 5, 5), d3);
    }

    @Test
    public void equals() {
        d1 = new DurationNumeric(3660);
        d2 = new DurationNumeric(1, 1, 0);
        assertEquals(d1, d2);
    }

    @Test
    public void toDate() {
        d1 = new DurationNumeric(1,2,3);
        assertEquals("01'02\"03", d1.toDate());
        d1 = new DurationNumeric(1,11,12,13);
        assertEquals("1:11'12\"13", d1.toDate());
        d1 = new DurationNumeric(1,2);
        assertEquals("00'01\"02", d1.toDate());
    }
}