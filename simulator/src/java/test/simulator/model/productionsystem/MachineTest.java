package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.*;

public class MachineTest {

    private Machine m1;
    private State s1;
    private State s2;
    private Transition t1;
    private Transition t2;
    private Variable v1;
    private Variable v2;

    @Before
    public void setUp() {
        m1 = new Machine("M1");
        s1 = m1.createState("S1", new DurationNumeric(30), new DurationNumeric(30));
        s2 = m1.createState("S2", new DurationNumeric(30), new DurationNumeric(30));
        t1 = m1.createTransition("T1",s1,s2);
        t2 = m1.createTransition("T2",s2,s1);
        v1 = new Variable("V1", 0.0);
        v2 = new Variable("V2", 0.0);
    }

    @Test
    public void addState() {
        assertEquals(s1, m1.getState("S1"));
    }

    @Test
    public void addTransition() {
        assertEquals(t1, m1.getTransition("T1"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void addTransitionUnknownStates() {
        m1.createTransition("T3", new State("S3", new DurationNumeric(30), new DurationNumeric(30)), s2);
    }

    @Test
    public void launch() throws ProductionSystemSetupException {
        m1.setInitialState(s1);
        m1.launch();
        assertEquals(new DurationNumeric(30), m1.computeCurrentStateDuration());
    }

    @Test(expected=ProductionSystemSetupException.class)
    public void launchNoInitialState() throws ProductionSystemSetupException {
        m1.launch();
    }

    @Test
    public void transition() throws ProductionSystemSetupException {
        m1.setInitialState(s1);
        m1.launch();
        m1.transition();
        assertEquals(s2, m1.getCurrentState());
    }

    @Test(expected = ProductionSystemSetupException.class)
    public void transitionNotCurrentState() throws ProductionSystemSetupException {
        m1.setInitialState(s1);
        m1.transition();
    }

    @Test
    public void isStopped() throws ProductionSystemSetupException {
        m1.setInitialState(s1);
        m1.addFinalState(s2);
        assertTrue(m1.isStopped());
        m1.launch();
        assertFalse(m1.isStopped());
        m1.transition();
        assertTrue(m1.isStopped());
    }

    @Test
    public void createState() {
        assertEquals("S1",s1.getName());
        assertTrue(s2.computeDuration().compareTo(new DurationNumeric(29))>0);
        assertEquals(s1, m1.getState("S1"));
        assertEquals(2, m1.getStates().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createStateDuplicate() {
        m1.createState("S",new DurationNumeric(0), new DurationNumeric(30));
        m1.createState("S",new DurationNumeric(0), new DurationNumeric(30));
    }

    @Test
    public void createTransition() {
        assertEquals("T1",t1.getName());
        assertEquals(s1, t1.from());
        assertEquals(s2, t1.to());
        assertEquals(t1, m1.getTransition("T1"));
        assertEquals(2, m1.getTransitions().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createTransitionDuplicate() {
        final State s1 = m1.createState("S1", new DurationNumeric(0), new DurationNumeric(0));
        final State s2 = m1.createState("S2", new DurationNumeric(0), new DurationNumeric(0));
        m1.createTransition("T", s1,s2);
        m1.createTransition("T", s2,s1);
    }
}