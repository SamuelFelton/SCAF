package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;

import static org.junit.Assert.assertEquals;

public class OperationExpressionTest {

    private OperationExpression operationExpression;
    private ProductionSystem system;
    private Variable v1;
    private Variable v2;

    @Before
    public void setUp() {
        system = new ProductionSystem("Test system please ignore");
        v1 = system.createVariable("v1",2.);
        v2 = system.createVariable("v2", 3.);
    }

    @Test
    public void execute() {
        system.createVariable("v3", 10.);

        final Expression expression = new Expression("v2+v3*2+max(7,5)+v2+v3");
        expression.addArguments(new Argument("v2"));
        expression.addArguments(new Argument("v3"));

        operationExpression = new OperationExpression(system, v1, expression);

        operationExpression.execute();
        assertEquals(43., v1.getValue(), 0.);
        v2.setValue(5.);
        operationExpression.execute();
        assertEquals(47., v1.getValue(), 0.);
    }

    @Test
    public void executeConstant() {
        final Expression expression = new Expression("v2*2+pi-pi");
        expression.addArguments(new Argument("v2"));

        operationExpression = new OperationExpression(system, v1, expression);

        operationExpression.execute();
        assertEquals(6., v1.getValue(), 0.001); // stupid cutie pi
    }

    @Test(expected = IllegalArgumentException.class)
    public void executeUnknownVariable() {
        final Expression expression = new Expression("v4");
        expression.addArguments(new Argument("v4"));

        operationExpression = new OperationExpression(system, v1, expression);
        operationExpression.execute();
    }
}