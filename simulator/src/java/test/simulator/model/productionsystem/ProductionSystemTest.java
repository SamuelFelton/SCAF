package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProductionSystemTest {

    private ProductionSystem ps;

    @Before
    public void setUp() {
        ps = new ProductionSystem("Production System Test");
    }

    @Test
    public void createMachine() {
        final Machine m1 = ps.createMachine("M1");
        ps.createMachine("M2");
        assertEquals("M1", m1.getName());
        assertEquals(m1, ps.getMachine("M1"));
        assertEquals(2, ps.getMachines().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createDuplicateMachine() {
        ps.createMachine("M");
        ps.createMachine("M");
    }

    @Test
    public void createVariable() {
        final Variable v1 = ps.createVariable("V1", 0);
        assertEquals(v1, ps.getVariable("V1"));
        assertEquals(1, ps.getVariables().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createDuplicateVariable() {
        ps.createVariable("V1", 0);
        ps.createVariable("V1", 1);
    }

}