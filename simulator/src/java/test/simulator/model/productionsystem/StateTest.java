package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;

import static org.junit.Assert.assertTrue;

public class StateTest {

    private ProductionSystem system;
    private State s1;

    @Before
    public void setUp() {
        system = new ProductionSystem("Test production system please ignore");
        s1 = new State("State Test", new DurationNumeric(5,0), new DurationNumeric(10, 0));
    }

    @Test
    public void computeDurationNumeric() {
        s1 = new State("State Test", new DurationNumeric(5,0), new DurationNumeric(10, 0));
        final DurationNumeric d1 = s1.computeDuration();
        assertTrue(d1.toSeconds() >= 300 && d1.toSeconds() <= 600);
    }

    @Test
    public void computeDurationVariable() {
        final Variable v1 = system.createVariable("Var1", 300);
        final Variable v2 = system.createVariable("Var2", 600);
        final Expression expressionMin = new Expression("Var1");
        expressionMin.addArguments(new Argument("Var1"));
        final Expression expressionMax = new Expression("Var2");
        expressionMax.addArguments(new Argument("Var2"));
        s1 = new State("State Test", new DurationExpression(system, expressionMin), new DurationExpression(system, expressionMax));
        DurationNumeric d1 = s1.computeDuration();
        assertTrue(d1.toSeconds() >= 300 && d1.toSeconds() <= 600);

        v1.setValue(900.);
        v2.setValue(600.);
        d1 = s1.computeDuration();
        assertTrue(d1.toSeconds() >= 600 && d1.toSeconds() <= 900);
    }


}