package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Expression;

import static org.junit.Assert.*;

public class TransitionTest {

    private ProductionSystem system;
    private Transition t;
    private Condition c;
    private Variable v;
    private Operation o;


    @Before
    public void setUp() {
        system = new ProductionSystem("Test production system please ignore");
        final State s1 = new State("State Test 1", new DurationNumeric(0), new DurationNumeric(1));
        final State s2 = new State("State Test 2", new DurationNumeric(0), new DurationNumeric(1));
        t = new Transition("Transition Test", s1, s2);
        v = system.createVariable("Variable Double Test", 0.0);
        c = new ConditionExpression(system, v, ConditionExpression.Type.EQUAL, new Expression("0"));
        o = new OperationExpression(system, v, new Expression("1"));
    }

    @Test
    public void conditionIsValidated() {
        t.addCondition(c);
        assertTrue(t.evaluateConditions());
        v.setValue(1.);
        assertFalse(t.evaluateConditions());
    }

    @Test
    public void executeEffects() {
        t.addEffect(o);
        t.executeEffects();
        assertEquals(1, v.getValue(), 0.);
    }
}