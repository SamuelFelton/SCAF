package simulator.model.productionsystem;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VariableTest {

    private Variable v;

    @Before
    public void setUp() {
        v = new Variable("Variable Double Test", 0.0);
    }

    @Test
    public void initialize() {
        assertEquals(0., v.getValue(), 0.);
        v.setValue(1.0);
        assertEquals(1., v.getValue(), 0.);
        v.initialize();
        assertEquals(0., v.getValue(), 0.);
    }
}