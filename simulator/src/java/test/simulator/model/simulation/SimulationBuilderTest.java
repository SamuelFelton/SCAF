package simulator.model.simulation;

import org.junit.Before;
import org.junit.Test;
import simulator.model.productionsystem.DurationInterval;
import simulator.model.productionsystem.DurationNumeric;
import simulator.model.productionsystem.ProductionSystem;

import static org.junit.Assert.assertEquals;

public class SimulationBuilderTest {

    private SimulationBuilder simulationBuilder;
    private ProductionSystem ps;
    private Simulation s;

    @Before
    public void setUp() {
        ps = new ProductionSystem("Simulation Builder Production System Test");
    }

    @Test
    public void createSimulation() throws SimulationSetupException {
        ps.createMachine("M1");
        simulationBuilder = new SimulationBuilder(ps);
        simulationBuilder.addMachineLaunchDate("M1", new DurationNumeric(0), new DurationNumeric(10));
        s = simulationBuilder.createSimulation();
        assertEquals(new DurationInterval(new DurationNumeric(0), new DurationNumeric(10)), s.getMachinesLaunchDates().get(ps.getMachine("M1")));
    }

    @Test(expected = SimulationSetupException.class)
    public void createSimulationException() throws SimulationSetupException {
        ps.createMachine("Simulation Builder Machine Test 2");
        simulationBuilder = new SimulationBuilder(ps);
        simulationBuilder.createSimulation();
    }
}