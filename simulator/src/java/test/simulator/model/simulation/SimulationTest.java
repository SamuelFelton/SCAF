package simulator.model.simulation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import simulator.model.productionsystem.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SimulationTest {

    @BeforeClass
    public static void setUpOnce() {
        File dir = new File("logs");
        if (!dir.mkdir() && !(dir.exists())) throw new RuntimeException("Could not create log directory for the tests");
    }

    @Test
    public void simulate() throws SimulationSetupException, ProductionSystemSetupException, IOException {

        //production system setup

        final ProductionSystem ps = new ProductionSystem("System_decoupe");

        final Machine m1 = ps.createMachine("Machine_decoupe_1");
        final Machine m2 = ps.createMachine("Machine_decoupe_2");

        // Variables

        final Variable qfarine1 = ps.createVariable("qfarine1", 250.);
        final Variable qfarine2 = ps.createVariable("qfarine2", 250.);
        final Variable qpate1 = ps.createVariable("qpate1", 0.);
        final Variable qpate2 = ps.createVariable("qpate2", 0.);
        final Variable clock = ps.createVariable("clock", 0.);

        // States

        final State nettoyer1 = m1.createState("s_nettoyer1", new DurationNumeric(5, 0), new DurationNumeric(10, 0));
        final State melanger1 = m1.createState("s_melanger1", new DurationNumeric(20, 0), new DurationNumeric(30, 0));
        melanger1.setConsumption(new Expression("10"));
        final State decouper1 = m1.createState("s_decouper1", new DurationNumeric(15, 0), new DurationNumeric(20, 0));
        decouper1.setConsumption(new Expression("20"));
        final State arret1 = m1.createState("s_arret1", new DurationNumeric(0), new DurationNumeric(0));

        final State nettoyer2 = m2.createState("s_nettoyer2", new DurationNumeric(5, 0), new DurationNumeric(10, 0));
        final State melanger2 = m2.createState("s_melanger2", new DurationNumeric(20, 0), new DurationNumeric(30, 0));
        melanger1.setConsumption(new Expression("10"));
        final State decouper2 = m2.createState("s_decouper2", new DurationNumeric(15, 0), new DurationNumeric(20, 0));
        decouper1.setConsumption(new Expression("20"));
        final State arret2 = m2.createState("s_arret2", new DurationNumeric(0), new DurationNumeric(0));


        // Transitions

        final Transition debut_melange_m1 = m1.createTransition("debut_melange_m1", nettoyer1, melanger1);
        final Transition debut_melange_m2 = m2.createTransition("debut_melange_m2", nettoyer2, melanger2);

        final Transition debut_decoupe_m1 = m1.createTransition("debut_decoupe_m1", melanger1, decouper1);
        debut_decoupe_m1.addEffect(new OperationExpression(ps, qpate1, new Expression("15")));
        debut_decoupe_m1.addEffect(new OperationExpression(ps, qfarine1, new Expression("0")));
        final Transition debut_decoupe_m2 = m2.createTransition("debut_decoupe_m2", melanger2, decouper2);
        debut_decoupe_m2.addEffect(new OperationExpression(ps, qpate2, new Expression("10")));
        debut_decoupe_m2.addEffect(new OperationExpression(ps, qfarine2, new Expression("0")));

        final Transition decoupe_continue_m1 = m1.createTransition("decoupe_continue_m1", decouper1, decouper1);
        decoupe_continue_m1.addCondition(new ConditionExpression(ps, qpate1, ConditionExpression.Type.GREATER, new Expression("0")));
        final Expression expression_1 = new Expression("qpate1 - 1");
        expression_1.addArguments(new Argument("qpate1"));
        decoupe_continue_m1.addEffect(new OperationExpression(ps, qpate1, expression_1));

        final Transition decoupe_continue_m2 = m2.createTransition("decoupe_continue_m2", decouper2, decouper2);
        decoupe_continue_m2.addCondition(new ConditionExpression(ps, qpate2, ConditionExpression.Type.GREATER, new Expression("0")));
        final Expression expression_2 = new Expression("qpate2 - 1");
        expression_2.addArguments(new Argument("qpate2"));
        decoupe_continue_m2.addEffect(new OperationExpression(ps, qpate2, expression_2));

        final Transition arret_m1 = m1.createTransition("arret_m1", decouper1, arret1);
        arret_m1.addCondition(new ConditionExpression(ps, qpate1, ConditionExpression.Type.LESSER_EQUAL, new Expression("0")));

        final Transition arret_m2 = m2.createTransition("arret_m2", decouper2, arret2);
        arret_m2.addCondition(new ConditionExpression(ps, qpate2, ConditionExpression.Type.LESSER_EQUAL, new Expression("0")));

        // Machines

        m1.setInitialState(nettoyer1);
        m1.addFinalState(arret1);

        m2.setInitialState(nettoyer2);
        m2.addFinalState(arret2);

        // Simulation building

        final SimulationBuilder sb = new SimulationBuilder(ps).setSimulationCount(1);
        sb.addMachineLaunchDate("Machine_decoupe_1", new DurationNumeric(1, 0, 0), new DurationNumeric(2, 10, 0));
        sb.addMachineLaunchDate("Machine_decoupe_2", new DurationNumeric(2, 0, 0), new DurationNumeric(3, 10, 0));
        final List<Operation> clockEffects = new ArrayList<>();
        final Expression expression_3 = new Expression("clock + 1");
        expression_3.addArguments(new Argument("clock"));
        clockEffects.add(new OperationExpression(ps, clock, expression_3));
        sb.addEvolution("Clock", clockEffects, new DurationNumeric(1, 0, 0), new DurationNumeric(1, 0, 0));

        final List<Operation> pateReloadEffects = new ArrayList<>();
        final Expression expression_4 = new Expression("qpate2 + 2");
        expression_4.addArguments(new Argument("qpate2"));
        pateReloadEffects.add(new OperationExpression(ps, qpate2, expression_4));
        sb.addAction("Pate reload", pateReloadEffects, new DurationNumeric(3, 0, 0), new DurationNumeric(4, 0, 0));
        final Simulation s = sb.createSimulation();

        // Simulation testing

        s.simulate("logs", true, true);

        assertEquals(0.0, qpate1.getValue(), 0.);
        assertEquals(0.0, qpate2.getValue(), 0.);
    }
}