package simulator.parser;

import org.junit.After;
import org.junit.Test;
import simulator.model.productionsystem.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static simulator.parser.SimulatorParser.generateProductionSystem;


public class ProductionSystemHandlerTest {

    private final String filepath = "ProductionSystemHandler.xml";

    @After
    public void tearDown() {
        try {
            Files.delete(Paths.get(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void print(String content) {
        try (PrintStream out = new PrintStream(new FileOutputStream(filepath))) {
            out.print(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_Machine() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        assertNotNull(sys.getMachine("Machine"));
    }

    @Test
    public void check_Variable() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);
        final Machine machine = sys.getMachine("Machine");

        assertTrue(sys.getVariables().size() == 1);
        assertEquals(1., sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_State() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<STATE name=\"State1\" dur=\"1d2h3m4s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertEquals(93784, machine.getState("State1").computeDuration().toSeconds());
    }

    @Test
    public void check_State_initial() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertEquals(machine.getState("State1"), machine.getInitialState());
    }

    @Test
    public void check_State_final() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<STATE name=\"State1\" final=\"true\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getFinalStates().contains(machine.getState("State1")));
    }

    @Test
    public void check_State_comsumption() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\" cons=\"0.125*x\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertEquals("0.125*x", machine.getState("State1").getConsumption().getExpressionString());
    }

    @Test
    public void check_State_translation() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\" tran=\"0.5\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final List<Double> transformations = machine.getState("State1").getTransformations();
        assertEquals(0.5, transformations.get(0), 0.);
        assertEquals(0.5, transformations.get(1), 0.);
    }

    @Test
    public void check_State_dilatation() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\" dila=\"0.25;0.75\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final List<Double> transformations = machine.getState("State1").getTransformations();
        assertEquals(0.25, transformations.get(2), 0.);
        assertEquals(0.75, transformations.get(3), 0.);
    }

    @Test
    public void check_State_duration_variable() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"60\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"Var1*5+60\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertEquals(360, machine.getState("State1").computeDuration().toSeconds());

        final Variable var = sys.getVariable("Var1");
        var.setValue(120.);
        assertEquals(660, machine.getState("State1").computeDuration().toSeconds());
    }

    @Test
    public void check_Transitions() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition trans1 = machine.getTransition("Trans1");
        assertEquals(machine.getState("State1"), trans1.from());
        assertEquals(machine.getState("State2"), trans1.to());
    }

    @Test
    public void check_Trans_effect_add() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = Var1 + 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition t = machine.getTransition("Trans1");
        final Collection<Operation> operations = t.getEffects();
        assertTrue(operations.size() == 1);
        final Operation o = (Operation) operations.toArray()[0];
        o.execute();
        assertEquals(1., sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_Trans_effect_sub() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = Var1 - 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition t = machine.getTransition("Trans1");
        final Collection<Operation> operations = t.getEffects();
        assertTrue(operations.size() == 1);
        final Operation o = (Operation) operations.toArray()[0];
        o.execute();
        assertEquals(-1., sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_Trans_effect_mul() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = Var1 * 2.5;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition t = machine.getTransition("Trans1");
        final Collection<Operation> operations = t.getEffects();
        assertTrue(operations.size() == 1);
        final Operation o = (Operation) operations.toArray()[0];
        o.execute();
        assertEquals(2.5, sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_Trans_effect_div() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"2\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = Var1 / 2;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition t = machine.getTransition("Trans1");
        final Collection<Operation> operations = t.getEffects();
        assertTrue(operations.size() == 1);
        final Operation o = (Operation) operations.toArray()[0];
        o.execute();
        assertEquals(1., sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_Trans_effect_mul_add() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = Var1 * 2 + 2;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition t = machine.getTransition("Trans1");
        final Collection<Operation> operations = t.getEffects();
        assertTrue(operations.size() == 1);
        final Operation o = (Operation) operations.toArray()[0];
        o.execute();
        assertEquals(4., sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_Trans_effect_equal_num() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition t = machine.getTransition("Trans1");
        final Collection<Operation> operations = t.getEffects();
        assertTrue(operations.size() == 1);
        final Operation o = (Operation) operations.toArray()[0];
        o.execute();
        assertEquals(1., sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_Trans_effect_equal_two_num() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<VARIABLE name=\"Var2\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = Var2;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        assertTrue(machine.getTransitions().size() == 1);
        final Transition t = machine.getTransition("Trans1");
        final Collection<Operation> operations = t.getEffects();
        assertTrue(operations.size() == 1);
        final Operation o = (Operation) operations.toArray()[0];
        o.execute();
        assertEquals(1., sys.getVariable("Var1").getValue(), 0.);
    }

    @Test
    public void check_Trans_condition_eq_num() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 eq 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertFalse(c.evaluate());
        sys.getVariable("Var1").setValue(1.);
        assertTrue(c.evaluate());

    }

    @Test
    public void check_Trans_condition_neq() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 neq 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertTrue(c.evaluate());
        sys.getVariable("Var1").setValue(1.);
        assertFalse(c.evaluate());

    }

    @Test
    public void check_Trans_condition_eq_two_num() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<VARIABLE name=\"Var2\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 eq Var2;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";


        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertFalse(c.evaluate());
        sys.getVariable("Var1").setValue(1.);
        assertTrue(c.evaluate());
    }

    @Test
    public void check_Trans_condition_gt() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 gt 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertFalse(c.evaluate());
        sys.getVariable("Var1").setValue(1.);
        assertFalse(c.evaluate());
        sys.getVariable("Var1").setValue(2.);
        assertTrue(c.evaluate());
    }

    @Test
    public void check_Trans_condition_gte() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 gte 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertFalse(c.evaluate());
        sys.getVariable("Var1").setValue(1.);
        assertTrue(c.evaluate());
        sys.getVariable("Var1").setValue(2.);
        assertTrue(c.evaluate());
    }

    @Test
    public void check_Trans_condition_lt() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 lt 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertTrue(c.evaluate());
        sys.getVariable("Var1").setValue(1.);
        assertFalse(c.evaluate());
        sys.getVariable("Var1").setValue(2.);
        assertFalse(c.evaluate());
    }

    @Test
    public void check_Trans_condition_lte() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 lte 1;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertTrue(c.evaluate());
        sys.getVariable("Var1").setValue(1.);
        assertTrue(c.evaluate());
        sys.getVariable("Var1").setValue(2.);
        assertFalse(c.evaluate());
    }

    @Test
    public void check_Trans_condition_arith() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"0\"/>\n" +
                "\t\t<VARIABLE name=\"Var2\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"initial\" dur=\"0m\" initial=\"true\"/>\n" +
                "\t\t<STATE name=\"final\" final=\"true\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 eq Var2 * 2 + 2;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        final ProductionSystem sys = generateProductionSystem(filepath);

        final Machine machine = sys.getMachine("Machine");
        final Collection<Transition> transitions = machine.getTransitions();
        assertTrue(transitions.size() == 1);
        final Transition t = (Transition) transitions.toArray()[0];
        final Collection<Condition> conditions = t.getConditions();
        assertTrue(conditions.size() == 1);
        final Condition c = (Condition) conditions.toArray()[0];
        assertFalse(c.evaluate());
        sys.getVariable("Var1").setValue(4.);
        assertTrue(c.evaluate());

    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_Machine_exception_name() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE>\n" +
                "\t\t<STATE dur=\"0s\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";


        print(content);

        //Prepare for test
        generateProductionSystem(filepath);
    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_State_exception_name() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name =\"Machine\">\n" +
                "\t\t<STATE dur=\"0s\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";


        print(content);

        //Prepare for test
        generateProductionSystem(filepath);

    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_State_exception_two_initial() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name =\"Machine\">\n" +
                "\t\t<STATE name=\"State1\" initial=\"true\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" initial=\"true\" dur=\"0s\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";


        print(content);

        //Prepare for test
        generateProductionSystem(filepath);

    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_Variable_exception_name() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "   <VARIABLE type=\"string\" init=\"false\"/>\n" +
                "</PRODSYS>";


        print(content);

        //Prepare for test
        generateProductionSystem(filepath);
    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_Transition_exception_name() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\">\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<TRANSITION from=\"State1\" to=\"State2\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        //Prepare for test
        generateProductionSystem(filepath);
    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_Boolean_exception() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name =\"Machine\">\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\" final=\"ftrulse\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";


        print(content);

        //Prepare for test
        generateProductionSystem(filepath);

    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_Condition_illegal_operator() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name =\"Machine1\">\n" +
                "\t\t<VARIABLE name=\"Variable1\" init=\"0\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<TRANSITION name=\"Transition1\" from=\"State1\" to=\"State2\" cond=\"Variable1 is 3;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";


        print(content);

        //Prepare for test
        generateProductionSystem(filepath);

    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_Trans_effect_illegal_arguments() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" eff=\"Var1 = Var1 + Var3 * 2;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);
        generateProductionSystem(filepath);
    }

    @Test(expected = ProductionSystemSetupException.class)
    public void check_Trans_cond_illegal_arguments() throws Exception {
        final String content = "<PRODSYS name=\"sys\">\n" +
                "\t<MACHINE name=\"Machine\" >\n" +
                "\t\t<VARIABLE name=\"Var1\" init=\"1\"/>\n" +
                "\t\t<STATE name=\"State1\" dur=\"0s\"/>\n" +
                "\t\t<STATE name=\"State2\" dur=\"0s\"/>\n" +
                "\t\t<TRANSITION name=\"Trans1\" from=\"State1\" to=\"State2\" cond=\"Var1 eq Var1 + Var3 * 2;\"/>\n" +
                "\t</MACHINE>\n" +
                "</PRODSYS>";

        print(content);

        generateProductionSystem(filepath);
    }
}