package simulator.parser;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import simulator.model.productionsystem.Action;
import simulator.model.productionsystem.DurationInterval;
import simulator.model.productionsystem.DurationNumeric;
import simulator.model.productionsystem.ProductionSystem;
import simulator.model.simulation.Simulation;
import simulator.model.simulation.SimulationSetupException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static simulator.parser.SimulatorParser.generateSimulation;

public class SimulationHandlerTest {

    private final String filepath = "ProductionSystemHandlerTestFile.xml";
    private ProductionSystem ps;

    @Before
    public void setUp() {
        ps = new ProductionSystem("Test production system please ignore");
        ps.createMachine("M1");
        ps.createVariable("V1", 0);
        ps.createVariable("V2", 0);
    }

    @After
    public void tearDown() {
        try {
            Files.delete(Paths.get(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void print(String content) {
        try (PrintStream out = new PrintStream(new FileOutputStream(filepath))) {
            out.print(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void check_SimulationCount() throws SimulationSetupException {
        final String content = "<SIMULATION count=\"100\" step=\"1s\">\n" +
                "    <LAUNCH machine=\"M1\" date=\"0s\"/>\n" +
                "</SIMULATION>";

        print(content);

        //Prepare for test
        final Simulation sim = generateSimulation(filepath, ps);

        assertEquals(100, sim.getSimulationCount());
    }

    @Test
    public void check_Step() throws SimulationSetupException {
        final String content = "<SIMULATION count=\"1\" step=\"1h2m3s\">\n" +
                "\t<LAUNCH machine=\"M1\" date=\"0s\"/>\n" +
                "</SIMULATION>";

        print(content);

        //Prepare for test
        final Simulation sim = generateSimulation(filepath, ps);

        assertEquals(new DurationNumeric(1, 2, 3), sim.getStep());
    }

    @Test
    public void check_MachineLaunchDate() throws SimulationSetupException {
        final String content = "<SIMULATION count=\"1\" step=\"1s\">\n" +
                "\t<LAUNCH machine=\"M1\" date=\"5h;6h\"/>\n" +
                "</SIMULATION>";

        print(content);

        //Prepare for test
        final Simulation sim = generateSimulation(filepath, ps);

        assertEquals(new DurationInterval(new DurationNumeric(5, 0, 0), new DurationNumeric(6, 0, 0)), sim.getMachinesLaunchDates().get(ps.getMachine("M1")));
    }

    @Test
    public void check_Evolution() throws SimulationSetupException {
        final String content = "<SIMULATION count=\"1\" step=\"1s\">\n" +
                "\t<LAUNCH machine=\"M1\" date=\"0s\"/>\n" +
                "\t<EVOLUTION name=\"increment\" eff=\"V1 = V1 + 1; V2 = V2 - 1;\" dur=\"10h;12h\"/>" +
                "</SIMULATION>";

        print(content);

        //Prepare for test
        final Simulation sim = generateSimulation(filepath, ps);
        final Set<Action> actions = sim.getActions().keySet();
        for (Action action : actions) {
            if (action.getName().equals("increment")) action.execute();
        }
        assertTrue(sim.getActions().values().contains(new DurationInterval(36000, 43200)));
        assertEquals(1., ps.getVariable("V1").getValue(), 0.);
        assertEquals(-1., ps.getVariable("V2").getValue(), 0.);
    }

    @Test
    public void check_Action() throws SimulationSetupException {
        final String content = "<SIMULATION count=\"1\" step=\"1s\">\n" +
                "\t<LAUNCH machine=\"M1\" date=\"0s\"/>\n" +
                "\t<ACTION name=\"plus\" eff=\"V1 = V1 + 10; V2 = V2 - 10;\" date=\"2h;8h\"/>" +
                "</SIMULATION>";

        print(content);

        //Prepare for test
        final Simulation sim = generateSimulation(filepath, ps);
        final Set<Action> actions = sim.getActions().keySet();
        for (Action action : actions) {
            if (action.getName().equals("plus")) action.execute();
        }
        assertTrue(sim.getActions().values().contains(new DurationInterval(7200, 28800)));
        assertEquals(10., ps.getVariable("V1").getValue(), 0.);
        assertEquals(-10., ps.getVariable("V2").getValue(), 0.);
    }


}