find ./ -name "*.java" -o -name "*.html"  -type f |
while read file
do
  echo " $file"
  mv $file $file.icv
  iconv -f WINDOWS-1252 -t UTF-8 $file.icv > $file
  rm -f $file.icv
done