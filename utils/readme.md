# Utilisation des scripts Python

## Génération des sommes de consommation
*Nom du script* : sum_sim_consum.py 
*Fait avec Python 3*

Utilisation : ./sum_sim_consum.py folder
Ici, Folder contient l'ensemble des courbes où il faut faire la sommation par machine.

## Déroulement des trois logiciels SCAF
*Nom du script* : scaf.py
*Fait avec Python 3*

Utilisation :

```
scaf.py [-h] [-sj SIMULATOR_JAR] [-gj GENERATOR_JAR] [-mj MINING_JAR]
               [-sd] [-sw] [-ps PROD_SYS] [-si SIMULATION_INPUT]
               [-so SIMULATOR_OUTPUT] [-gi GENERATOR_INPUT]
               [-go GENERATOR_OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -sj SIMULATOR_JAR, --simulator-jar SIMULATOR_JAR
                        The path to the simulator JAR
  -gj GENERATOR_JAR, --generator-jar GENERATOR_JAR
                        The path to the generator JAR
  -mj MINING_JAR, --mining-jar MINING_JAR
                        The path to the mining JAR
  -sd, --simulator-disable
                        Disables the simulator. The generator input files are
                        required.
  -sw, --simulator-no-warning
                        Disables the warnings about infinite sims
  -ps PROD_SYS, --prod-sys PROD_SYS
                        The simulator production system input file
  -si SIMULATION_INPUT, --simulation-input SIMULATION_INPUT
                        The simulator input file
  -so SIMULATOR_OUTPUT, --simulator-output SIMULATOR_OUTPUT
                        Simulator output folder
  -gi GENERATOR_INPUT, --generator-input GENERATOR_INPUT
                        Generator input folder. Must be provided only if the
                        simulator is disabled
  -go GENERATOR_OUTPUT, --generator-output GENERATOR_OUTPUT
                        Generator output folder
```
Le simulateur peut être désactivé (option `-sd`) mais il faut fournir un fichier ou un dossier d'entrée pour le générateur.

La plupart des paramètres sont optionnels, et les valeurs par défaut sont disponibles en haut du script. 
