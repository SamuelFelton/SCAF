# -*- coding: utf-8 -*-
"""
Runs the simulator, the generator and launches the mining gui with the data preloaded
"""
import sys
import argparse
import os
import subprocess

simulator_jar_path = '../build/simulator/libs/simulator.jar'
generator_jar_path = '../build/generator/libs/generator.jar'
mining_jar_path = '../build/mining/SCAF-mining.jar'

simulator_output = './sim_out'
generator_output = './gen_out'

parser = argparse.ArgumentParser()
parser.add_argument('-sj', '--simulator-jar',
                    help='The path to the simulator JAR', required=False)
parser.add_argument('-gj', '--generator-jar',
                    help='The path to the generator JAR', required=False)
parser.add_argument('-mj', '--mining-jar',
                    help='The path to the mining JAR', required=False)
parser.add_argument('-sd', '--simulator-disable', action="store_true",
                    help='Disables the simulator. The generator input files are required.',
                    required=False)
parser.add_argument('-sw', '--simulator-no-warning', action="store_true",
                    help='Disables the warnings about infinite sims',
                    required=False)

parser.add_argument('-ps', '--prod-sys',
                    help="The simulator production system input file",
                    required=False)
parser.add_argument('-si', '--simulation-input',
                    help="The simulator input file",
                    required=False)
parser.add_argument('-so', '--simulator-output',
                    help='Simulator output folder',
                    required=False)
parser.add_argument('-gi', '--generator-input',
                    help='Generator input folder. Must be provided only if the simulator is disabled',
                    required=False)
parser.add_argument('-go', '--generator-output',
                    help='Generator output folder',
                    required=False)

args = parser.parse_args()

if args.simulator_jar:
    simulator_jar_path = args.simulator_jar
if args.generator_jar:
    generator_jar_path = args.generator_jar
if args.mining_jar:
    mining_jar_path = args.mining_jar

if args.generator_output:
    generator_output = args.generator_output
if args.simulator_output:
    simulator_output = args.simulator_output
    
generator_input = os.path.join(simulator_output, 'generator')
if args.generator_input:
    generator_input = args.generator_input

if args.simulator_disable and (args.simulation_input or args.prod_sys):
    sys.exit('Simulator is disabled, yet input files for simulator are present')
    
if not args.simulator_disable and (not args.simulation_input or not args.prod_sys):
    sys.exit('Simulator is enabled, yet input files for simulator are missing')


if args.simulator_disable and not args.generator_input:
    sys.exit('Simulator is disabled, but no generator input was provided')
if not args.simulator_disable and args.generator_input:
    sys.exit('Simulator is enabled, but a generator input folder was given')
    

if not args.simulator_disable:
    print('========Running Simulator========')
    sim_args = ['java', '-jar', simulator_jar_path, '-g',
                    '-prodsys_input', args.prod_sys,
                    '-simulation_input', args.simulation_input,
                    '-output', simulator_output]
    if args.simulator_no_warning == True:
        sim_args.append('-w')
    res = subprocess.call(sim_args)
    if res != 0:
        sys.exit('Error during simulation, exiting')

print('========Running Generator========')
if os.path.isdir(generator_input):
    for f in os.listdir(generator_input):
        res = subprocess.call(['java', '-jar', generator_jar_path,
                        '-input', os.path.join(generator_input, f),
                        '-output', generator_output])
        print('')
else:
    res = subprocess.call(['java', '-jar', generator_jar_path,
                        '-input', generator_input,
                        '-output', generator_output])
if res != 0:
    sys.exit('Error during data generation, exiting')

print('========Running Mining interface========')
subprocess.call(['java', '-jar', mining_jar_path, generator_output])
if res != 0:
    sys.exit('Error during mining, exiting')
