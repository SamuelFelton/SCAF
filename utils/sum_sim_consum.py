# -*- coding: utf-8 -*-
#!/usr/bin/python
import sys
import glob
import os
from os import path

if __name__ == "__main__":
    """
    Script to sum consumption by simulation id
    
    Usage:
        sum_sim_consum folder
        
    result:
        in folder, for each simulation id, a new file is created which has the same
        format as the other output files. The number of simulations is the minimum
        number of simulations of each machine        
    """
    if len(sys.argv) != 2:
        sys.exit("Wrong number of arguments, use : script.py folder")
    input_s = sys.argv[1]
    if os.path.isdir(input_s) == False:
        sys.exit("Input not found or not a folder")
    by_machine = {}
    for filepath in glob.glob(os.path.join(input_s, '*.*.txt')):
        filename = os.path.basename(filepath)
        filename_split = filename.split('.')
        if filename_split[0] == 'summed':
            continue
            
        sim = filename_split[1]
        if sim not in by_machine:
            by_machine[sim] = []
        by_machine[sim].append(filepath)
    for sim, files_s in by_machine.items():
        try:
            summed_file = open(os.path.join(input_s, 'summed.{}.txt'.format(sim)), 'w')
            files = list(map(lambda fs: open(fs, 'r'),files_s))
            reached_one_end = False
            while True:
                lines = list(map(lambda f: f.readline(), files))
                if '' in lines:
                    break #end of a file reached
                split_lines = list(map(lambda l: l.split('/'), lines))
                
                (ids, steps) = (set(map(lambda sl: sl[0], split_lines)),
                 set(map(lambda sl: float(sl[1]), split_lines)))
                values = list(map(lambda sl: list(map(lambda v: float(v), sl[2].split(';')[:-1])), split_lines))
                
                if len(steps) > 1:
                    sys.exit('Multiple steps found for lines {}'.format(lines))
                if len(ids) > 1:
                    sys.exit('Multiple ids found for lines {}'.format(lines))
                max_len = 0
                for vl in values:
                    max_len = max(len(vl), max_len)
                summed_values = []
                for i in range(max_len):
                    summed_val = 0.0
                    for vl in values:
                        if len(vl) > i:
                            summed_val += vl[i]
                    summed_values.append(summed_val)
                (id, step) = (ids.pop(), steps.pop())
                summed_file.write('{}/{}/{};\n'.format(str(id), str(step), ';'.join(list(map(lambda s: str(s), summed_values)))))
        except IOError:
            print('An IO error happened while summing consommation files')
        finally:
            summed_file.close()
            for f in files:
                f.close()